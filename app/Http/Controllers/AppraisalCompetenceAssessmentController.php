<?php

namespace App\Http\Controllers;

use App\Helpers\AppConstants;
use App\Helpers\ConstAppraisalSections;
use App\Helpers\DataGenerator;
use App\Helpers\DataLoader;
use App\Helpers\Security;
use App\Helpers\SharedCommons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class AppraisalCompetenceAssessmentController extends Controller
{

    private  $formDataUpdateMessage = "Appraisal form data has been updated successfully";

    public function saveSection3(Request $request){

        try{

            if($request->has('save')){

                return $this->section3Save($request);

            }else{

                return $this->section3Update($request);

            }

        }catch (\Exception $exception){

            /*An exception occurred on saving the form, we need to redirect back with the error */
            $error = AppConstants::generalError($exception->getMessage());
            return $this->redirectBackToFormWithError($error);

        }

    }

    private function section3Save($request){

        $appraisalRef = $request['appraisal'];

        //get the competence categories
        $token = Cookie::get(Security::$COOKIE_TOKEN);
        $baseResp = DataLoader::getBehavioralCompetenceCategories($token,[]);
        $categorizedCompetences = $baseResp->statusCode === AppConstants::$STATUS_CODE_SUCCESS ? $baseResp->result : [];

        //list to hold data to send to API
        $competencesList = [];

        //loop through the categories
        foreach ($categorizedCompetences as $category){

            $categoryId = $category->id;
            $competences = $category->behavioralCompetences;

            //foreach competence check for it's values
            foreach ($competences as $item){

                $maxRating = $item->maximumScore;
                $competenceId = $item->id;

                $appraiseeRating = $request[$categoryId.'_appraisee_rating_'.$competenceId];
                $appraiserRating = $request[$categoryId.'_appraiser_rating_'.$competenceId];
                $agreedRating = $request[$categoryId.'_agreed_rating_'.$competenceId];

                $competenceData = [];
                $competenceData['competence_category_id'] = $categoryId;
                $competenceData['appraisal_competence_id'] = $competenceId;

                /*
                 * Some of these fields may be empty e.g if the appraisal is at the appraisee level and
                 * the appraise has not put in their marks. So in this case i dont send them to server because the server
                 * would validate them and it expects only numeric if the values are sent
                 * */
                if(isset($maxRating) && !empty($maxRating)){
                    $competenceData['maximum_rating'] = $maxRating;
                }
                if(isset($appraiseeRating) && !empty($appraiseeRating)){
                    $competenceData['appraisee_rating'] = $appraiseeRating;
                }
                if(isset($appraiserRating) && !empty($appraiserRating)){
                    $competenceData['appraiser_rating'] = $appraiserRating;
                }
                if(isset($agreedRating) && !empty($agreedRating)){
                    $competenceData['agreed_rating'] = $agreedRating;
                }


                //add to list
                $competencesList[] = $competenceData;

            }

        }

        $data = [];
        $data['appraisal_reference'] = $appraisalRef;
        $data['assessments'] = $competencesList;

        $baseResp1 = DataLoader::saveCompetenceAssessments($token, $data);

        if($baseResp1->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

            $msg = $baseResp1->statusDescription;
            return $this->redirectBackToFormWithError($msg);
        }

        $appraisal = $baseResp1->result;



        /*
         * Begin the saving of the scores
         * */

        $dataNext = [];
        $dataNext['appraisal_reference'] = $appraisalRef;
        $dataNext['total_maximum_rating'] = $request['secEmaxTotal'];

        if(isset($request['secEappraiseeTotal']) && !empty($request['secEappraiseeTotal'])){
            $dataNext['total_appraisee_rating'] = $request['secEappraiseeTotal'];
        }
        if(isset($request['secEappraiserTotal']) && !empty($request['secEappraiserTotal'])){
            $dataNext['total_appraiser_rating'] = $request['secEappraiserTotal'];
        }
        if(isset($request['secEagreedTotal']) && !empty($request['secEagreedTotal'])){
            $dataNext['total_agreed_rating'] = $request['secEagreedTotal'];
        }
        if(isset($request['supervisor_comment']) && !empty($request['supervisor_comment'])){
            $dataNext['supervisor_comment'] = $request['supervisor_comment'];
        }

        $baseRespNext = DataLoader::saveCompetenceAssessmentsScore($dataNext);

        if($baseRespNext->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

            $msg = $baseRespNext->statusDescription;
            return $this->redirectBackToFormWithError($msg);

        }
        $appraisal = $baseRespNext->result;

        /*
         * End saving of the scores
         * */

        return $this->reloadTheAppraisalPage($appraisal,ConstAppraisalSections::SECTION_4);


    }

    private function section3Update($request){

        $appraisalRef = $request['appraisal'];

        //get the competence categories
        $token = Cookie::get(Security::$COOKIE_TOKEN);
        $baseResp = DataLoader::getBehavioralCompetenceCategories($token, []);
        $categorizedCompetences = $baseResp->statusCode === AppConstants::$STATUS_CODE_SUCCESS ? $baseResp->result : [];

        //list to hold data to send to API
        $competencesList = [];

        //loop through the categories
        foreach ($categorizedCompetences as $category){

            $categoryId = $category->id;
            $competences = $category->behavioralCompetences;

            //foreach competence check for it's values
            foreach ($competences as $item){

                $maxRating = $item->maximumScore;
                $competenceId = $item->id;

                $appraiseeRating = $request[$categoryId.'_appraisee_rating_'.$competenceId];
                $appraiserRating = $request[$categoryId.'_appraiser_rating_'.$competenceId];
                $agreedRating = $request[$categoryId.'_agreed_rating_'.$competenceId];
                $recordId = $request[$categoryId.'_record_id_'.$competenceId];

                $competenceData = [];
                $competenceData['record_id'] = $recordId;
                $competenceData['competence_category_id'] = $categoryId;
                $competenceData['appraisal_competence_id'] = $competenceId;

                /*
                 * Some of these fields may be empty e.g if the appraisal is at the appraisee level and
                 * the appraise has not put in their marks. So in this case i dont send them to server because the server
                 * would validate them and it expects only numeric if the values are sent
                 * */
                if(isset($maxRating) && !empty($maxRating)){
                    $competenceData['maximum_rating'] = $maxRating;
                }
                if(isset($appraiseeRating) && !empty($appraiseeRating)){
                    $competenceData['appraisee_rating'] = $appraiseeRating;
                }
                if(isset($appraiserRating) && !empty($appraiserRating)){
                    $competenceData['appraiser_rating'] = $appraiserRating;
                }
                if(isset($agreedRating) && !empty($agreedRating)){
                    $competenceData['agreed_rating'] = $agreedRating;
                }

                //add to list
                $competencesList[] = $competenceData;

            }

        }


        $data = [];
        $data['appraisal_reference'] = $appraisalRef;
        $data['assessments'] = $competencesList;

        $baseResp1 = DataLoader::saveCompetenceAssessments($token, $data,true);

        if($baseResp1->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

            $msg = $baseResp1->statusDescription;
            return $this->redirectBackToFormWithError($msg);

        }


        /*
         * Begin saving of the scores
         * */

        $recordId = $request['scores_sec_e_record_id'];

        $dataNext = [];
        $dataNext['record_id'] = $recordId;
        $dataNext['appraisal_reference'] = $appraisalRef;
        $dataNext['total_maximum_rating'] = $request['secEmaxTotal'];

        /*
         * These fields can be empty e.g when the form is at the appraisee and the appraiser has no input in the form
         * */
        if(isset($request['secEappraiseeTotal']) && !empty($request['secEappraiseeTotal'])){
            $dataNext['total_appraisee_rating'] = $request['secEappraiseeTotal'];
        }
        if(isset($request['secEappraiserTotal']) && !empty($request['secEappraiserTotal'])){
            $dataNext['total_appraiser_rating'] = $request['secEappraiserTotal'];
        }
        if(isset($request['secEagreedTotal']) && !empty($request['secEagreedTotal'])){
            $dataNext['total_agreed_rating'] = $request['secEagreedTotal'];
        }
        if(isset($request['supervisor_comment']) && !empty($request['supervisor_comment'])){
            $dataNext['supervisor_comment'] = $request['supervisor_comment'];
        }


        $baseRespNext = isset($recordId) && !empty($recordId) ?
            DataLoader::saveCompetenceAssessmentsScore($dataNext,true,$recordId) :
            DataLoader::saveCompetenceAssessmentsScore($dataNext);


        if($baseRespNext->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

            $msg = $baseRespNext->statusDescription;
            return $this->redirectBackToFormWithError($msg);

        }

        $appraisal = $baseRespNext->result;

        /*
         * End saving of the scores
         * */

        return $this->reloadTheAppraisalPage($appraisal,ConstAppraisalSections::SECTION_4);

    }


    /**
     * @param $error
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectBackToFormWithError($error) {

        return redirect()->back()->withErrors(SharedCommons::customFormError($error))->withInput();

    }


    /**
     * @param $appraisal
     * @param $activeStep
     * @param bool $isUpdate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function reloadTheAppraisalPage($appraisal, $activeStep, $isUpdate = true) {

        $appraisalRef = $appraisal->appraisalRef;
        return redirect(route('open_appraisal',[$appraisalRef, $activeStep, $isUpdate]));
    }

}
