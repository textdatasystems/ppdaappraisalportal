<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\Helpers\AppConstants;
use App\Helpers\ConstAppraisalSections;
use App\Helpers\ConstAppraisalStatus;
use App\Helpers\DataGenerator;
use App\Helpers\DataLoader;
use App\Helpers\Security;
use App\Helpers\SharedCommons;
use App\Letter;
use App\Models\ApiAppraisal;
use App\Models\ApiAppraisalReq;
use App\Models\ApiUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class AppraisalController extends BaseController
{

    private  $formDataSavedMessage = "Appraisal form has been created, please continue to fill in the relevant sections, when done click the SUBMIT button at the end of the Form";
    private  $formDataUpdateMessage = "Appraisal form data has been updated successfully";


    public function __construct() {

        parent::__construct(AppConstants::$ACTIVE_MOD_APPRAISAL);
    }


    public function index($appraisalRef = null, $activeStep = null, $fromUpdateAction = true){

        if($appraisalRef == null){
            return $this->getView();
        }


        /*
         * Here we are viewing a filled in form
         * */

        $baseResp = DataLoader::getAppraisalByAppraisalReference($appraisalRef);

        if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            return $this->redirectBackToFormWithError($baseResp->statusDescription);
        }

        $appraisal = $baseResp->result;

        if($fromUpdateAction){

            $message = $this->formDataUpdateMessage;

        }else{

            /*
             * This carters for the first time the form is saved,
             * */
            $message = $activeStep == ConstAppraisalSections::SECTION_2 ? $this->formDataSavedMessage : null;

        }

        return $this->getView($appraisal,$message,null, DataGenerator::visibleSections($activeStep),$activeStep);

    }


    public function getView(ApiAppraisal $appraisal = null, $formMessage =  null, $workFlowStep = null,
                             $visibleSections = [],$activeStep = null, $isError = false, $msg = null) {

        /*
         * 1. Get the strategic objectives based on the user's organization
         * 2. Get the competences based on the user's organization
         * */


        /*
         * Get the logged in user
         * */
        $user = session(Security::$SESSION_USER);


        /*
         * Get strategic objectives
         * */
        $token = Cookie::get(Security::$COOKIE_TOKEN);
        $baseRespObjs = DataLoader::getStrategicObjectives($token);


        /*
         * Get assessment scores if it's an existing appraisal
         * */
        $assessmentScores = $this->getAppraisalCompetenceAssessments($appraisal);


        /*
         * Get the behavioral competences, if an existing form attach the scores to
         * */
        $baseRespCompetences = isset($appraisal) ? DataLoader::getBehavioralCompetenceCategories($token, $assessmentScores) :  DataLoader::getBehavioralCompetenceCategories($token);


        /*
         * Get the user's academic background
         * */
        $baseRespAcademicBg = DataLoader::getUsersAcademicBackgrounds($user->username);


        /*
         * Get the system users
         * */
        $baseRespUsers = DataLoader::allUsers();

        $strategicObjectives = $baseRespObjs->statusCode == AppConstants::$STATUS_CODE_SUCCESS ? $baseRespObjs->result : [];
        $categorizedCompetences = $baseRespCompetences->statusCode == AppConstants::$STATUS_CODE_SUCCESS ? $baseRespCompetences->result : [];
        $institutions = $baseRespAcademicBg->statusCode == AppConstants::$STATUS_CODE_SUCCESS ? $baseRespAcademicBg->result : [];
        $users = $baseRespUsers->statusCode == AppConstants::$STATUS_CODE_SUCCESS ? $baseRespUsers->result : [];


        /*
         * Class we use to disable or enable editing of the form
         * */

        $editClass = $this->checkIfAuthUserCanEditAtThisStage($appraisal,$user);

        /*
         * We use this array to mark the sections that are done
         * */
        $doneSteps = isset($appraisal) ? DataGenerator::getFilledInSections($appraisal) : [];


        /*
         * Get the active step index for activating/opening the active step of the form using Java Script
         * */
        $activeStep = !isset($activeStep) ? 'NONE' : $activeStep;
        $defaultActiveStepIndex = $this->getDefaultActiveStepIndex($activeStep);


        $active_module = $this->getActiveModule($appraisal, $user);

        $viewParams = [];
        $viewParams[] = ['editClass','appraisal','user','active_module','doneSteps','activeStep',
            'strategicObjectives', 'categorizedCompetences','institutions','users','isError','msg','defaultActiveStepIndex'];

        if($formMessage != null){
            $viewParams[] = 'formMessage';
        }


        /*
         * If no workflow step, probably it's a new form so it's at the owner stage
         * */
        if($workFlowStep == null){
            $workFlowStep = AppConstants::$WORK_FLOW_STEP1_OWNER;
            $viewParams[] = 'workFlowStep';
        }else{
            $viewParams[] = 'workFlowStep';
        }

        return view('appraisals/appraisal_form', compact($viewParams));

     }


    public function saveSectionB(Request $request){


        try{

            //it's the first time we saving section B
            if($request->has('save')){

                //get the appraisal form ID
                $appraisalRef = $request['appraisal'];

                //how do I dynamically get the max_rows passed
                //if it's numeric we take the value, else we assume we did not receive any values
                $countRowsPassed = is_numeric($request['counter_max_rows']) ? $request['counter_max_rows'] : 0;


                //define array to hold the schools
                $institutions = [];

                //now we loop through all the rows saving the data therein
                for($i = 1; $i<= $countRowsPassed; $i++){

                    $school = $request['school_'.$i];
                    $year = $request['year_of_study_'.$i];
                    $award = $request['award_'.$i];

                    //institute object
                    $institute = [];
                    $institute['school'] = $school;
                    $institute['year'] = $year;
                    $institute['award'] = $award;

                    //Add to institute list
                    $institutions[] = $institute;

                }


                //we now have to save these institutes using the API
                $token = Cookie::get(Security::$COOKIE_TOKEN);
                $data = [];
                $data['appraisal_reference'] = $appraisalRef;
                $data['institutions'] = $institutions;

                $baseResp = DataLoader::saveAcademicInstitutes($token, $data);

                if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                    $msg = $baseResp->statusDescription;
                    $respAppraisal = DataLoader::getAppraisalByAppraisalReference($appraisalRef);
                    $appraisal = $respAppraisal->statusCode == AppConstants::$STATUS_CODE_SUCCESS ? $respAppraisal->result : null;
                    return $this->getView($appraisal,null,null,DataGenerator::visibleSections(AppConstants::$SECTION_B),AppConstants::$SECTION_B,true,$msg);

                }

                $appraisal = $baseResp->result;
                return $this->getView($appraisal,$this->formDataUpdateMessage,null,DataGenerator::visibleSections(AppConstants::$SECTION_C),AppConstants::$SECTION_C);

            }
            else{

                //get the appraisal form ID
                $appraisalRef = $request['appraisal'];

                //dynamically get the count of rows passed
                $countRowsPassed = is_numeric($request['counter_max_rows_update']) ? $request['counter_max_rows_update'] : 0;

                //hold list of institutions
                $institutions = [];

                //loop through the rows passed as we update the data
                for($i = 1; $i<= $countRowsPassed; $i++){

                    $recordId  = $request['record_id_'.$i];
                    $school = $request['school_'.$i];
                    $year = $request['year_of_study_'.$i];
                    $award = $request['award_'.$i];

                    //institute object
                    $institute = [];
                    $institute['school'] = $school;
                    $institute['year'] = $year;
                    $institute['award'] = $award;

                    //if this is not set, then it's a new row being created, so in that case we do not set the record id field
                    if(isset($recordId)){
                        $institute['record_id'] = $recordId;
                    }

                    //Add to institute list
                    $institutions[] = $institute;

                }

                //we now have to save these institutes using the API
                $token = Cookie::get(Security::$COOKIE_TOKEN);
                $data = [];
                $data['appraisal_reference'] = $appraisalRef;
                $data['institutions'] = $institutions;

                //make request to the API
                $baseResp = DataLoader::saveAcademicInstitutes($token, $data, true);

                if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                    $msg = $baseResp->statusDescription;
                    $appraisal = null;
                    return $this->getView($appraisal,null,null,DataGenerator::visibleSections(AppConstants::$SECTION_B),AppConstants::$SECTION_B,true,$msg);

                }

                $appraisal = $baseResp->result;
                return $this->getView($appraisal,$this->formDataUpdateMessage,null,DataGenerator::visibleSections(AppConstants::$SECTION_C),AppConstants::$SECTION_C);

            }

        }catch (\Exception $exception){

            /*An exception occurred on saving the form, we need to redirect back with the error */

            $error = AppConstants::generalError($exception->getMessage());
            return $this->getView(null,null,null,[],null,true,$error);

        }

    }


    public function saveSection6b(Request $request){

        try{

            $appraisalRef = $request['appraisal'];
            $appraiserName = $request['appraiser_name'];
            $appraiseeName = $request['appraisee_name'];
            $duration = $request['duration'];
            $startDate = $request['start_date'];
            $endDate = $request['end_date'];

            $data = [];
            $data['appraisal_reference'] = $appraisalRef;
            $data['appraiser_name'] = $appraiserName;
            $data['appraisee_name'] = $appraiseeName;
            $data['duration'] = $duration;
            $data['start_date'] = $startDate;
            $data['end_date'] = $endDate;

            if(!$request->has('record_id')){

                $baseResp = DataLoader::saveSupervisorDeclaration($data,false);

            }else{

                $identifier = $request['record_id'];
                $baseResp = DataLoader::saveSupervisorDeclaration($data,true,$identifier);

            }

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $msg = $baseResp->statusDescription;
                return $this->redirectBackToFormWithError($msg);
            }

            $appraisal = $baseResp->result;
            return $this->reloadTheAppraisalPage($appraisal,ConstAppraisalSections::SECTION_6C);

        }catch (\Exception $exception){

            $msg = AppConstants::generalError($exception->getMessage());
            return $this->redirectBackToFormWithError($msg);

        }

    }


    public function saveSection6c(Request $request){

        try{

            $appraisalRef = $request['appraisal'];
            $appraiseeName = $request['appraisee_name'];
            $agreedOrDisagreed = $request['agree_or_disagree'];
            $reasonForDisagree = $request['reason_for_disagreement'];
            $name = $request['name'];
            $initials = $request['initials'];
            $date = $request['date'];

            $data = [];
            $data['appraisal_reference'] = $appraisalRef;
            $data['appraisee_name'] = $appraiseeName;
            $data['agreement_decision'] = $agreedOrDisagreed;
            $data['disagreement_reason'] = $reasonForDisagree;
            $data['declaration_name'] = $name;
            $data['declaration_initials'] = $initials;
            $data['declaration_date'] = $date;

            if(!$request->has('record_id')){

                $baseResp = DataLoader::saveAppraiseeRemarks($data,false);

            }else{

                $identifier = $request['record_id'];
                $baseResp = DataLoader::saveAppraiseeRemarks($data,true,$identifier);

            }

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $msg = $baseResp->statusDescription;
                return $this->redirectBackToFormWithError($msg);
            }

            $appraisal = $baseResp->result;
            return $this->reloadTheAppraisalPage($appraisal,ConstAppraisalSections::SECTION_6D);

        }catch (\Exception $exception){

            $msg = AppConstants::generalError($exception->getMessage());
            return $this->redirectBackToFormWithError($msg);

        }

    }


    public function saveSection6d(Request $request){


        try{

            $appraisalRef = $request['appraisal'];
            $hodComment = $request['comment'];
            $hodName = $request['hod_name'];
            $hodInitials = $request['hod_initials'];
            $date = $request['date'];

            $data = [];
            $data['appraisal_reference'] = $appraisalRef;
            $data['comments'] = $hodComment;
            $data['name'] = $hodName;
            $data['initials'] = $hodInitials;
            $data['date'] = $date;

            if(!$request->has('record_id')){

                $baseResp = DataLoader::saveHodComment($data,false);

            }else{

                $identifier = $request['record_id'];
                $baseResp = DataLoader::saveHodComment($data,true,$identifier);

            }

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $msg = $baseResp->statusDescription;
                return $this->redirectBackToFormWithError($msg);
            }

            $appraisal = $baseResp->result;
            return $this->reloadTheAppraisalPage($appraisal,ConstAppraisalSections::SECTION_6E);

        }catch (\Exception $exception){

            $msg = AppConstants::generalError($exception->getMessage());
            return $this->redirectBackToFormWithError($msg);

        }

    }


    public function saveSection6e(Request $request){


        try{

            $appraisalRef = $request['appraisal'];
            $edComment = $request['comment'];
            $edName = $request['ed_name'];
            $edInitials = $request['ed_initials'];
            $date = $request['date'];

            $data = [];
            $data['appraisal_reference'] = $appraisalRef;
            $data['comments'] = $edComment;
            $data['name'] = $edName;
            $data['initials'] = $edInitials;
            $data['date'] = $date;

            if(!$request->has('record_id')){

                $baseResp = DataLoader::saveDirectorComment($data,false);

            }else{

                $identifier = $request['record_id'];
                $baseResp = DataLoader::saveDirectorComment($data,true,$identifier);

            }

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $msg = $baseResp->statusDescription;
                return $this->redirectBackToFormWithError($msg);

            }


            $appraisal = $baseResp->result;
            return $this->reloadTheAppraisalPage($appraisal,ConstAppraisalSections::SECTION_1);


        }catch (\Exception $exception){

            $msg = AppConstants::generalError($exception->getMessage());
            return $this->redirectBackToFormWithError($msg);

        }

    }


    public function moveAppraisal($appraisalId,$workflowStep){

        try{

            $active_module = $this->active_module;
            $appraisalStatus = $workflowStep;
            $appraisalRef = $appraisalId;

            if(
                $appraisalStatus == ConstAppraisalStatus::PENDING_WORKFLOW_SUBMISSION ||
                $appraisalStatus == ConstAppraisalStatus::REJECTED_BY_SUPERVISOR ||
                $appraisalStatus == ConstAppraisalStatus::REJECTED_BY_DEPARTMENT_HEAD ||
                $appraisalStatus == ConstAppraisalStatus::REJECTED_BY_EXECUTIVE_DIRECTOR
            ){

                $data = [];
                $data['appraisal_reference'] = $appraisalId;

                $baseResp = DataLoader::appraisalWorkFlowStart($data);

                if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                    $error =$baseResp->statusDescription;
                    return $this->redirectBackToFormWithError($error);
                }

                /*
                 * Redirect to the list of appraisee forms
                 * */
                return redirect()->route('appraisal-forms.owner');

            }


            /*
             * The appraiser is resubmitting the form after inputting their declaration
             * */
            if($appraisalStatus == ConstAppraisalStatus::PENDING_APPRAISEE_AGREEMENT){

                return $this->appraisalApprove($appraisalRef, $workflowStep);

            }

            /*
             * Appraisal is already in a workflow so we are forwarding it
             * */
            return $this->redirectBackToFormWithError("APPRAISAL CANNOT BE SUBMITTED OR MARKED AS ACCEPTED BY APPRAISEE AT CURRENT STAGE [".$appraisalStatus."]");


        }catch (\Exception $exception){
            /*An exception occurred on saving the form, we need to redirect back with the error */
            $error = AppConstants::generalError($exception->getMessage());
            return $this->getView(null,null,null,[],null,true,$error);
        }

    }


    public function cancelAppraisal($appraisalRef){

        try{

            /*
             * Send cancel request to the API
             * */
            $baseResp = DataLoader::appraisalCancel($appraisalRef);

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                return $this->redirectBackToFormWithError($baseResp->statusDescription);

            }

            /*
             * Redirect back to my appraisals
             * */
            return redirect()->route('appraisal-forms.owner');


        }catch (\Exception $exception){

            /*An exception occurred on saving the form, we need to redirect back with the error */
            $error = AppConstants::generalError($exception->getMessage());
            return $this->redirectBackToFormWithError($error);

        }

    }


    public function assignApprovers(Request $request){

        try{

            $appraisalId = $request['appraisal'];
            $supervisor = $request['supervisor'];
            $hod = $request['hod'];
            $ed = $request['ed'];

            $data = [];
            $data['appraisal_reference'] = $appraisalId;
            $data['supervisor_id'] = $supervisor;
            $data['department_head_id'] = $hod;
            $data['executive_director_id'] = $ed;

            $baseResp = DataLoader::appraisalWorkFlowStart($data);

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $error = $baseResp->statusDescription;
                return $this->getView(null,null,null,[],null,true,$error);
            }

            return redirect(route('user_dashboard'));

        }catch (\Exception $exception){

            /*An exception occurred on saving the form, we need to redirect back with the error */
            $error = AppConstants::generalError($exception->getMessage());
            return $this->getView(null,null,null,[],null,true,$error);

        }

    }


    public function show($id){

        $appraisalRef = $id;

        /*
         * About redirecting back with an Error Message
         * */
        if(!$appraisalRef){
            return redirect()->back();
        }

        /*
         * Go to the TDS API and get the appraisal
         * */
        $baseResp = DataLoader::getAppraisalByAppraisalReference($appraisalRef);

        /*
         * About redirecting back with an Error Message
         * */
        if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            return redirect()->back();
        }

        /*
         * We got the appraisal
         * */
        $appraisal = $baseResp->result;
        return $this->getView($appraisal,null,null);

    }


    public function getApprovalForm($appraisalId,$workFlowStep){

        $appraisalRef = $appraisalId;
        $appraisalStatus = $workFlowStep;
        $active_module = $this->active_module;
        $viewParams = [];
        $viewParams[] = 'active_module';
        $viewParams[] = 'appraisalStatus';
        $viewParams[] = 'appraisalRef';

        return view('appraisals.approval_decision',compact($viewParams));

    }


    public function approveOrRejectAppraisal(Request $request){

        try{

            $appraisalRef= $request['appraisal_reference'];
            $decision = $request['approval_decision'];
            $rejectionReason = $request['rejection_reason'];

            $data = [];
            $data['appraisal_reference'] = $appraisalRef;
            $data['is_movement_forward'] = $decision == 'approved' ? true : false;
            $data['remark'] = $rejectionReason;

            //send movement request to the API
            $baseResp = DataLoader::appraisalWorkFlowMove($data);

            //movement failed
            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $error = $baseResp->statusDescription;
                return $this->getView(null,null,null,[],null,true,$error);
            }

            //movement was successful
            return redirect()-> route('user_dashboard');


        }catch (\Exception $exception){

            /*An exception occurred on saving the form, we need to redirect back with the error */
            $error = AppConstants::generalError($exception->getMessage());
            return $this->getView(null,null,null,[],null,true,$error);

        }

    }


    public function appraisalApprove($appraisalId,$workflowStep){

        try{

            $appraisalStatus = $workflowStep;
            $appraisalRef = $appraisalId;

            $isAppraiseeResubmission = $appraisalStatus == ConstAppraisalStatus::PENDING_APPRAISEE_AGREEMENT;

            /*
             * Confirm if the appraisal is really up for approval or its for re-submission by appraisee after declaration
             * */
            if($appraisalStatus == ConstAppraisalStatus::PENDING_SUPERVISOR_APPROVAL ||
                $appraisalStatus == ConstAppraisalStatus::PENDING_DEPARTMENT_HEAD_APPROVAL ||
                $appraisalStatus == ConstAppraisalStatus::PENDING_EXECUTIVE_DIRECTOR_APPROVAL ||
                $appraisalStatus == ConstAppraisalStatus::PENDING_EXECUTIVE_DIRECTOR_APPROVAL_AS_SUPERVISOR ||
                $isAppraiseeResubmission
            ){

                $data = [];
                $data['appraisal_reference'] = $appraisalRef;
                $data['is_movement_forward'] = true;

                //send movement request to the API
                $baseResp = DataLoader::appraisalWorkFlowMove($data);

                if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                    $error =$baseResp->statusDescription;
                    return $this->redirectBackToFormWithError($error);
                }

                /*
                 * Redirect to the list of the right list of appraisals
                 * */
                return $this->loadAppropriatePageBasedOnTheStatusOfAppraisalAction($appraisalStatus);

            }else{

                /*
                 * This status is not a valid status for approval
                 * */
                $error = AppConstants::$MSG_INVALID_APPROVAL_STATUS.' ['.$appraisalStatus.']';
                return $this->redirectBackToFormWithError($error);

            }


        }catch (\Exception $exception){
            /*An exception occurred on saving the form, we need to redirect back with the error */
            $error = AppConstants::generalError($exception->getMessage());
            return $this->redirectBackToFormWithError($error);
        }

    }


    public function appraisalReject(Request $request){

        try{

            $appraisalRef= $request['appraisal_reference'];
            $rejectionReason = $request['rejection_reason'];

            $data = [];
            $data['appraisal_reference'] = $appraisalRef;
            $data['is_movement_forward'] = false;
            $data['remark'] = $rejectionReason;

            //send movement request to the API
            $baseResp = DataLoader::appraisalWorkFlowMove($data);

            //movement failed
            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $error = $baseResp->statusDescription;
                return $this->getView(null,null,null,[],null,true,$error);
            }

            //movement was successful
            return redirect()-> route('user_dashboard');

        }catch (\Exception $exception){

            /*An exception occurred on saving the form, we need to redirect back with the error */
            $error = AppConstants::generalError($exception->getMessage());
            return $this->getView(null,null,null,[],null,true,$error);

        }

    }


    public function getOwnerAppraisals(Request $request){


        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $req = new ApiAppraisalReq();
            $req->token = $token;
            $req->workflowRole = AppConstants::WORK_FLOW_ROLE_OWNER;

            $baseResp = DataLoader::getUserAppraisals($req);

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->getViewOwnerAppraisals([], $baseResp->statusDescription, true);
            }

            /*
             * We successfully got the appraisals
             * */
            $appraisals =  $baseResp->result;
            return $this->getViewOwnerAppraisals($appraisals);

        }catch (\Exception $exception){

            $msg = AppConstants::generalError($exception->getMessage());
            return $this->getViewOwnerAppraisals([], $msg, true);

        }

    }


    public function getSupervisorAppraisals(Request $request){

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $req = new ApiAppraisalReq();
            $req->token = $token;
            $req->workflowRole = AppConstants::WORK_FLOW_ROLE_SUPERVISOR;
            $req->status = ConstAppraisalStatus::PENDING_SUPERVISOR_APPROVAL;
            $req->supervisorDecision = 'PENDING';

            $baseResp = DataLoader::getUserAppraisals($req);
            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->getViewSupervisorAppraisals([], $baseResp->statusDescription, true);
            }

            /*
             * We successfully got the appraisals
             * */
            $appraisals =  $baseResp->result;
            return $this->getViewSupervisorAppraisals($appraisals);

        }catch (\Exception $exception){

            $msg = AppConstants::generalError($exception->getMessage());
            return $this->getViewSupervisorAppraisals([], $msg, true);

        }

    }


    public function getViewOwnerAppraisals($appraisals, $msg = null, $isError = false){

        $active_module = AppConstants::$ACTIVE_MOD_MY_APPRAISALS;
        return view('appraisals.appraisal-myappraisal',compact('appraisals','active_module','msg','isError'));

    }

    public function getViewSupervisorAppraisals($appraisals, $msg = null, $isError = false){

        $active_module = AppConstants::$ACTIVE_MOD_SUPERVISOR_APPRAISALS;
        return view('appraisals.appraisal-supervisor-approval',compact('appraisals','active_module','msg','isError'));

    }

    public function getViewHodAppraisals($appraisals, $msg = null, $isError = false){

        $active_module = AppConstants::$ACTIVE_MOD_HOD_APPRAISALS;
        return view('appraisals.appraisal-hod-approval',compact('appraisals','active_module','msg','isError'));

    }

    public function getViewDirectorAppraisals($appraisals, $msg = null, $isError = false){

        $active_module = AppConstants::$ACTIVE_MOD_DIRECTOR_APPRAISALS;
        return view('appraisals.appraisal-director-approval',compact('appraisals','active_module','msg','isError'));

    }


    public function getHodAppraisals(Request $request){

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $req = new ApiAppraisalReq();
            $req->token = $token;
            $req->workflowRole = AppConstants::WORK_FLOW_ROLE_HOD;
            $req->status = ConstAppraisalStatus::PENDING_DEPARTMENT_HEAD_APPROVAL;
            $req->hodDecision = 'PENDING';

            $baseResp = DataLoader::getUserAppraisals($req);

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->getViewHodAppraisals([], $baseResp->statusDescription, true);
            }

            /*
             * We successfully got the appraisals
             * */
            $appraisals =  $baseResp->result;
            return $this->getViewHodAppraisals($appraisals);

        }catch (\Exception $exception){

            $msg = AppConstants::generalError($exception->getMessage());
            return $this->getViewHodAppraisals([], $msg, true);

        }

    }


    public function getDirectorAppraisals(Request $request){

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $req = new ApiAppraisalReq();
            $req->token = $token;
            $req->workflowRole = AppConstants::WORK_FLOW_ROLE_DIRECTOR;
            $req->directorDecision = 'PENDING';
            $req->status = ConstAppraisalStatus::PENDING_EXECUTIVE_DIRECTOR_APPROVAL;

            $baseResp = DataLoader::getUserAppraisals($req);

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->getViewDirectorAppraisals([], $baseResp->statusDescription, true);
            }

            /*
             * We successfully got the appraisals
             * */
            $appraisals =  $baseResp->result;
            return $this->getViewDirectorAppraisals($appraisals);

        }catch (\Exception $exception){

            $msg = AppConstants::generalError($exception->getMessage());
            return $this->getViewDirectorAppraisals([], $msg, true);

        }

    }


    /**
     * @param $appraisal
     * @return array
     */
    private function getAppraisalCompetenceAssessments($appraisal) {

        return isset($appraisal) && isset($appraisal->competenceAssessments) && count($appraisal->competenceAssessments) > 0 ?
            $appraisal->competenceAssessments : [];

    }


    private function checkIfAuthUserCanEditAtThisStage($appraisal, ApiUser $user) {


        try{

            if(!isset($appraisal) || !$appraisal instanceof  ApiAppraisal){
                return AppConstants::$EDIT_MODE_ON;
            }

            /**
             * check the edit mode status based on the appraisal workflow type
             */
            if($appraisal->appraisal_workflow_type == APPRAISAL_WORKFLOW_TYPE_NORMAL){

                return $this->checkEditModeForWorkflowTypeNormal($appraisal, $user);

            }else if($appraisal->appraisal_workflow_type == APPRAISAL_WORKFLOW_TYPE_MANAGERS){

                return $this->checkEditModeForWorkflowTypeManagers($appraisal, $user);

            }else if($appraisal->appraisal_workflow_type == APPRAISAL_WORKFLOW_TYPE_DIRECTORS){

                return $this->checkEditModeForWorkflowTypeDirectors($appraisal, $user);

            }

            return AppConstants::$EDIT_MODE_OFF;

        }catch (\Exception $exception){
            return AppConstants::$EDIT_MODE_OFF;
        }

    }


    /**
     * @param $error
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectBackToFormWithError($error) {

        return redirect()->back()->withErrors(SharedCommons::customFormError($error))->withInput();

    }


    private function getDefaultActiveStepIndex($activeStep) {

        switch ($activeStep){
            case ConstAppraisalSections::SECTION_1:{
                return 0; break;
            }
            case ConstAppraisalSections::SECTION_2:{
                return 1; break;
            }
            case ConstAppraisalSections::SECTION_3:{
                return 2;  break;
            }
            case ConstAppraisalSections::SECTION_4:{
                return 3; break;
            }
            case ConstAppraisalSections::SECTION_5:{
                return 4; break;
            }
            case ConstAppraisalSections::SECTION_6A:{
                return 5; break;
            }
            case ConstAppraisalSections::SECTION_6B:{
                return 6; break;
            }
            case ConstAppraisalSections::SECTION_6C:{
                return 7; break;
            }
            case ConstAppraisalSections::SECTION_6D:{
                return 8; break;
            }
            case ConstAppraisalSections::SECTION_6E:{
                return 9; break;
            }
            default :{
                return 0;
            }
        }


    }


    public function returnAppraisalToPreviousStep(Request $request){


        try{

            $appraisalRef = $request['appraisal_reference'];
            $previousStatus = $request['previous_status'];
            $comment = $request['comment'];

            $data = [];
            $data['appraisal_reference'] = $appraisalRef;
            $data['previous_status'] = $previousStatus;
            $data['comment'] = $comment;
            

            $baseResp = DataLoader::workFlowReturnToPreviousStatus($data);

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $error =$baseResp->statusDescription;
                return $this->redirectBackToFormWithError($error);
            }

            $moduleIndicator = $request['active_module'];
            return $this->loadAppropriatePageBasedOnTheStatusOfAppraisalAction(null, $moduleIndicator);


        }catch (\Exception $exception){
            /*An exception occurred on saving the form, we need to redirect back with the error */
            $error = AppConstants::generalError($exception->getMessage());
            return $this->getView(null,null,null,[],null,true,$error);
        }

    }


    /**
     * @param $appraisal
     * @param $activeStep
     * @param bool $isUpdate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function reloadTheAppraisalPage($appraisal, $activeStep, $isUpdate = true) {

        $appraisalRef = $appraisal->appraisalRef;
        return redirect(route('open_appraisal',[$appraisalRef, $activeStep, $isUpdate]));
    }

    private function getActiveModule($appraisal, $user)
    {

        try{


            if($appraisal == null){
                return AppConstants::$ACTIVE_MOD_MY_APPRAISALS;
            }

            $username = $user->username;
            $supervisorUsername = $appraisal->supervisorUsername;
            $ownerUsername = $appraisal->ownerUsername;
            $hod = $appraisal->deptHeadUsername;
            $ed = $appraisal->executiveDirectorUsername;

            if($username == $ownerUsername){
                return AppConstants::$ACTIVE_MOD_MY_APPRAISALS;
            }

            if($username == $supervisorUsername){
                return AppConstants::$ACTIVE_MOD_SUPERVISOR_APPRAISALS;
            }

            if($username == $hod){
                return AppConstants::$ACTIVE_MOD_HOD_APPRAISALS;
            }

            if($username == $ed){
                return AppConstants::$ACTIVE_MOD_DIRECTOR_APPRAISALS;
            }

            return AppConstants::$ACTIVE_MOD_MY_APPRAISALS;

        }catch (\Exception $exception){
            return AppConstants::$ACTIVE_MOD_MY_APPRAISALS;
        }

    }

    /**
     * @param $appraisalStatus
     * @return \Illuminate\Http\RedirectResponse
     */
    public function loadAppropriatePageBasedOnTheStatusOfAppraisalAction($appraisalStatus = null, $moduleIndicator = null): \Illuminate\Http\RedirectResponse
    {


        if(!is_null($appraisalStatus)){

            if($appraisalStatus == null){
                return redirect()->route('appraisal-forms.owner');
            }

            if($appraisalStatus == ConstAppraisalStatus::PENDING_SUPERVISOR_APPROVAL){
                //return to the supervisor page
                return redirect()->route('appraisal-forms.supervisor');

            }else if($appraisalStatus == ConstAppraisalStatus::PENDING_DEPARTMENT_HEAD_APPROVAL){
                //return to the hod page
                return redirect()->route('appraisal-forms.hod');

            }else if($appraisalStatus == ConstAppraisalStatus::PENDING_EXECUTIVE_DIRECTOR_APPROVAL){
                //return to the hod page
                return redirect()->route('appraisal-forms.director');

            }



        }else if(!is_null($moduleIndicator)){


            if($moduleIndicator == 'supervisor'){
                //return to the supervisor page
                return redirect()->route('appraisal-forms.supervisor');

            }else if($moduleIndicator == 'hod'){
                //return to the hod page
                return redirect()->route('appraisal-forms.hod');

            }else if($moduleIndicator == 'ed'){
                //return to the hod page
                return redirect()->route('appraisal-forms.director');

            }else if($moduleIndicator == 'owner'){
                //return to the hod page
                return redirect()->route('appraisal-forms.owner');

            }


        }else{

            return redirect()->route('appraisal-forms.owner');

        }

        return redirect()->route('appraisal-forms.owner');


    }


    public function getAppraisalComments($appraisalRef){

        try{

            $user = session(Security::$SESSION_USER);
            if($user == null){
                return AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN;
            }

            $baseResp = DataLoader::getAppraisalByAppraisalReference($appraisalRef);

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $baseResp->statusDescription;
            }

            $appraisal = $baseResp->result;

            return view('appraisals.modal-view-comments', compact('appraisal'));

        }catch (\Exception $exception){
            return AppConstants::generalError($exception->getMessage());
        }

    }

    public function getLetterDownloadLink($appraisalRef){

        try{

            $baseResp = DataLoader::downloadAppraisalPDF($appraisalRef);

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $data['statusCode'] = 1;
                $data['statusDescription'] = $baseResp->statusDescription;
                return json_encode($data);
            }

            $data['statusCode'] = 0;
            $data['statusDescription'] = $baseResp->statusDescription;
            $data['result'] = $baseResp->statusDescription;
            return json_encode($data);

        }catch (\Exception $exception){
            /*An exception occurred on saving the form, we need to redirect back with the error */
            $error = AppConstants::generalError($exception->getMessage());
            $data['statusCode'] = 1;
            $data['statusDescription'] = $error;
            return json_encode($data);
        }

    }

    private function checkEditModeForWorkflowTypeNormal($appraisal, $user)
    {

        if(!isset($user)){
            return AppConstants::$EDIT_MODE_OFF;
        }

        /*
        * Form is completed (fully approved) or cancelled no fucking Nigga should edit it
        * */
        if($appraisal->status == ConstAppraisalStatus::COMPLETED_SUCCESSFULLY || $appraisal->status == ConstAppraisalStatus::CANCELED_BY_OWNER){
            return AppConstants::$EDIT_MODE_OFF;
        }

        /*
         * Form is at supervisor step, so if not supervisor reject edit
         * */
        if($appraisal->status == ConstAppraisalStatus::PENDING_SUPERVISOR_APPROVAL && $user->username != $appraisal->supervisorUsername){
            return AppConstants::$EDIT_MODE_OFF;
        }

        /*
         * Form is at pending appraisee acceptance step, so if not appraisee reject edit
         * */
        if($appraisal->status == ConstAppraisalStatus::PENDING_APPRAISEE_AGREEMENT && $user->username != $appraisal->ownerUsername){
            return AppConstants::$EDIT_MODE_OFF;
        }

        /*
         *
         * Form is at the HOD step, so if not HOD reject edit*/
        if($appraisal->status == ConstAppraisalStatus::PENDING_DEPARTMENT_HEAD_APPROVAL && $user->username != $appraisal->deptHeadUsername){
            return AppConstants::$EDIT_MODE_OFF;
        }

        /*
         * Form is at the ED, so if not ED reject edit
         * */
        if($appraisal->status == ConstAppraisalStatus::PENDING_EXECUTIVE_DIRECTOR_APPROVAL && $user->username != $appraisal->executiveDirectorUsername){
            return AppConstants::$EDIT_MODE_OFF;
        }

        /*
         * All rejected forms must be at the owner, any other guy reject edit
         * */
        if(ConstAppraisalStatus::isRejectedStatus($appraisal->status) && $user->username != $appraisal->ownerUsername){
            return AppConstants::$EDIT_MODE_OFF;
        }

        return AppConstants::$EDIT_MODE_ON;

    }

    private function checkEditModeForWorkflowTypeManagers($appraisal, $user)
    {
        return AppConstants::$EDIT_MODE_ON;
    }

    private function checkEditModeForWorkflowTypeDirectors($appraisal, $user)
    {

        if(!isset($user)){
            return AppConstants::$EDIT_MODE_OFF;
        }

        /*
        * Form is completed (fully approved) or cancelled no fucking Nigga should edit it
        * */
        if($appraisal->status == ConstAppraisalStatus::COMPLETED_SUCCESSFULLY || $appraisal->status == ConstAppraisalStatus::CANCELED_BY_OWNER){
            return AppConstants::$EDIT_MODE_OFF;
        }

        /*
         * Form is at pending appraisee acceptance step, so if not appraisee reject edit
         * */
        if($appraisal->status == ConstAppraisalStatus::PENDING_APPRAISEE_AGREEMENT && $user->username != $appraisal->ownerUsername){
            return AppConstants::$EDIT_MODE_OFF;
        }

        /*
         *
         * Form is at pending ED approval as supervisor*/
        if($appraisal->status == ConstAppraisalStatus::PENDING_EXECUTIVE_DIRECTOR_APPROVAL_AS_SUPERVISOR && $user->username != $appraisal->executiveDirectorUsername){
            return AppConstants::$EDIT_MODE_OFF;
        }

        /*
         * Form is at the ED, so if not ED reject edit
         * */
        if($appraisal->status == ConstAppraisalStatus::PENDING_EXECUTIVE_DIRECTOR_APPROVAL && $user->username != $appraisal->executiveDirectorUsername){
            return AppConstants::$EDIT_MODE_OFF;
        }

        /*
         * All rejected forms must be at the owner, any other guy reject edit
         * */
        if(ConstAppraisalStatus::isRejectedStatus($appraisal->status) && $user->username != $appraisal->ownerUsername){
            return AppConstants::$EDIT_MODE_OFF;
        }

        return AppConstants::$EDIT_MODE_ON;

    }


}
