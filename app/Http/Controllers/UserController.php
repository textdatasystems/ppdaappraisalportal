<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\Department;
use App\Helpers\AjaxResponseHandler;
use App\Helpers\ApiHandler;
use App\Helpers\AppConstants;
use App\Helpers\DataFormatter;
use App\Helpers\DataGenerator;
use App\Helpers\DataLoader;
use App\Helpers\EndPoints;
use App\Helpers\Security;
use App\Http\Requests\ChangeDefaultPasswordRequest;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\PasswordChangeRequest;
use App\Http\Requests\PasswordResetRequest;
use App\Http\Requests\SignInRequest;
use App\Http\Requests\UserProfileUpdateRequest;
use App\Models\ApiApplicationStat;

use App\Repositories\Contracts\UserRepoInterface;
use App\Repositories\General\ApplicationStatsRepo;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Session\Session;

class UserController extends Controller {

    use AuthenticatesUsers;

    /**
     * @var UserRepoInterface
     */
    private $userRepo;
    private $active_module;

    function __construct(UserRepoInterface $userRepo) {
        $this->userRepo = $userRepo;
        $this->active_module = AppConstants::$ACTIVE_MOD_USERS;
    }


    /*
    * Logic for Users : Start
    * */


    public function getCreationUserForm($message = null, $isError = false){

        /*
         * Set variable for the active module for highlight purposes
         * */

        $active_module = $this->active_module;

        /*
         * Get the user who is logged in
         * */
        $user = session(Security::$SESSION_USER);

        /*
         * What happens if I fail to get the user who is logged in, I redirect to login page
         * */
        if($user == null){
            return redirect('/');
        }


        /*
         * We managed to get the logged in user,
         * Now we need to go and get the list of organizations
         * */

        $token = Cookie::get(Security::$COOKIE_TOKEN);

        /*
         * Get all the data required to prefill the user form
         * I think you can decide to fetch all this data in one trip
         * */
        $respOrganizations = ApiHandler::makeGetRequest(EndPoints::$ORGANIZATIONS_ALL, true, $token);
        $respRegOffices = ApiHandler::makeGetRequest(EndPoints::$REGIONAL_OFFICES_ALL, true, $token);
        $respDepts = ApiHandler::makeGetRequest(EndPoints::$DEPARTMENTS_ALL, true, $token);
        $respCategories = ApiHandler::makeGetRequest(EndPoints::$EMPLOYEE_CATEGORIES_ALL, true, $token);
        $respRoleCodes = ApiHandler::makeGetRequest(EndPoints::$ROLE_CODES_ALL, true, $token);


        /*
         * Failed to get a response from the server
         * */

        if($respOrganizations->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            return $respOrganizations->statusDescription;
        }
        if($respRegOffices->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            return $respRegOffices->statusDescription;
        }
        if($respDepts->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            return $respDepts->statusDescription;
        }
        if($respCategories->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            return $respCategories->statusDescription;
        }
        if($respRoleCodes->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            return $respRoleCodes->statusDescription;
        }

        /*
         * End of check to see if we got data from the server
         * Past this we received data from the requests made above
         * */



        /*
         * Get the different status and status description from the different responses
         * */

        $apiRespOrg = json_decode($respOrganizations->result, true);
        $statusCodeOrg = $apiRespOrg['statusCode'];
        $statusDescriptionOrg = $apiRespOrg['statusDescription'];

        $apiRespCategories = json_decode($respCategories->result, true);
        $statusCodeCategories = $apiRespCategories['statusCode'];
        $statusDescriptionCategories = $apiRespCategories['statusDescription'];

        $apiRespDepts = json_decode($respDepts->result, true);
        $statusCodeDept = $apiRespDepts['statusCode'];
        $statusDescriptionDept = $apiRespDepts['statusDescription'];

        $apiRespRegOffices = json_decode($respRegOffices->result, true);
        $statusCodeRegOffices = $apiRespRegOffices['statusCode'];
        $statusDescriptionRegOffices = $apiRespRegOffices['statusDescription'];

        $apiRespRoleCodes = json_decode($respRoleCodes->result, true);
        $statusCodeRoleCodes = $apiRespRoleCodes['statusCode'];
        $statusDescriptionRoleCodes = $apiRespRoleCodes['statusDescription'];

        /*
         * End getting the different statuses
         * */


        /*
         * Check if we got success for all the requests from the server
         * */
        if($statusCodeOrg != AppConstants::$STATUS_CODE_SUCCESS){
            return $statusDescriptionOrg;
        }
        if($statusCodeDept != AppConstants::$STATUS_CODE_SUCCESS){
            return $statusDescriptionDept;
        }
        if($statusCodeRegOffices != AppConstants::$STATUS_CODE_SUCCESS){
            return $statusDescriptionRegOffices;
        }
        if($statusCodeCategories != AppConstants::$STATUS_CODE_SUCCESS){
            return $statusDescriptionCategories;
        }
        if($statusCodeRoleCodes != AppConstants::$STATUS_CODE_SUCCESS){
            return $statusDescriptionRoleCodes;
        }
        /*
         * By the end these checks, we got success from the server for all our request
         * We can proceed to format the data
         * */


        /*
         * We format the data returned
         * */
        $organizations = DataFormatter::formatOrganizations($apiRespOrg['data']);
        $regionalOffices = DataFormatter::formatRegionalOffices($apiRespRegOffices['data']);
        $departments = DataFormatter::formatDepartments($apiRespDepts['data']);
        $categories = DataFormatter::formatEmployeeCategories($apiRespCategories['data']);
        $roles = DataFormatter::formatRoleCodes($apiRespRoleCodes['data']);


        /*
         * I echo below line to see if I reach here after the long journey
         * */
        //return "SUCCESS FOR SURE WE GOOD HERE";


        /*
         * Here a dealing with a new request to this page, we a just returning a fresh page
         * */
        if($message == null){

            return view('user/user-form',
                compact('active_module','user','organizations','regionalOffices','departments','categories','roles'));

        }


        /*
         * Here it's like have been doing some stuff on the page
         * */


        /*
         * We were creating a user and got an error
         * */
        if($isError){

            return back()
                ->withInput()
                ->withErrors([$message]);

        }else{

            $successMessage = $message;
            return view('user/user-form',
                compact('active_module','successMessage','user','organizations','regionalOffices','departments','categories','roles'));

        }

    }


    public function storeUser(CreateUserRequest $request){


        /*
         * Get the authentication token
         * */
        $token = Cookie::get(Security::$COOKIE_TOKEN);


        /*
         * Request data
         * */
        $data = [

            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'other_name' => $request['other_name'],
            'email' => $request['email'],

            'staff_number' => $request['staff_number'],
            'contract_start_date' => $request['contract_start_date'],
            'contract_expiry_date' => $request['contract_expiry_date'],
            'date_of_birth' => $request['date_of_birth'],
            'designation' => $request['designation'],

            'phone' => $request['phone'],
            'org_code' => $request['org_code'],
            'role_code' => $request['role_code'],
            'department_code' => $request['department_code'],
            'category_code' => $request['category_code'],
            'regional_office_code' => $request['regional_office_code'],
            'created_by' => $request['created_by'],
            'letter_movement_role' => $request['letter_movement_role'],

        ];


        /*
         * Action
         * */
        $action = EndPoints::$USERS_STORE;


        /*
         * Send request to the API
         * */
        $resp = ApiHandler::makePostRequest($action, $data, true, $token);

        /*
         * Error occurred on sending the request, redirect to the page with data
         * */
        if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

            return $this->getCreationUserForm($resp->statusDescription, true);

        }

        /*
         * Here got a response from the server, so we get the response from the server
         * */
        $apiResult = json_decode($resp->result, true);


        /*
         * Get the status code from the API
         * */
        $statusCode = $apiResult['statusCode'];
        $statusDescription = $apiResult['statusDescription'];

        if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

            return $this->getCreationUserForm($statusDescription, true);

        }


        /*
         * Operation was successful at the server side
         * */

        $successMessage = "SYSTEM USER [".$request['first_name']."] SUCCESSFULLY CREATED";

        /*
         * Return to the form create page with a success message
         * */
        return $this->getCreationUserForm($successMessage);

    }


    public function updateUserProfileByOwner(UserProfileUpdateRequest $request){

        /*
         * Get the authentication token
         * */
        $token = Cookie::get(Security::$COOKIE_TOKEN);

        /*
         * Request data
         * */
        $data = [
            'first_name' => $request['first_name'], 'last_name' => $request['last_name'],
            'designation' => $request['designation'], 'date_of_birth' => $request['date_of_birth'],
        ];

        $user = session(Security::$SESSION_USER);
        if(!isset($user)){
            return $this->getUserProfilePage(AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN, true);
        }

        /*
         * Send request to the API
         * */
        $resp = DataLoader::updateUserProfileByOwner($data, $user->username);

        /*
         * Error occurred on sending the request, redirect to the page with data
         * */
        if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            return $this->getUserProfilePage($resp->statusDescription, true);
        }

        /*
         * Operation was successful at the server side
         * */
        $successMessage = "PROFILE DETAILS SUCCESSFULLY UPDATED";

        /*
         * Return to the form with a success message
         * */
        return $this->getUserProfilePage($successMessage);

    }



    public function allUsers($deletionMessage = null){

        /*
         * Aren't you supposed to put this stuff in a try catch block
         * */

        /*
         * Get tha access token
         * */
        $token = Cookie::get(Security::$COOKIE_TOKEN);

        /*
         * Send request to the api
         * */
        $resp = ApiHandler::makeGetRequest(EndPoints::$USERS_ALL, true, $token);

        /*
         * We didn't get a response from API
         * */
        if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            /*
             * Return error
             * */
            return $resp->statusDescription;
        }

        /*
         * We got a response from the API
         * */

        $apiResp = json_decode($resp->result, true);

        /*
         * Get statusCode, statusDescription
         * */
        $statusCode = $apiResp['statusCode'];
        $statusDescription = $apiResp['statusDescription'];

        /*
         * Check if got a success on fetching the data from the API
         * */
        if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            /*
             * Return error
             * */
            return $statusDescription;
        }


        /*
         * We got the data from the API
         * */
        $data = $apiResp['data'];


        /*
         * Format data
         * */
        $users = DataFormatter::formatUsers($data);


        /*
         * ------------------------------------
         * START : COMPLEX LOGIC OF GETTING
         * ORGS, DEPTS, ROLES, REGIONS, CATEGORIES
         * ------------------------------------
         * */


        /*
         * Get all the data required to prefill the user form
         * I think you can decide to fetch all this data in one trip
         * */
        $respOrganizations = ApiHandler::makeGetRequest(EndPoints::$ORGANIZATIONS_ALL, true, $token);
        $respRegOffices = ApiHandler::makeGetRequest(EndPoints::$REGIONAL_OFFICES_ALL, true, $token);
        $respDepts = ApiHandler::makeGetRequest(EndPoints::$DEPARTMENTS_ALL, true, $token);
        $respCategories = ApiHandler::makeGetRequest(EndPoints::$EMPLOYEE_CATEGORIES_ALL, true, $token);
        $respRoleCodes = ApiHandler::makeGetRequest(EndPoints::$ROLE_CODES_ALL, true, $token);


        /*
         * Failed to get a response from the server
         * */

        if($respOrganizations->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            return $respOrganizations->statusDescription;
        }
        if($respRegOffices->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            return $respRegOffices->statusDescription;
        }
        if($respDepts->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            return $respDepts->statusDescription;
        }
        if($respCategories->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            return $respCategories->statusDescription;
        }
        if($respRoleCodes->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            return $respRoleCodes->statusDescription;
        }

        /*
         * End of check to see if we got data from the server
         * Past this we received data from the requests made above
         * */



        /*
         * Get the different status and status description from the different responses
         * */

        $apiRespOrg = json_decode($respOrganizations->result, true);
        $statusCodeOrg = $apiRespOrg['statusCode'];
        $statusDescriptionOrg = $apiRespOrg['statusDescription'];

        $apiRespCategories = json_decode($respCategories->result, true);
        $statusCodeCategories = $apiRespCategories['statusCode'];
        $statusDescriptionCategories = $apiRespCategories['statusDescription'];

        $apiRespDepts = json_decode($respDepts->result, true);
        $statusCodeDept = $apiRespDepts['statusCode'];
        $statusDescriptionDept = $apiRespDepts['statusDescription'];

        $apiRespRegOffices = json_decode($respRegOffices->result, true);
        $statusCodeRegOffices = $apiRespRegOffices['statusCode'];
        $statusDescriptionRegOffices = $apiRespRegOffices['statusDescription'];

        $apiRespRoleCodes = json_decode($respRoleCodes->result, true);
        $statusCodeRoleCodes = $apiRespRoleCodes['statusCode'];
        $statusDescriptionRoleCodes = $apiRespRoleCodes['statusDescription'];

        /*
         * End getting the different statuses
         * */


        /*
         * Check if we got success for all the requests from the server
         * */
        if($statusCodeOrg != AppConstants::$STATUS_CODE_SUCCESS){
            return $statusDescriptionOrg;
        }
        if($statusCodeDept != AppConstants::$STATUS_CODE_SUCCESS){
            return $statusDescriptionDept;
        }
        if($statusCodeRegOffices != AppConstants::$STATUS_CODE_SUCCESS){
            return $statusDescriptionRegOffices;
        }
        if($statusCodeCategories != AppConstants::$STATUS_CODE_SUCCESS){
            return $statusDescriptionCategories;
        }
        if($statusCodeRoleCodes != AppConstants::$STATUS_CODE_SUCCESS){
            return $statusDescriptionRoleCodes;
        }
        /*
         * By the end these checks, we got success from the server for all our request
         * We can proceed to format the data
         * */


        /*
         * We format the data returned
         * */
        $organizations = DataFormatter::formatOrganizations($apiRespOrg['data']);
        $regionalOffices = DataFormatter::formatRegionalOffices($apiRespRegOffices['data']);
        $departments = DataFormatter::formatDepartments($apiRespDepts['data']);
        $categories = DataFormatter::formatEmployeeCategories($apiRespCategories['data']);
        $roles = DataFormatter::formatRoleCodes($apiRespRoleCodes['data']);


        /*
         * I echo below line to see if I reach here after the long journey
         * */
        //return "SUCCESS FOR SURE WE GOOD HERE";


        /*
         * ----------------------------------
         * END : COMPLEX LOGIC
         * ---------------------------------
         * */


        /*
         * Holds the active module
         * */
        $active_module = $this->active_module;


        /*
         * Get logged in user
         * */
        $author = session(Security::$SESSION_USER);


        /*
         * We are unable to get the logged in user due to a session timeout
         * */
        if($author == null){
            return "Failed to get logged in user. Please login again and try again";
        }


        /*
         * Return the view with the role codes
         * */
        return view('user/user-list',compact(
            'active_module','author','users','organizations','regionalOffices','departments','categories','roles','deletionMessage'));

    }


    public function updateUser(Request $request){


        try{


            /*
             * Check if request received is ajax
             * */
            if(!$request->ajax()){

                /*
                 * We did not get JSON from the client
                 * */
                return response()->json(['error' => [AppConstants::$MSG_BAD_REQUEST_JSON_EXPECTED]], 403);

            }


            /*
             * Valid JSON received, validate the request
             * */
            $validator = Validator::make($request->all(), [

                'first_name' => 'required|min:2',
                'last_name' => 'required|min:2',
                'email' => 'required|email',
                'org_code' => 'required|min:2',
                'regional_office_code' => 'required',
                'department_code' => 'required',
                'role_code' => 'required',
                'staff_number'=>'required|min:2',
                'date_of_birth'=>'required|date',
                'designation'=>'required|min:2',

            ]);


            /*
             * If validation failed
             * */
            if (!$validator->passes()) {

                return response()->json(['error'=>$validator->errors()->all()]);

            }


            /*
             * Successful validation
             * */
            return $this->updateUserViaApi($request);


        }catch (\Exception $exception){

            /*
             * Log error here
             * */
            return response()->json(['error' => [AppConstants::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage()]], 500);

        }

    }


    public function getResetPasswordForm($msg = null, $isError = false){

        return view('user.password-forgot', compact('msg','isError'));

    }

    public function resetPassword(PasswordResetRequest $request){

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            /*
             * Request data
             * */
            $data = [  'username' => $request['username'] ];

            /*
             * Action
             * */
            $action = EndPoints::$USERS_RESET_PASSWORD;


            /*
             * Send request to the API
             * */
            $resp = ApiHandler::makePostRequest($action, $data, true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                return $this->getResetPasswordForm($resp->statusDescription, true);

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);


            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                return $this->getResetPasswordForm($statusDescription, true);

            }


            /*
             * Operation was successful at the server side
             * */

            $successMessage = "PASSWORD SUCCESSFULLY RESET, CHECK EMAIL FOR NEW CREDENTIALS";

            /*
             * Return to the form create page with a success message
             * */
            return $this->getResetPasswordForm($successMessage);


        }catch (\Exception $exception){

            return $this->getResetPasswordForm(AppConstants::$GENERAL_ERROR_AT_TDS. ' ' .$exception->getMessage());

        }


    }

    /*
     * Logic for Users : End
     * */




    public function signin(SignInRequest $request){


        /*
         * Sign in via the TDS Appraisal System API
         * Save the auth_token to a session
         * For all next requests send the auth_token as a header
         * */


        /*
         * Username
         * */
        $username = $request['username'];
        $password = $request['password'];

        /*
         * Build data to send in the request
         * */
        $data = ['username'=>$username,'password'=>$password];


        /*
         * Get the end point to access
         * */
        $endPoint = EndPoints::$USERS_LOGIN;


        /*
         * Make request to the API
         * */
        $result = ApiHandler::makePostRequest($endPoint,$data);


        /*
         * We failed to get data from the API
         * */
        if($result->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

            throw ValidationException::withMessages([
                $this->username() => $result->statusDescription
            ]);

        }


        /*
         * We got data from the API
         * */
        $apiResult = json_decode($result->result, true);


        /*
         * Get the statusCode, statusDescription
         * */
        $statusCode = $apiResult['statusCode'];
        $statusDescription = $apiResult['statusDescription'];


        /*
         * We failed to validate from the API
         * */
        if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

            throw ValidationException::withMessages([
                $this->username() => $statusDescription
            ]);

        }


        /*
         * The user is required to change their password
         * */
        if($statusDescription == AppConstants::$REQUIRE_CHANGE_PASSWORD){

            $token = $apiResult['token'];
            $user = $apiResult['data']['profile'];
            $user = DataFormatter::getApiUser($user);

            //store the temp user object in the session and as well store the access token
            session([Security::$SESSION_TEMP_USER => $user]);
            $cookie = cookie(Security::$COOKIE_TOKEN, $token, Security::$COOKIE_TOKEN_EXPIRY_MINUTES);

            return redirect()->route('user.password.change-auto.form')->withCookie($cookie);

        }


        /*
         *Authentication was successful
         * */


        /*
         * Get the access token
         * */
        $token = $apiResult['token'];

        /*
         * Get user profile information and dashboard info
         * */
        $user = $apiResult['data']['profile'];
        $user = DataFormatter::getApiUser($user);

        $roleCode = $user->roleCode;
        return $this->showUserDashboardBasedOnUserType($roleCode, $token, $user);

    }

    private function showUserDashboardBasedOnUserType($account_type, $accessToken, $user) {


        /*
         * Create a cookie to hold the access token
         * */
        $cookie = cookie(Security::$COOKIE_TOKEN, $accessToken, Security::$COOKIE_TOKEN_EXPIRY_MINUTES);

        /*
         * Save the user in a session
         * */
        session([Security::$SESSION_USER => $user]);

        /*
         * it's an admin account
         * */
        if($account_type == AppConstants::$ROLE_CODE_ADMIN){

            return redirect()->route('admin_dashboard')->withCookie($cookie);

        }

        return $this->appSelectionView($accessToken);
        /*
         * it's a normal user account
         * */
        return redirect()->route('appraisal-forms.owner')->withCookie($cookie);

    }

    public function getAdminDashboard(){


        $token = Cookie::get(Security::$COOKIE_TOKEN);

        /*
         * Get the app stats for use on the dashboard
         * */
        $repoResp = ApplicationStatsRepo::getApplicationStats($token);

        /*
         * If we fail to get the values we default to 0
         * */
        $appStats = $repoResp->statusCode == AppConstants::$STATUS_CODE_SUCCESS ?
                    $repoResp->repoData : new ApiApplicationStat();


        return view('admin.admin-dashboard', compact('appStats'));

    }

    public function getUserDashboard(){

        try{

            return redirect(route('appraisal-forms.owner'));

            $active_module = AppConstants::$ACTIVE_MOD_DASHBOARD;

            /*
             * Get the top 3 latest appraisals from the server
             * */
            $token = Cookie::get(Security::$COOKIE_TOKEN);
            $baseResp = DataLoader::getTopThreeLatestAppraisals($token);

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $msg = $baseResp->statusDescription;
                $isError = true;
                return view('user.dashboard', compact('user','active_module','msg','isError'));
            }

            /*
             * Get the data
             * */
            $data = $baseResp->result;
            $appraisals = DataFormatter::formatAppraisals($data['appraisals_owned']);
            $appraisalsAssigned = DataFormatter::formatAppraisals($data['appraisals_assigned']);

            $summaries = DataGenerator::getAppraisalSummaries($appraisals);
            $assignedSummaries = DataGenerator::getAppraisalSummaries($appraisalsAssigned);

            /*
             * Get logged in user
             * */
            $user = session(Security::$SESSION_USER);

            return view('user.dashboard', compact('user','active_module','appraisals','appraisalsAssigned','summaries','assignedSummaries'));

        }catch (\Exception $exception){

            $user = session(Security::$SESSION_USER);
            $msg = AppConstants::generalError($exception->getMessage());
            $isError = true;
            return view('user.dashboard', compact('user','active_module','msg','isError'));

        }



        $user = Auth::user();

        $appraisals = null; /* Appraisal::with(
            ['academicBackgrounds','keyDuties','additionalAssignments',
                'competences','workflow', 'assignments','keyDuties'] )->
        where('user_id','=',$user->id)->orderBy('created_at','desc')->get(); */



        $user = null;//User::with(['department'])->find($user->id);
        $appraisalsAssigned = null;// DataGenerator::getAssingedAppraisals($user->id);

        $summaries = null;//DataGenerator::getAppraisalSummaries($appraisals);
        $assignedSummaries =null;// DataGenerator::getAppraisalSummaries($appraisalsAssigned);

       // return view('user.user_dashboard',compact('appraisals','user','active_module','summaries','appraisalsAssigned','assignedSummaries'));


    }

    public function signoutAdmin(Request $request){

        return $this->logout($request);

    }

    public function logoutAndRedirectBackToPpdaApps(Request $request){

        $request->session()->invalidate();
        $url = "http://".EndPoints::SERVER_IP.'/ppda-apps/selection';
        return Redirect::to($url);

    }

    public function getUserRegistrationForm($message = null){

        $active_module = AppConstants::$ACTIVE_MOD_USERS;
        $departments = Department::all();
        $roles = Role::all();
        $registrationMessage = $message;

        if($message != nullOrEmptyString()){
            return view('user.registration',compact('active_module','departments','roles','registrationMessage'));
        }
        return view('user.registration',compact('active_module','departments','roles'));

    }

    public function createUser(CreateUserRequest $request){

        $user = new User();
        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->email = $request['email'];
        $user->department_id = $request['department'];
        $user->staff_category = $request['staff_category'];
        $roleId = $request['role'];
        $unencrypted_password = $request['password'];//Security::randomPassword();
        $user->password = bcrypt($unencrypted_password);
        $user->account_type = AppConstants::$ACCOUNT_TYPE_USER;
        $user->created_at = Carbon::now();

        $username = $this->createUsername($user->first_name,$user->last_name);
        $user->username = $username;


        if(($this->userRepo->createUser($user))){

            $user->roles()->sync([$roleId]);

            $registrationMessage = "Account for user ".$username.
                " with password [ ".$unencrypted_password." ] has been successfully created";

            return $this->getUserRegistrationForm($registrationMessage);

        }

        return redirect()->back();

    }

    private function createUsername($firstName, $lastName) {

        $firstNameArr = str_split(strtolower($firstName));
        $firstNameInitial = $firstNameArr[0];

        $temporaryUsername = $firstNameInitial . strtolower($lastName);
        $username = $this->userRepo->nonExistentUsername($temporaryUsername);

        return strtolower($username);

    }

    public function userProfiles(){

        $users = $this->userRepo->users();
        $active_module = AppConstants::$ACTIVE_MOD_USERS;
        return view('user.user_profiles',compact('active_module','users'));

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function updateUserViaApi(Request $request) {


        /*
         * Get the authentication token
         * */
        $token = Cookie::get(Security::$COOKIE_TOKEN);

        /*
         * User we are updating
         * */
        $username = $request['username'];

        /*
         * Request data
         * */
        $data = [


            'username' => $username,
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'other_name' => $request['other_name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'org_code' => $request['org_code'],
            'role_code' => $request['role_code'],
            'department_code' => $request['department_code'],
            'category_code' => $request['category_code'],
            'regional_office_code' => $request['regional_office_code'],
            'created_by' => $request['created_by'],
            'updated_by' => $request['updated_by'],
            'letter_movement_role' => $request['letter_movement_role'],

            'staff_number' => $request['staff_number'],
            'date_of_birth' => $request['date_of_birth'],
            'designation' => $request['designation'],
        ];


        /*
         * Action
         * */
        $action = EndPoints::$USERS_UPDATE;


        /*
         * Send request to the API
         * */
        $resp = ApiHandler::makePutRequest($action, $username, $data, true, $token);

        /*
         * Error occurred on sending the request
         * */
        if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

             return response()->json(['error' => [$resp->statusDescription]]);

        }

        /*
         * Here got a response from the server, so we get the response from the server
         * */
        $apiResult = json_decode($resp->result, true);


        /*
         * Get the status code from the API
         * */
        $statusCode = $apiResult['statusCode'];
        $statusDescription = $apiResult['statusDescription'];


        /*
         * Api returned failed
         * */
        if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

            return response()->json(['error' => [$statusDescription]]);

        }


        /*
         * Operation was successful at the server side
         * */

        $successMessage = "SYSTEM USER [".$username."] SUCCESSFULLY UPDATED";


        /*
         * Return to the form create page with a success message
         * */
        return response()->json(['success' => $successMessage]);


    }

    public function deleteProfile(Request $request){


        try{

            /*
             * Get the authentication token
             * */
            $token = Cookie::get(Security::$COOKIE_TOKEN);

            /*
             * User we are updating
             * */
            $username = $request['id'];


            /*
             * Get the user who is deleting
             * */
            $user = session(Security::$SESSION_USER);

            /*
             * We failed to get the logged in user
             * */
            if($user == null){
                return $this->allUsers(AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN);
            }

            /*
             * Request data
             * */
            $data = [ 'deleted_by' => $user->username ];


            /*
             * Action
             * */
            $action = EndPoints::$USERS_DELETE;


            /*
             * Send request to the API
             * */
            $resp = ApiHandler::makeDeleteRequest($action, $username, $data, true, $token);

            /*
             * Error occurred on sending the request
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->allUsers($resp->statusDescription);
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);


            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];


            /*
             * Api returned failed
             * */
            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->allUsers($resp->$statusDescription);
            }


            /*
             * Operation was successful at the server side
             * */

            $successMessage = "SYSTEM USER [".$username."] SUCCESSFULLY DELETED";


            /*
             * Return to the form create page with a success message
             * */
            return $this->allUsers($successMessage);


        }catch (\Exception $exception){

            /*
             * We should log the error message we have got
             * */
            $errorMessage = "Failed To Delete User With Error. ".$exception->getMessage();
            return $this->allUsers($errorMessage);

        }

    }


    public function getPasswordChangeForm($msg = null, $isError = false){

        $active_module = AppConstants::$ACTIVE_MOD_PROFILES;
        $user = session(Security::$SESSION_USER);

        if($user == null){
            return redirect('/');
        }

        /*
         * If it's the admin redirect to the admin change password page
         * */
        if($user->roleCode == AppConstants::$ROLE_CODE_ADMIN){
            return view('user.password-change-admin',compact('active_module', 'msg','isError','user'));
        }else{
            return view('user.password-change-general',compact('active_module', 'msg','isError','user'));
        }



    }

    public function changePassword(PasswordChangeRequest $request){


        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            /*
             * Request data
             * */
            $data = [
                'username' => $request['username'],
                'old_password' => $request['old_password'],
                'new_password' => $request['new_password'],
            ];

            /*
             * Action
             * */
            $action = EndPoints::$USERS_CHANGE_PASSWORD;


            /*
             * Send request to the API
             * */
            $resp = ApiHandler::makePostRequest($action, $data, true, $token, EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                return $this->getPasswordChangeForm($resp->statusDescription, true);

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);


            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                return $this->getPasswordChangeForm($statusDescription, true);

            }


            /*
             * Operation was successful at the server side
             * */

            $successMessage = "PASSWORD SUCCESSFULLY CHANGED";

            /*
             * Return to the form create page with a success message
             * */
            return $this->getPasswordChangeForm($successMessage, false);


        }catch (\Exception $exception){

            return $this->getPasswordChangeForm(AppConstants::$GENERAL_ERROR_AT_TDS. ' ' .$exception->getMessage(), true);

        }

    }

    public function getUserProfilePage($msg = null, $isError = false){

        try{

            $active_module = AppConstants::$ACTIVE_MOD_PROFILES;

            $user = session(Security::$SESSION_USER);
            if($user ==  null){
                return redirect('login');
            }

            //send request to API
            $baseResp = DataLoader::getUsersAcademicBackgrounds($user->username);

            //error on getting API data
            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $msg = $baseResp->statusDescription;
                $isError = true;
                return view('user.user-profile', compact('active_module','user','msg','isError'));
            }

            //get the academic backgrounds
            $academicBackgrounds = $baseResp->result;

            return view('user.user-profile', compact('active_module','user','msg','isError','academicBackgrounds'));

        }catch (\Exception $exception){

            $user = session(Security::$SESSION_USER);
            if($user ==  null){
                return redirect('login');
            }

            $active_module = AppConstants::$ACTIVE_MOD_PROFILES;
            $msg = AppConstants::generalError($exception->getMessage());
            $isError = true;
            return view('user.user-profile', compact('active_module','user','msg','isError'));

        }

    }

    public function saveUserAcademicBackgroundAjax(Request $request){

        try{

            //check if request us ajax
            if(!$request->ajax()){
                return response()->json(['error' => [AppConstants::$MSG_BAD_REQUEST_JSON_EXPECTED]], 403);
            }

            //validate request
            $validator = Validator::make($request->all(), [
                'username' => 'required|min:2',
                'institution' => 'required|min:2',
                'year_of_study' => 'required|min:2',
                'award' => 'required|min:2',
            ]);

            //failed validation
            if (!$validator->passes()) {
                return response()->json(['error'=>$validator->errors()->all()]);
            }

            //send request to API
            $data =
                [
                    'username' => $request['username'],
                    'institution' => $request['institution'],
                    'year_of_study' => $request['year_of_study'],
                    'award' => $request['award'],
                ];

            $resp = DataLoader::saveUserAcademicBackground($data);

            // Error occurred on sending the request
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return response()->json(['error' => [$resp->statusDescription]]);
            }

            $successMessage = "RECORD SUCCESSFULLY SAVED";
            return response()->json(['success' => $successMessage]);

        }catch (\Exception $exception){
            return response()->json(['error' => [AppConstants::generalError($exception->getMessage())]]);
        }

    }

    public function updateUserAcademicBackgroundAjax(Request $request){

        try{

            //check if request us ajax
            if(!$request->ajax()){
                return response()->json(['error' => [AppConstants::$MSG_BAD_REQUEST_JSON_EXPECTED]], 403);
            }

            //validate request
            $validator = Validator::make($request->all(), [
                'username' => 'required|min:2',
                'institution' => 'required|min:2',
                'year_of_study' => 'required|min:2',
                'award' => 'required|min:2',
                'record_id' => 'required|numeric',
            ]);

            //failed validation
            if (!$validator->passes()) {
                return response()->json(['error'=>$validator->errors()->all()]);
            }

            //send request to API
            $data =
                [
                    'username' => $request['username'],
                    'institution' => $request['institution'],
                    'year_of_study' => $request['year_of_study'],
                    'award' => $request['award'],
                    'record_id' => $request['record_id'],
                ];

            $identifier = $request['record_id'];
            $resp = DataLoader::saveUserAcademicBackground($data,true, $identifier);

            // Error occurred on sending the request
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return response()->json(['error' => [$resp->statusDescription]]);
            }

            $successMessage = "RECORD SUCCESSFULLY UPDATED";
            return response()->json(['success' => $successMessage]);

        }catch (\Exception $exception){
            return response()->json(['error' => [AppConstants::generalError($exception->getMessage())]]);
        }

    }

    public function showAcademicBg($id){


        try{

            $user = session(Security::$SESSION_USER);
            if($user == null){
                return AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN;
            }

            $baseResp = DataLoader::getUsersAcademicBackgrounds($user->username);

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return AjaxResponseHandler::failedResponse($baseResp->statusDescription);
            }

            $academicBg = null;
            foreach ($baseResp->result as $school){

                if($school->id == $id) {
                    $academicBg = $school;
                    return AjaxResponseHandler::successResponse($academicBg);
                };

            }

            return AjaxResponseHandler::failedResponse("FAILED TO GET ACADEMIC BG WITH ID [".$id."]");

        }catch (\Exception $exception){
            return AjaxResponseHandler::failedResponse(AppConstants::generalError($exception->getMessage()));
        }

    }

    public function deleteAcademicBackground(Request $request){

        try{

            $identifier = $request['id'];
            $user = session(Security::$SESSION_USER);

            /*
             * We failed to get the logged in user
             * */
            if($user == null){
                return $this->getUserProfilePage(AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN, true);
            }

            /*
             * Send request to the API
             * */
            $resp = DataLoader::deleteAcademicBackground([],$identifier);

            /*
             * Error occurred on sending the request
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->getUserProfilePage($resp->statusDescription);
            }

            /*
             * Operation was successful at the server side
             * */
            $successMessage = "INSTITUTION SUCCESSFULLY DELETED";
            return $this->getUserProfilePage($successMessage);


        }catch (\Exception $exception){

            $errorMessage = "Failed To Delete Institution With Error. ".$exception->getMessage();
            return $this->getUserProfilePage($errorMessage,true);

        }


    }


    public function saveUserAjax(Request $request){

        try{


            if(!$request->ajax()){
                return response()->json(['error' => [AppConstants::$MSG_BAD_REQUEST_JSON_EXPECTED]], 403);
            }


            $validator = Validator::make($request->all(), [
                'first_name' => 'required|min:2',
                'last_name' => 'required|min:2',
                'role_code' => 'required',
                'department_code' => 'required',
                'category_code' => 'required',
                'org_code' => 'required',
                'email' => 'required|email',
                'staff_number'=>'required|min:2',
                'contract_start_date'=>'required|date',
                'contract_expiry_date'=>'required|date',
                'date_of_birth'=>'required|date',
                'designation'=>'required|min:2',
                'letter_movement_role'=>'required|min:2',
               ]);


            if (!$validator->passes()) {
                return response()->json(['error'=>$validator->errors()->all()]);
            }

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $data = [

                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'other_name' => $request['other_name'],
                'email' => $request['email'],

                'staff_number' => $request['staff_number'],
                'contract_start_date' => $request['contract_start_date'],
                'contract_expiry_date' => $request['contract_expiry_date'],
                'date_of_birth' => $request['date_of_birth'],
                'designation' => $request['designation'],

                'phone' => $request['phone'],
                'org_code' => $request['org_code'],
                'role_code' => $request['role_code'],
                'department_code' => $request['department_code'],
                'category_code' => $request['category_code'],
                'regional_office_code' => $request['regional_office_code'],
                'created_by' => $request['created_by'],
                'letter_movement_role' => $request['letter_movement_role'],

            ];

            $action = EndPoints::$USERS_STORE;
            $resp = ApiHandler::makePostRequest($action, $data, true, $token);


            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return response()->json(['error'=>[$resp->statusDescription]]);
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return response()->json(['error'=>[$statusDescription]]);
            }

            $successMessage = "USER [".$request['first_name']."] SUCCESSFULLY CREATED";
            return response()->json(['success' => $successMessage]);


        }catch (\Exception $exception){
            return response()->json(['error'=>[AppConstants::generalError($exception->getMessage())]]);
        }

    }

    public function getChangeDefaultPasswordForm($msg = null, $isError = false){

        $active_module = AppConstants::$ACTIVE_MOD_PROFILES;

        /*
         * We store the temporarily logged in user in this session variable
         * */
        $user = session(Security::$SESSION_TEMP_USER);

        if($user == null){
            return redirect('/');
        }

        return view('user.password-change-default',compact('active_module', 'msg','isError','user'));

    }


    public function changeDefaultPassword(ChangeDefaultPasswordRequest $request){

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);
            $tempUser = session(Security::$SESSION_TEMP_USER);

            /*
             * Request data
             * */
            $data = [
                'username' => $tempUser->username,
                'new_password' => $request['new_password'],
            ];

            /*
             * Action
             * */
            $action = EndPoints::$USERS_CHANGE_DEFAULT_PASSWORD;


            /*
             * Send request to the API
             * */
            $resp = ApiHandler::makePostRequest($action, $data, true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                return $this->getChangeDefaultPasswordForm($resp->statusDescription, true);

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);


            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->getChangeDefaultPasswordForm($statusDescription, true);
            }


            /*
             * Operation was successful at the server side
             * */

            /*
             * Destroy the temp user object
             * */
            //$request->session()->forget(Security::$SESSION_TEMP_USER);
           // session()->flush();

            $successMessage = "Password successfully changed, you can now login using your new password";


            /*
             * Return to the form create page with a success message
             * */
            return $this->redirectToLoginPage($request,$successMessage, false);

        }catch (\Exception $exception){

            return $this->getChangeDefaultPasswordForm(AppConstants::$GENERAL_ERROR_AT_TDS. ' ' .$exception->getMessage(), true);

        }

    }

    public function appSelectionView($token, $msg = null, $isError = false){

        try{

            $response = new Response(view('app-selection', compact('msg',$isError)));

            $cookie = cookie(Security::$COOKIE_TOKEN, $token, Security::$COOKIE_TOKEN_EXPIRY_MINUTES);
            $response->withCookie($cookie);

            return $response;

        }catch (\Exception $exception){

            return redirect(route('login'));

        }

    }

    public function accessApp($app){

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            /*
             * Check if the supplied application is valid
             * */
            if(!in_array($app, AppConstants::$SUPPORTED_APPLICATIONS)){
                return $this->appSelectionView($token, "APPLICATION ['.$app.'] NOT SUPPORTED", true);
            }


            /*
             * user wants to access the staff appraisal system, so we redirect them to that
             * */
            if($app == AppConstants::$SUPPORTED_APPLICATIONS_STAFF_APPRAISAL){

                $cookie = cookie(Security::$COOKIE_TOKEN, $token, Security::$COOKIE_TOKEN_EXPIRY_MINUTES);
               return redirect()->route('appraisal-forms.owner')->withCookie($cookie);

            }


            /*
             * user wants to access the letter movement so we redirect them to that
             * */
            if($app == AppConstants::$SUPPORTED_APPLICATIONS_LETTER_MOVEMENT){

                /*
                 * I encrypt the access token
                 * */
                $encryptedCookie = Security::dtEncrypt($token);
                $url = EndPoints::$LETTER_MOVEMENT_APP_END_POINT . $encryptedCookie;

                /*
                 * Redirect to the letter movement app with the token
                 * */
                return Redirect::to($url);

            }

            /*
             * No method to handle supplied application
             * */
            return $this->appSelectionView($token, "NO URL DEFINED TO HANDLE SUPPLIED APPLICATION ['.$app.']", true);


        }catch (\Exception $exception){

            $error = AppConstants::generalError($exception->getMessage());
            $token = Cookie::get(Security::$COOKIE_TOKEN);
            return $this->appSelectionView($token, $error, true);

        }

    }

    public function redirectToLoginPage(Request $request,$msg = null, $isError = false) {

//         $request->session()->forget(Security::$SESSION_TEMP_USER);
        return view('welcome', compact('msg','isError'));

    }


    public function authenticate($key){

        try{

            $token = Security::dtDecrypt($key);
            $resp = DataLoader::getAuthenticatedUser($token);

            /*
             * Authentication failed
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $resp->statusDescription.$token;
            }

            $user = $resp->result;

            /*
             * Save the user in the session
             * */
            session([Security::$SESSION_USER => $user]);
            /*
             * Save the token in a cookie
             * */
            $cookie = cookie(Security::$COOKIE_TOKEN, $token, Security::$COOKIE_TOKEN_EXPIRY_MINUTES);
            return redirect(route('appraisal-forms.owner'))->withCookie($cookie);


        }catch (\Exception $exception){

            $error = AppConstants::generalError($exception->getMessage());
            return $error;

        }

    }



}
