<?php

namespace App\Http\Controllers;

use App\Helpers\AppConstants;
use App\Helpers\ConstAppraisalSections;
use App\Helpers\DataLoader;
use App\Helpers\SharedCommons;
use Illuminate\Http\Request;

class AssessmentSummaryController extends Controller
{

    public function saveSection6A(Request $request) {

        try{

            //get the appraisal form ID
            $appraisalRef = $request['appraisal'];

            $recordId = $request['record_id'];
            $keyResultScoreNoWeight = $request['key_result_score_no_weight'];
            $behavioralCompetenceScoreNoWeight = $request['behavioral_competence_score_no_weight'];
            $keyResultScoreActual = $request['key_result_score_actual'];
            $behavioralCompetenceScoreActual = $request['behavioral_competence_score_actual'];
            $totalScoreActual = $request['total_score_actual'];
            $appraiseeComment = $request['appraisee_comment'];
            $recommendationDecision = $request['recommendation_decision'];
            $appraiserComment = $request['appraiser_comment'];


            $data = [];
            $data['appraisal_reference'] = $appraisalRef;
            $data['key_result_area_score_no_weight'] = $keyResultScoreNoWeight;
            $data['behavioral_score_no_weight'] = $behavioralCompetenceScoreNoWeight;
            $data['key_result_area_score_actual'] = $keyResultScoreActual;
            $data['behavioral_score_actual'] = $behavioralCompetenceScoreActual;
            $data['total_score_actual'] = $totalScoreActual;
            $data['appraisee_comment'] = $appraiseeComment;
            $data['appraiser_recommendation'] = $recommendationDecision;
            $data['appraiser_comment'] = $appraiserComment;


            $baseResp = isset($recordId) && !empty($recordId) ? DataLoader::saveAssessmentSummary($data,true,$recordId) : DataLoader::saveAssessmentSummary($data);

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $msg = $baseResp->statusDescription;
                return $this->redirectBackToFormWithError($msg);
            }

            $appraisal = $baseResp->result;
            return $this->reloadTheAppraisalPage($appraisal, ConstAppraisalSections::SECTION_6B);


        }catch (\Exception $exception){
            $error = AppConstants::generalError($exception->getMessage());
            return $this->redirectBackToFormWithError($error);
        }

    }


    /**
     * @param $error
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectBackToFormWithError($error) {

        return redirect()->back()->withErrors(SharedCommons::customFormError($error))->withInput();

    }

    /**
     * @param $appraisal
     * @param $activeStep
     * @param bool $isUpdate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function reloadTheAppraisalPage($appraisal, $activeStep, $isUpdate = true) {

        $appraisalRef = $appraisal->appraisalRef;
        return redirect(route('open_appraisal',[$appraisalRef, $activeStep, $isUpdate]));
    }



}
