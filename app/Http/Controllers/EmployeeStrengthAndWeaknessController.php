<?php

namespace App\Http\Controllers;

use App\Helpers\AppConstants;
use App\Helpers\ConstAppraisalSections;
use App\Helpers\DataGenerator;
use App\Helpers\DataLoader;
use App\Helpers\Security;
use App\Helpers\SharedCommons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class EmployeeStrengthAndWeaknessController extends Controller
{

    private  $formDataUpdateMessage = "Appraisal form data has been updated successfully";

    public function saveStrengthAndWeakness(Request $request) {

        try{

            /*
             * We are saving
             * */
            if($request->has('save')){
                return $this->save($request);
            }


            /*
             * We are updating
             * */
            return $this->update($request);


        }catch (\Exception $exception){
            $error = AppConstants::generalError($exception->getMessage());
            return $this->redirectBackToFormWithError($error);
        }

    }


    private function save(Request $request) {


        /*
         * We need to save 3 items
         * 1. strengths
         * 2. weaknesses
         * 3. comments
         * */
        $strengths = $request['strengths'];
        $hodComments = $request['hod_comment'];
        $headOfUnitComments = $request['head_of_unit_comment'];
        $weaknesses = [];

        //get the appraisal form ID
        $appraisalRef = $request['appraisal'];

        $data = [];
        $data['appraisal_reference'] = $appraisalRef;


        /*
         * If the variable has data then we send it
         * */
        if(isset($strengths) && !empty($strengths)){
            $data['strengths'] = $strengths;
        }

        if(isset($hodComments) && !empty($hodComments)){
            $data['hod_comment'] = $hodComments;
        }

        if(isset($headOfUnitComments) && !empty($headOfUnitComments)){
            $data['head_of_unit_comment'] = $headOfUnitComments;
        }


        //how do I dynamically get the max_rows passed
        //if it's numeric we take the value, else we assume we did not receive any values
        $countRowsPassed = is_numeric($request['counter_max_rows_weaknesses']) ? $request['counter_max_rows_weaknesses'] : 0;

        //define array to data
        $weaknessesList = [];

        //now we loop through all the rows saving the data therein
        for($i = 1; $i<= $countRowsPassed; $i++){

            $weakness = $request['weakness_'.$i];
            $mitigationMeasures = $request['mitigation_measure_'.$i];
            $selfImprovementPlan = $request['improvement_plan_'.$i];
            $timeFrame = $request['time_frame_'.$i];
            $supervisorComment = $request['supervisor_comment_'.$i];

            //assignment object
            $weaknessData = [];

            /*
             * For the values I only send them when they are not null or empty because if they are null or empty, they will be rejected
             * */
            if(isset($weakness) && !empty($weakness)){ $weaknessData['weakness'] = $weakness; }else{continue;};
            if(isset($mitigationMeasures) && !empty($mitigationMeasures)){ $weaknessData['mitigation_measures'] = $mitigationMeasures; };
            if(isset($selfImprovementPlan) && !empty($selfImprovementPlan)){ $weaknessData['self_improvement_plan'] = $selfImprovementPlan; };
            if(isset($timeFrame) && !empty($timeFrame)){ $weaknessData['time_frame'] = $timeFrame; };
            if(isset($supervisorComment) && !empty($supervisorComment)){ $weaknessData['supervisor_comment'] = $supervisorComment; };

            //Add to list
            $weaknessesList[] = $weaknessData;

        }

        if(isset($weaknessesList) && count($weaknessesList)>0){
            $data['weaknesses'] = $weaknessesList;
        }

        $baseResp = DataLoader::saveStrengthsAndWeaknessesAndComments($data);
        if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            $msg = $baseResp->statusDescription;
            return $this->redirectBackToFormWithError($msg);
        }

        $appraisal = $baseResp->result;
        return $this->reloadTheAppraisalPage($appraisal, ConstAppraisalSections::SECTION_5);

    }


    private function update(Request $request) {


        /*
        * We need to update 3 items
        * 1. strengths
        * 2. weaknesses
        * 3. comments
        * */


        /*
         * Get the strength and their recordID if there
         * */
        $strengths = $request['strengths'];
        $recordIdStrengths = $request['record_id_strengths'];

        /*
         * Get the comments and their record id if there
         * */
        $supervisorComment = $request['hod_comment'];
        $headOfUnitComments = $request['head_of_unit_comment'];
        $recordIdStrengthAndWeaknessComments = $request['record_id_comments'];


        //get the appraisal form ID
        $appraisalRef = $request['appraisal'];

        $data = [];
        $data['appraisal_reference'] = $appraisalRef;


        /*
         * If the variable has data then we send it
         * */
        if(isset($strengths) && !empty($strengths)){
            $data['strengths'] = $strengths;
        }

        if(isset($recordIdStrengths) && !empty($recordIdStrengths)){
            $data['strengths_record_id'] = $recordIdStrengths;
        }


        if(isset($supervisorComment) && !empty($supervisorComment)){
            $data['hod_comment'] = $supervisorComment;
        }

        if(isset($headOfUnitComments) && !empty($headOfUnitComments)){
            $data['head_of_unit_comment'] = $headOfUnitComments;
        }

        if(isset($recordIdStrengthAndWeaknessComments) && !empty($recordIdStrengthAndWeaknessComments)){
            $data['strength_weakness_comment_record_id'] = $recordIdStrengthAndWeaknessComments;
        }


        //how do I dynamically get the max_rows passed
        //if it's numeric we take the value, else we assume we did not receive any values
        $countRowsPassed = is_numeric($request['counter_max_rows_weaknesses']) ? $request['counter_max_rows_weaknesses'] : 0;

        //define array to data
        $weaknessesList = [];

        //now we loop through all the rows saving the data therein
        for($i = 1; $i<= $countRowsPassed; $i++){

            $recordId = $request['record_id_weakness_'.$i];
            $weakness = $request['weakness_'.$i];
            $mitigationMeasures = $request['mitigation_measure_'.$i];
            $selfImprovementPlan = $request['improvement_plan_'.$i];
            $timeFrame = $request['time_frame_'.$i];
            $supervisorComment = $request['supervisor_comment_'.$i];

            //assignment object
            $weaknessData = [];

            /*
             * For the values I only send them when they are not null or empty because if they are null or empty, they will be rejected
             * */
            if(isset($weakness) && !empty($weakness)){ $weaknessData['weakness'] = $weakness; }else{continue;};
            if(isset($recordId) && !empty($recordId)){ $weaknessData['record_id'] = $recordId; };
            if(isset($mitigationMeasures) && !empty($mitigationMeasures)){ $weaknessData['mitigation_measures'] = $mitigationMeasures; };
            if(isset($selfImprovementPlan) && !empty($selfImprovementPlan)){ $weaknessData['self_improvement_plan'] = $selfImprovementPlan; };
            if(isset($timeFrame) && !empty($timeFrame)){ $weaknessData['time_frame'] = $timeFrame; };
            if(isset($supervisorComment) && !empty($supervisorComment)){ $weaknessData['supervisor_comment'] = $supervisorComment; };

            //Add to list
            $weaknessesList[] = $weaknessData;

        }

        if(isset($weaknessesList) && count($weaknessesList)>0){
            $data['weaknesses'] = $weaknessesList;
        }

        $baseResp = DataLoader::saveStrengthsAndWeaknessesAndComments($data);
        if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            $msg = $baseResp->statusDescription;
            return $this->redirectBackToFormWithError($msg);
        }

        $appraisal = $baseResp->result;
        return $this->reloadTheAppraisalPage($appraisal, ConstAppraisalSections::SECTION_5);


    }


    /**
     * @param $error
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectBackToFormWithError($error) {

        return redirect()->back()->withErrors(SharedCommons::customFormError($error))->withInput();

    }

    /**
     * @param $appraisal
     * @param $activeStep
     * @param bool $isUpdate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function reloadTheAppraisalPage($appraisal, $activeStep, $isUpdate = true) {

        $appraisalRef = $appraisal->appraisalRef;
        return redirect(route('open_appraisal',[$appraisalRef, $activeStep, $isUpdate]));
    }

}
