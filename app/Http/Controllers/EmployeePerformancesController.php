<?php

namespace App\Http\Controllers;

use App\Helpers\AppConstants;
use App\Helpers\ConstAppraisalSections;
use App\Helpers\DataGenerator;
use App\Helpers\DataLoader;
use App\Helpers\Security;
use App\Helpers\SharedCommons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class EmployeePerformancesController extends Controller
{

    private  $formDataUpdateMessage = "Appraisal form data has been updated successfully";

    public function saveSection2(Request $request) {

        try{

            /*
             * We are saving
             * */
            if($request->has('save')){
                return $this->section2Save($request);
            }


            /*
             * We are updating
             * */
            return $this->section2Update($request);


        }catch (\Exception $exception){
            $error = AppConstants::generalError($exception->getMessage());
            return $this->redirectBackToFormWithError($error);
        }

    }


    private function section2Save(Request $request) {

        //get the appraisal form ID
        $appraisalRef = $request['appraisal'];

        //how do I dynamically get the max_rows passed
        //if it's numeric we take the value, else we assume we did not receive any values
        $countRowsPassed = is_numeric($request['counter_max_rows_employee_performance']) ? $request['counter_max_rows_employee_performance'] : 0;

        //define array to data
        $performances = [];

        //now we loop through all the rows saving the data therein
        for($i = 1; $i<= $countRowsPassed; $i++){

            $objective = $request['objective_'.$i];
            $keyResultArea = $request['key_result_area_'.$i];
            $keyPerformanceIndicator = $request['key_performance_indicator_'.$i];
            $performanceResult = $request['performance_result_'.$i];
            $maximumRating = $request['maximum_rating_sec_two_'.$i];
            $appraiseeRating = $request['appraisee_rating_sec_two_'.$i];
            $appraiserRating = $request['appraiser_rating_sec_two_'.$i];
            $agreedRating = $request['agreed_rating_sec_two_'.$i];
            $AppraiseeComment = $request['appraisee_comment_section_two_'.$i];
            $supervisorComment = $request['supervisor_comment_section_two_'.$i];


            //assignment object
            $performance = [];
            $performance['strategic_objective_id'] = $objective;
            $performance['key_result_area'] = $keyResultArea;
            $performance['key_performance_indicator'] = $keyPerformanceIndicator;
            $performance['performance_result'] = $performanceResult;


            /*
             * For the values I only send them when they are not null or empty because if they are null or empty, they will be rejected
             * */
            if(isset($maximumRating) && !empty($maximumRating)){ $performance['maximum_assessment'] = $maximumRating; };
            if(isset($appraiseeRating) && !empty($appraiseeRating)){ $performance['appraisee_assessment'] = $appraiseeRating; };
            if(isset($appraiserRating) && !empty($appraiserRating)){ $performance['supervisor_assessment'] = $appraiserRating; };
            if(isset($agreedRating) && !empty($agreedRating)){ $performance['agreed_assessment'] = $agreedRating; };
            if(isset($AppraiseeComment) && !empty($AppraiseeComment)){ $performance['appraisee_comment'] = $AppraiseeComment; };
            if(isset($supervisorComment) && !empty($supervisorComment)){ $performance['supervisor_comment'] = $supervisorComment; };

            //Add to list
            $performances[] = $performance;

        }

        //we now have to save these institutes using the API
        $token = Cookie::get(Security::$COOKIE_TOKEN);
        $data = [];
        $data['appraisal_reference'] = $appraisalRef;
        $data['employee_performances'] = $performances;

        $baseResp = DataLoader::saveEmployeePerformances($token, $data);

        if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            $msg = $baseResp->statusDescription;
            return $this->redirectBackToFormWithError($msg);
        }


        /*
         * Begin the saving of the scores
         * */

        $dataNext = [];
        $dataNext['appraisal_reference'] = $appraisalRef;

        /*
         * Please note, I am assuming these fields can be saved later on, e.g the appraiser total may be empty
         * therefore i dont pass them to API when they empty or null because if I pass them
         * they will fail validation as only numbers are expected
         * */
        $sec2TotalMaximumRating = $request['sec_2_total_maximum_rating'];
        $sec2TotalAppraiseeRating = $request['sec_2_total_appraisee_rating'];
        $sec2TotalAppraiserRating = $request['sec_2_total_appraiser_rating'];
        $sec2TotalAgreedRating = $request['sec_2_total_agreed_rating'];
        $supervisorComment = $request['supervisor_comment'];


        if(isset($sec2TotalMaximumRating) && !empty($sec2TotalMaximumRating)){
            $dataNext['total_maximum_assessment'] = $sec2TotalMaximumRating;
        }
        if(isset($sec2TotalAppraiseeRating) && !empty($sec2TotalAppraiseeRating)){
            $dataNext['total_appraisee_assessment'] = $sec2TotalAppraiseeRating;
        }
        if(isset($sec2TotalAppraiserRating) && !empty($sec2TotalAppraiserRating)){
            $dataNext['total_supervisor_assessment'] = $sec2TotalAppraiserRating;
        }
        if(isset($sec2TotalAgreedRating) && !empty($sec2TotalAgreedRating)){
            $dataNext['total_agreed_assessment'] = $sec2TotalAgreedRating;
        }
        if(isset($supervisorComment) && !empty($supervisorComment)){
            $dataNext['supervisor_comment'] = $supervisorComment;
        }


        $baseRespNext = DataLoader::saveEmployeePerformanceScores($dataNext);

        if($baseRespNext->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            $msg = $baseRespNext->statusDescription;
            return $this->redirectBackToFormWithError($msg);
        }

        $appraisal = $baseRespNext->result;

        /*
         * End saving of the scores
         * */
        return $this->reloadTheAppraisalPage($appraisal,ConstAppraisalSections::SECTION_3);

    }


    private function section2Update(Request $request) {

        //do update

        //get the appraisal form ID
        $appraisalRef = $request['appraisal'];

        //how do I dynamically get the max_rows passed
        //if it's numeric we take the value, else we assume we did not receive any values
        $countRowsPassed = is_numeric($request['counter_max_rows_employee_performance']) ? $request['counter_max_rows_employee_performance'] : 0;


        //define array to data
        $performances = [];

        //now we loop through all the rows saving the data therein
        for($i = 1; $i<= $countRowsPassed; $i++){

            $objective = $request['objective_'.$i];
            $keyResultArea = $request['key_result_area_'.$i];
            $keyPerformanceIndicator = $request['key_performance_indicator_'.$i];
            $performanceResult = $request['performance_result_'.$i];
            $maximumRating = $request['maximum_rating_sec_two_'.$i];
            $appraiseeRating = $request['appraisee_rating_sec_two_'.$i];
            $appraiserRating = $request['appraiser_rating_sec_two_'.$i];
            $agreedRating = $request['agreed_rating_sec_two_'.$i];
            $appraiseeComment = $request['appraisee_comment_section_two_'.$i];
            $supervisorComment = $request['supervisor_comment_section_two_'.$i];


            //assignment object
            $performance = [];
            $performance['strategic_objective_id'] = $objective;
            $performance['key_result_area'] = $keyResultArea;
            $performance['key_performance_indicator'] = $keyPerformanceIndicator;
            $performance['performance_result'] = $performanceResult;


            /*
             * For the values I only send them when they are not null or empty because if they are null or empty, they will be rejected
             * */
            if(isset($maximumRating) && !empty($maximumRating)){ $performance['maximum_assessment'] = $maximumRating; };
            if(isset($appraiseeRating) && !empty($appraiseeRating)){ $performance['appraisee_assessment'] = $appraiseeRating; };
            if(isset($appraiserRating) && !empty($appraiserRating)){ $performance['supervisor_assessment'] = $appraiserRating; };
            if(isset($agreedRating) && !empty($agreedRating)){ $performance['agreed_assessment'] = $agreedRating; };
            if(isset($appraiseeComment) && !empty($appraiseeComment)){ $performance['appraisee_comment'] = $appraiseeComment; };
            if(isset($supervisorComment) && !empty($supervisorComment)){ $performance['supervisor_comment'] = $supervisorComment; };

            $recordId = $request['record_id_'.$i];

            //if no recordId, then it's a new being added dynamically
            if(isset($recordId)){
                $performance['record_id'] = $recordId;
            }

            //Add to list
            $performances[] = $performance;

        }

        //we now have to save data using the API
        $token = Cookie::get(Security::$COOKIE_TOKEN);
        $data = [];
        $data['appraisal_reference'] = $appraisalRef;
        $data['employee_performances'] = $performances;

        $baseResp = DataLoader::saveEmployeePerformances($token, $data,true);

        if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            $msg = $baseResp->statusDescription;
            return $this->redirectBackToFormWithError($msg);
        }


        //Assignment scores begin

        $recordId = $request['scores_sec_2_record_id'];

        $dataNext = [];
        $dataNext['record_id'] = $recordId;
        $dataNext['appraisal_reference'] = $appraisalRef;

        $sec2TotalMaximumRating = $request['sec_2_total_maximum_rating'];
        $sec2TotalAppraiseeRating = $request['sec_2_total_appraisee_rating'];
        $sec2TotalAppraiserRating = $request['sec_2_total_appraiser_rating'];
        $sec2TotalAgreedRating = $request['sec_2_total_agreed_rating'];
        $supervisorComment = $request['supervisor_comment'];

        if(isset($sec2TotalMaximumRating) && !empty($sec2TotalMaximumRating)){
            $dataNext['total_maximum_assessment'] = $sec2TotalMaximumRating;
        }
        if(isset($sec2TotalAppraiseeRating) && !empty($sec2TotalAppraiseeRating)){
            $dataNext['total_appraisee_assessment'] = $sec2TotalAppraiseeRating;
        }
        if(isset($sec2TotalAppraiserRating) && !empty($sec2TotalAppraiserRating)){
            $dataNext['total_supervisor_assessment'] = $sec2TotalAppraiserRating;
        }
        if(isset($sec2TotalAgreedRating) && !empty($sec2TotalAgreedRating)){
            $dataNext['total_agreed_assessment'] = $sec2TotalAgreedRating;
        }
        if(isset($supervisorComment) && !empty($supervisorComment)){
            $dataNext['supervisor_comment'] = $supervisorComment;
        }


        /*
         * If the record ID is not set, then we saving for the first time else we are updating
         * */
        $baseRespNext = !isset($recordId) ? DataLoader::saveEmployeePerformanceScores($dataNext) : DataLoader::saveEmployeePerformanceScores($dataNext, true, $recordId);

        if($baseRespNext->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
            $msg = $baseRespNext->statusDescription;
            return $this->redirectBackToFormWithError($msg);
        }
        $appraisal = $baseRespNext->result;

        //Assignment scores end

        return $this->reloadTheAppraisalPage($appraisal,ConstAppraisalSections::SECTION_3);


    }



    /**
     * @param $error
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectBackToFormWithError($error) {

        return redirect()->back()->withErrors(SharedCommons::customFormError($error))->withInput();

    }


    /**
     * @param $appraisal
     * @param $activeStep
     * @param bool $isUpdate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function reloadTheAppraisalPage($appraisal, $activeStep, $isUpdate = true) {

        $appraisalRef = $appraisal->appraisalRef;
        return redirect(route('open_appraisal',[$appraisalRef, $activeStep, $isUpdate]));
    }

}
