<?php

namespace App\Http\Controllers;

use App\Helpers\AppConstants;
use App\Helpers\ConstAppraisalStatus;
use App\Helpers\DataLoader;
use App\Helpers\Security;
use App\Models\ApiAppraisalReq;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HumanResourceController extends Controller
{

    public function __construct(){
        /**in this case the middleware named auth is applied
        to every single function within this controller
         */
        $this->middleware('users.hr');
    }

    public function indexAppraisalsFilter($incompleteAppraisalStatusFilter = null){

            return $this->index(null,false, $incompleteAppraisalStatusFilter);

    }

    public function index($msg = null, $isError = false, $incompleteAppraisalStatusFilter = null){

        try{

            //we attempt to get the users
            $baseResp = DataLoader::allUsers();

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $error =$baseResp->statusDescription;
                return $this->getHumanResourceView([],[], $error,true);
            }

            $users = $baseResp->result;


            //we now attempt to get the appraisals completed
            $req = new ApiAppraisalReq();
            $req->token = Cookie::get(Security::$COOKIE_TOKEN);;
            $req->workflowRole = AppConstants::WORK_FLOW_ROLE_ALL;
            $req->status = ConstAppraisalStatus::COMPLETED_SUCCESSFULLY;

            $baseRespNext = DataLoader::getUserAppraisals($req);

            if($baseRespNext->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->getHumanResourceView($users,[], $baseRespNext->statusDescription, true);
            }

            $appraisalsCompleted =  $baseRespNext->result;

            /*
             * Get incomplete appraisal
             * */
            $req = new ApiAppraisalReq();
            $req->token = Cookie::get(Security::$COOKIE_TOKEN);
            $req->workflowRole = AppConstants::WORK_FLOW_ROLE_ALL;
            $req->status = 'incomplete';
            if(isset($incompleteAppraisalStatusFilter)){
                $req->additionStatusFilter = $incompleteAppraisalStatusFilter;
            }

            $incompleteAppraisalsResp = DataLoader::getUserAppraisals($req);
            $appraisalsIncomplete = $incompleteAppraisalsResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS ? [] : $incompleteAppraisalsResp->result;

            /*
             * Get incomplete pending ED Approval appraisal
             * */
            $req = new ApiAppraisalReq();
            $req->token = Cookie::get(Security::$COOKIE_TOKEN);
            $req->workflowRole = AppConstants::WORK_FLOW_ROLE_ALL;
            $req->status = 'incomplete_pending_ed_approval';

            $incompletePendingEdAppraisalsResp = DataLoader::getUserAppraisals($req);
            $appraisalsIncompletePendingEd = $incompletePendingEdAppraisalsResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS ? [] : $incompletePendingEdAppraisalsResp->result;

            $appraisals = array(
                'incomplete' => $appraisalsIncomplete,
                'completed' => $appraisalsCompleted,
                'incomplete_pending_ed' => $appraisalsIncompletePendingEd,
            );


            return $this->getHumanResourceView($users, $appraisals,$msg,$isError,$incompleteAppraisalStatusFilter);

        }catch (\Exception $exception){
            return $this->getHumanResourceView([], [], AppConstants::generalError($exception->getMessage()),true);
        }

    }

    private function getHumanResourceView($users = [], $appraisalsData = [], $msg = null, $isError = null,$incompleteAppraisalStatusFilter=null) {

        $completeAppraisal = $appraisalsData == [] || !array_key_exists('completed', $appraisalsData) ? [] : $appraisalsData['completed'];
        $incompleteAppraisal = $appraisalsData == [] || !array_key_exists('incomplete', $appraisalsData) ? [] : $appraisalsData['incomplete'];
        $incompleteAppraisalPendingEd = $appraisalsData == [] || !array_key_exists('incomplete_pending_ed', $appraisalsData) ? [] : $appraisalsData['incomplete_pending_ed'];

        $data = array(
            'active_module' => AppConstants::$ACTIVE_MOD_HUMAN_RESOURCE,
            'users' => $users,
            'appraisals' =>$completeAppraisal,
            'appraisalsIncomplete' => $incompleteAppraisal,
            'appraisalsIncompletePendingEd' => $incompleteAppraisalPendingEd,
            'msg' => $msg,
            'isError' => $isError,
            'statusFilter' => $incompleteAppraisalStatusFilter,
        );

        return view('hr.hr-page')->with($data);

    }

    public function getUserContractsView($username){

        try{

            $user = session(Security::$SESSION_USER);
            if($user == null){
                return AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN;
            }

            $baseResp = DataLoader::getUsersContracts($username);

            if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $baseResp->statusDescription;
            }

            $contracts = $baseResp->result;
            $author = $user;
            return view('hr.modal-hr-contract-history-content', compact('author','username','contracts'));

        }catch (\Exception $exception){
            return AppConstants::generalError($exception->getMessage());
        }

    }


    public function getUserProfileAndAcademicBgView($username){

        try{

            /*
             * Check if the user is still logged in
             * */
            $authUser = session(Security::$SESSION_USER);
            if($authUser == null){
                return AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN;
            }

            /*
             * Get the user details
             * */
            $baseRespUserDetails = DataLoader::getUserByUsername($username);
            if($baseRespUserDetails->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $baseRespUserDetails->statusDescription;
            }


            /*
             * Get the academic backgrounds of the user
             * */
            $baseRespAcademicBg = DataLoader::getUsersAcademicBackgrounds($username);
            if($baseRespAcademicBg->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $baseRespAcademicBg->statusDescription;
            }


            $academicBackgrounds = $baseRespAcademicBg->result;
            $user = $baseRespUserDetails->result;

            return view('hr.modal-hr-academic-bg-content', compact('user','username','academicBackgrounds'));

        }catch (\Exception $exception){
            return AppConstants::generalError($exception->getMessage());
        }

    }



    public function saveUserContractAjax(Request $request){

        try{

            //check if request us ajax
            if(!$request->ajax()){
                return response()->json(['error' => [AppConstants::$MSG_BAD_REQUEST_JSON_EXPECTED]], 403);
            }

            //validate request
            $validator = Validator::make($request->all(), [
                'username' => 'required|min:2',
                'contract_start_date' => 'required|date',
                'contract_expiry_date' => 'required|date',
                'created_by' => 'required|min:2',
            ]);

            //failed validation
            if (!$validator->passes()) {
                return response()->json(['error'=>$validator->errors()->all()]);
            }

            //send request to API
            $data =
                [
                    'username' => $request['username'],
                    'contract_reference' => $request['contract_reference'],
                    'start_date' => $request['contract_start_date'],
                    'expiry_date' => $request['contract_expiry_date'],
                    'created_by' => $request['created_by'],
                ];

            $resp = DataLoader::saveUserContract($data);

            // Error occurred on sending the request
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return response()->json(['error' => [$resp->statusDescription]]);
            }

            $successMessage = "CONTRACT SUCCESSFULLY SAVED";
            return response()->json(['success' => $successMessage]);

        }catch (\Exception $exception){
            return response()->json(['error' => [AppConstants::generalError($exception->getMessage())]]);
        }

    }

    public function updateUserContractAjax(Request $request){

        try{

            //check if request us ajax
            if(!$request->ajax()){
                return response()->json(['error' => [AppConstants::$MSG_BAD_REQUEST_JSON_EXPECTED]], 403);
            }

            //validate request
            $validator = Validator::make($request->all(), [
                'username' => 'required|min:2',
                'contract_start_date' => 'required|date',
                'contract_expiry_date' => 'required|date',
                'record_id' => 'required|numeric',
            ]);

            //failed validation
            if (!$validator->passes()) {
                return response()->json(['error'=>$validator->errors()->all()]);
            }

            //send request to API
            $data =
                [
                    'username' => $request['username'],
                    'contract_reference' => $request['contract_reference'],
                    'start_date' => $request['contract_start_date'],
                    'expiry_date' => $request['contract_expiry_date'],
                    'record_id' => $request['record_id'],
                ];

            $identifier = $request['record_id'];
            $resp = DataLoader::saveUserContract($data, true, $identifier);

            // Error occurred on sending the request
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return response()->json(['error' => [$resp->statusDescription]]);
            }

            $successMessage = "CONTRACT DETAILS SUCCESSFULLY UPDATED";
            return response()->json(['success' => $successMessage]);

        }catch (\Exception $exception){
            return response()->json(['error' => [AppConstants::generalError($exception->getMessage())]]);
        }

    }


    public function deleteUserContract(Request $request){

        try{

            $identifier = $request['id'];
            $user = session(Security::$SESSION_USER);

            /*
             * We failed to get the logged in user
             * */
            if($user == null){
                return $this->index(AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN, true);
            }

            /*
             * Send request to the API
             * */
            $resp = DataLoader::deleteUserContract([],$identifier);

            /*
             * Error occurred on sending the request
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->index($resp->statusDescription, true);
            }

            /*
             * Operation was successful at the server side
             * */
            $successMessage = "CONTRACT DETAILS SUCCESSFULLY DELETED";
            return $this->index($successMessage);


        }catch (\Exception $exception){

            $errorMessage = "Failed To Delete Contract Details With Error. ".$exception->getMessage();
            return $this->index($errorMessage,true);

        }


    }

}
