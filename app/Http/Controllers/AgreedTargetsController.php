<?php

namespace App\Http\Controllers;

use App\Helpers\AppConstants;
use App\Helpers\ConstAppraisalSections;
use App\Helpers\DataLoader;
use App\Helpers\Security;
use App\Helpers\SharedCommons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class AgreedTargetsController extends Controller
{

    private  $formDataUpdateMessage = "Appraisal form data has been updated successfully";

    public function saveSection5(Request $request) {

        try{

            if($request->has('save')){
                //do saving

                //get the appraisal form ID
                $appraisalRef = $request['appraisal'];

                //how do I dynamically get the max_rows passed
                //if it's numeric we take the value, else we assume we did not receive any values
                $countRowsPassed = is_numeric($request['counter_max_rows_agreed_targets']) ? $request['counter_max_rows_agreed_targets'] : 0;

                //define array to data
                $workPlans = [];

                //now we loop through all the rows saving the data therein
                for($i = 1; $i<= $countRowsPassed; $i++){

                    $assignment = $request['key_result_area_'.$i];
                    $expectedOutput = $request['key_performance_indicator_'.$i];
                    $maxRating = $request['max_rating'.$i];
                    $timeFrame = $request['time_frame_'.$i];

                    //assignment object
                    $plan = [];
                    $plan['key_result_area'] = $assignment;
                    $plan['key_performance_indicator'] = $expectedOutput;
                    $plan['maximum_rating'] = $maxRating;
                    $plan['time_frame'] = $timeFrame;
                    //Add to list
                    $workPlans[] = $plan;

                }

                //we now have to save these institutes using the API
                $token = Cookie::get(Security::$COOKIE_TOKEN);
                $data = [];
                $data['appraisal_reference'] = $appraisalRef;
                $data['agreed-targets'] = $workPlans;

                $baseResp = DataLoader::saveAgreedTargets($token, $data);

                if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                    $msg = $baseResp->statusDescription;
                    return $this->redirectBackToFormWithError($msg);
                }

                $appraisal = $baseResp->result;
                return $this->reloadTheAppraisalPage($appraisal,ConstAppraisalSections::SECTION_6A);

            }
            else{
                //do update

                //get the appraisal form ID
                $appraisalRef = $request['appraisal'];

                //how do I dynamically get the max_rows passed
                //if it's numeric we take the value, else we assume we did not receive any values
                $countRowsPassed = is_numeric($request['counter_max_rows_agreed_targets']) ? $request['counter_max_rows_agreed_targets'] : 0;


                //define array to data
                $workPlans = [];

                //now we loop through all the rows saving the data therein
                for($i = 1; $i<= $countRowsPassed; $i++){

                    $assignment = $request['key_result_area_'.$i];
                    $expectedOutput = $request['key_performance_indicator_'.$i];
                    $maxRating = $request['max_rating'.$i];
                    $timeFrame = $request['time_frame_'.$i];

                    // object
                    $plan = [];
                    $plan['key_result_area'] = $assignment;
                    $plan['key_performance_indicator'] = $expectedOutput;
                    $plan['maximum_rating'] = $maxRating;
                    $plan['time_frame'] = $timeFrame;

                    $recordId = $request['record_id_'.$i];

                    //if no recordId, then it's a new being added dynamically
                    if(isset($recordId)){
                        $plan['record_id'] = $recordId;
                    }

                    //Add to list
                    $workPlans[] = $plan;

                }

                //we now have to save data using the API
                $token = Cookie::get(Security::$COOKIE_TOKEN);
                $data = [];
                $data['appraisal_reference'] = $appraisalRef;
                $data['agreed-targets'] = $workPlans;

                $baseResp = DataLoader::saveAgreedTargets($token, $data,true);


                if($baseResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                    $msg = $baseResp->statusDescription;
                    return $this->redirectBackToFormWithError($msg);

                }

                $appraisal = $baseResp->result;
                return $this->reloadTheAppraisalPage($appraisal,ConstAppraisalSections::SECTION_6A);

            }

        }catch (\Exception $exception){
            /*An exception occurred on saving the form, we need to redirect back with the error */
            $error = AppConstants::generalError($exception->getMessage());
            return $this->redirectBackToFormWithError($error);

        }

    }


    /**
     * @param $error
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectBackToFormWithError($error) {

        return redirect()->back()->withErrors(SharedCommons::customFormError($error))->withInput();

    }

    /**
     * @param $appraisal
     * @param $activeStep
     * @param bool $isUpdate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function reloadTheAppraisalPage($appraisal, $activeStep, $isUpdate = true) {

        $appraisalRef = $appraisal->appraisalRef;
        return redirect(route('open_appraisal',[$appraisalRef, $activeStep, $isUpdate]));
    }

}
