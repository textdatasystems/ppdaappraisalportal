<?php

namespace App\Http\Controllers;

use App\Helpers\ApiHandler;
use App\Helpers\AppConstants;
use App\Helpers\ConstAppraisalSections;
use App\Helpers\DataFormatter;
use App\Helpers\DataGenerator;
use App\Helpers\EndPoints;
use App\Helpers\Security;
use App\Helpers\SharedCommons;
use App\Http\Requests\SectionARequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class PersonalDetailController extends Controller
{

    private  $formDataUpdateMessage = "Appraisal form data has been updated successfully";

    public function saveSection1(SectionARequest $request){

        /*
         * Check if we are saving for the first time
         * */
        if($request->has('save')){

            return $this->section1Save($request);

        }
        /*
         * Here we are updating
         * */


        return $this->section1Update($request);

    }


    private function section1Save(Request $request) {

        try{

            /*
             * Save the info via the API
             * */
            $token = Cookie::get(Security::$COOKIE_TOKEN);
            $user = session(Security::$SESSION_USER);

            /*
             * We failed to get the logged in user
             * */
            if($user == null){
                return $this->redirectBackToFormWithError(AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN);
            }

            /*
             * Request data
             * */
            $data = [
                'owner_id' => $user->username,
                'appraisal_type' => $request['appraisal_type'],
                'supervisor_id' => $request['supervisor'],
                'department_head_id' => $request['hod'],
                'executive_director_id' => $request['ed'],
                'person_info_staff_number' => $request['staff_file_number'],
                'person_info_first_name' => $request['other_name'],
                'person_info_last_name' => $request['surname'],
                'person_info_department' => $request['department'],
                'person_info_designation' => $request['designation'],
                'person_info_employee_category' => $request['employee_category'],
                'person_info_date_of_birth' => $request['dob'],
                'person_info_appraisal_period_start_date' => $request['appraisal_start_date'],
                'person_info_appraisal_period_end_date' => $request['appraisal_end_date'],
                'person_info_contract_start_date' => $request['contract_start_date'],
                'person_info_contract_expiry_date' => $request['contract_expiry_date'],
                'appraisal_workflow_type' => $request['appraisal_workflow_type'],
            ];

            /*
             * Action
             * */
            $action = EndPoints::$APPRAISAL_SAVE_APPRAISAL;


            /*
             * Send request to the API
             * */
            $resp = ApiHandler::makePostRequest($action, $data, true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->redirectBackToFormWithError($resp->statusDescription);
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);


            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->redirectBackToFormWithError($statusDescription);
            }


            /*
             * Operation was successful at the server side
             * */
            $apiAppraisal = json_decode($apiResult['data'],true);
            $appraisal = DataFormatter::getApiAppraisal($apiAppraisal);
            return $this->reloadTheAppraisalPage($appraisal, ConstAppraisalSections::SECTION_2,false);


        }catch (\Exception $exception){

            /*An exception occurred on saving the form, we need to redirect back with the error */

            $error = AppConstants::generalError($exception->getMessage());
            return $this->redirectBackToFormWithError($error);

        }


    }

    private function section1Update(Request $request) {


        try{

            /*
             * Save the info via the API
             * */

            $appraisalRef = $request['appraisal'];
            $identifier = $request['code'];
            $token = Cookie::get(Security::$COOKIE_TOKEN);
            $user = session(Security::$SESSION_USER);

            /*
             * We failed to get the logged in user
             * */
            if($user == null){
                return $this->redirectBackToFormWithError(AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN);
            }

            /*
             * Request data
             * */
            $data = [
                'appraisal_reference' => $appraisalRef,
                'staff_number' => $request['staff_file_number'],
                'first_name' => $request['other_name'],
                'last_name' => $request['surname'],
                'department' => $request['department'],
                'designation' => $request['designation'],
                'date_of_birth' => $request['dob'],
                'appraisal_type' => $request['appraisal_type'],
                'supervisor_id' => $request['supervisor'],
                'department_head_id' => $request['hod'],
                'executive_director_id' => $request['ed'],
                'appraisal_period_start_date' => $request['appraisal_start_date'],
                'appraisal_period_end_date' => $request['appraisal_end_date'],
                'contract_start_date' => $request['contract_start_date'],
                'contract_expiry_date' => $request['contract_expiry_date'],
                'employee_category' => $request['employee_category'],
            ];

            /*
             * Action
             * */
            $action = EndPoints::$APPRAISAL_PERSONAL_DETAILS_UPDATE;


            /*
             * Send request to the API
             * */
            $resp = ApiHandler::makePutRequest($action,$identifier, $data, true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->redirectBackToFormWithError($resp->statusDescription);
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);


            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $this->redirectBackToFormWithError($statusDescription);
            }


            /*
             * Operation was successful at the server side
             * */
            $apiAppraisal = json_decode($apiResult['data'],true);
            $appraisal = DataFormatter::getApiAppraisal($apiAppraisal);
            return $this->reloadTheAppraisalPage($appraisal, ConstAppraisalSections::SECTION_2);


        }catch (\Exception $exception){

            /*An exception occurred on saving the form, we need to redirect back with the error */
            $error = AppConstants::generalError($exception->getMessage());
            return $this->redirectBackToFormWithError($error);

        }


    }



    /**
     * @param $error
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectBackToFormWithError($error) {

        return redirect()->back()->withErrors(SharedCommons::customFormError($error))->withInput();

    }


    /**
     * @param $appraisal
     * @param $activeStep
     * @param bool $isUpdate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function reloadTheAppraisalPage($appraisal, $activeStep, $isUpdate = true) {

        $appraisalRef = $appraisal->appraisalRef;
        return redirect(route('open_appraisal',[$appraisalRef, $activeStep, $isUpdate]));
    }

}
