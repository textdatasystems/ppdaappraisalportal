<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/19/2019
 * Time: 14:09
 */


namespace app;


class RepositoryResp {

    public $statusCode = "";
    public $statusDescription = "";
    public $repoData;

}