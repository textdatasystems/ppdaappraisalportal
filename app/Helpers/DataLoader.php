<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/24/2019
 * Time: 13:35
 */


namespace App\Helpers;


use app\ApiResp;
use app\Models\ApiAppraisalReq;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

class DataLoader {

    public static function getStrategicObjectives($token) {

        $baseResp = new ApiResp();

        try{

            $action = EndPoints::$APPRAISAL_STRATEGIC_OBJECTIVES_ALL;

            $resp = ApiHandler::makeGetRequest($action, true, $token, null,EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            $baseResp->result = DataFormatter::formatStrategicObjectives($apiResult['data']);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function getCategorizedCompetences($token, $appraisalRef = 'NEW', $competenceAssessments = []) {

        $baseResp = new ApiResp();

        try{

            $action = EndPoints::$APPRAISAL_COMPETENCE_CATEGORIES_ALL;

            $resp = ApiHandler::makeGetRequest($action, true, $token, $appraisalRef);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            $baseResp->result = DataFormatter::formatCategorizedCompetences($apiResult['data'], $competenceAssessments);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }



    public static function getBehavioralCompetenceCategories($token, $competenceAssessments = []) {

        $baseResp = new ApiResp();

        try{

            $action = EndPoints::$BEHAVIORAL_COMPETENCE_CATEGORIES;

            $resp = ApiHandler::makeGetRequest($action, true, $token /*,$appraisalRef*/);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            $baseResp->result = DataFormatter::formatBehaviorCompetenceCategories($apiResult['data'], $competenceAssessments);

            return $baseResp;


        }catch (\Exception $exception){
            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function getUserAppraisals(ApiAppraisalReq $req) {

        $baseResp = new ApiResp();

        try{

            /*
             * Build the body
             * */
            $data = [ 'work_flow_role' => $req->workflowRole ];

            if(isset($req->status)){
                $data['status'] = $req->status;
            }
            if(isset($req->additionStatusFilter)){
                $data['additionStatusFilter'] = $req->additionStatusFilter;
            }
            if(isset($req->startDate)){
                $data['start_date'] = $req->startDate;
            }
            if(isset($req->endDate)){
                $data['end_date'] = $req->endDate;
            }
            if(isset($req->supervisorDecision)){
                $data['supervisor_decision'] = $req->supervisorDecision;
            }
            if(isset($req->hodDecision)){
                $data['department_head_decision'] = $req->hodDecision;
            }
            if(isset($req->directorDecision)){
                $data['director_decision'] = $req->directorDecision;
            }

            /*
             * Action
             * */
            $action = EndPoints::$APPRAISAL_ALL;

            /*
             * Make the request
             * */
            $resp = ApiHandler::makePostRequest($action,$data, true, $req->token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            $baseResp->result = DataFormatter::formatAppraisals($apiResult['data']);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::$GENERAL_ERROR_AT_TDS;
            return $baseResp;
        }

    }

    public static function saveAcademicInstitutes($token, $data, $update = false) {

        $baseResp = new ApiResp();

        try{

            $action = !$update ?  EndPoints::$APPRAISAL_ACADEMIC_BG_SAVE : EndPoints::$APPRAISAL_ACADEMIC_BG_UPDATE;

            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveKeyDuties($token, $data, $update = false) {

        $baseResp = new ApiResp();

        try{

            $action = !$update ?  EndPoints::$APPRAISAL_KEY_DUTIES_SAVE : EndPoints::$APPRAISAL_KEY_DUTIES_UPDATE;

            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveAssignments($token, $data, $update = false) {

        $baseResp = new ApiResp();

        try{

            $action = !$update ?  EndPoints::$APPRAISAL_ASSIGNMENTS_SAVE : EndPoints::$APPRAISAL_ASSIGNMENTS_UPDATE;

            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveCompetenceAssessments($token, $data, $update = false) {

        $baseResp = new ApiResp();

        try{

            $action = !$update ?  EndPoints::$APPRAISAL_COMPETENCE_ASSESSMENT_SAVE : EndPoints::$APPRAISAL_COMPETENCE_ASSESSMENT_UPDATE;

            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function savePerformanceGaps($token, $data, $update = false) {

        $baseResp = new ApiResp();

        try{

            $action = !$update ?  EndPoints::$APPRAISAL_PERFORMANCE_GAPS_SAVE : EndPoints::$APPRAISAL_PERFORMANCE_GAPS_UPDATE;

            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function savePerformanceChallenges($token, $data, $update = false) {

        $baseResp = new ApiResp();

        try{

            $action = !$update ?  EndPoints::$APPRAISAL_PERFORMANCE_CHALLENGES_SAVE : EndPoints::$APPRAISAL_PERFORMANCE_CHALLENGES_UPDATE;

            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveAdditionalAssignments($token, $data, $update = false) {

        $baseResp = new ApiResp();

        try{

            $action = !$update ?  EndPoints::$APPRAISAL_ADDITIONAL_ASSIGNMENTS_SAVE : EndPoints::$APPRAISAL_ADDITIONAL_ASSIGNMENTS_UPDATE;

            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveWorkPlans($token, $data, $update = false) {

        $baseResp = new ApiResp();

        try{

            $action = !$update ?  EndPoints::$APPRAISAL_WORK_PLAN_SAVE : EndPoints::$APPRAISAL_WORK_PLAN_UPDATE;

            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function getTopThreeLatestAppraisals($token) {

        $baseResp = new ApiResp();

        try{

            $action = EndPoints::$APPRAISAL_TOP_3;

            $resp = ApiHandler::makeGetRequest($action, true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            $baseResp->result = $apiResult['data'];

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function getAppraisalByAppraisalReference($appraisalRef,$token = null) {

        $baseResp = new ApiResp();

        try{

            if(is_null($token)){
                $token = Cookie::get(Security::$COOKIE_TOKEN);
            }

            /*
             * We have failed to get an access token
             * */
            if(is_null($token)){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN;
                return $baseResp;
            }


            $identifier = $appraisalRef;
            $action = EndPoints::$APPRAISAL_GET;

            $resp = ApiHandler::makeGetRequest($action, true, $token, $identifier);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $appraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($appraisal);

            return $baseResp;

        }catch (\Exception $exception){
            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveAppraiserComment($data, $update = false,$identifier = null) {


        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_APPRAISER_COMMENT_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_APPRAISER_COMMENT_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveStrengthsAndWeaknesses($data, $update = false,$identifier = null) {


        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_STRENGTH_AND_WEAKNESS_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_STRENGTH_AND_WEAKNESS_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveAppraiserRecommendation($data, $update = false,$identifier = null) {


        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_APPRAISER_RECOMMENDATION_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_APPRAISER_RECOMMENDATION_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveSupervisorDeclaration($data, $update = false, $identifier = null) {


        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_SUPERVISOR_DECLARATION_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_SUPERVISOR_DECLARATION_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveHodComment($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_HOD_COMMENT_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_HOD_COMMENT_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveAssignmentsScore($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_ASSIGNMENTS_SCORES_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_ASSIGNMENTS_SCORES_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveAdditionalAssignmentsScore($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_ADDITIONAL_ASSIGNMENTS_SCORES_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_ADDITIONAL_ASSIGNMENTS_SCORES_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveAssignmentsSummary($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_ASSIGNMENTS_SUMMARY_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_ASSIGNMENTS_SUMMARY_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveTrainingSummary($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_PERFORMANCE_SUMMARY_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_PERFORMANCE_SUMMARY_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveCompetenceAssessmentsScore($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_COMPETENCE_ASSESSMENT_SCORES_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_COMPETENCE_ASSESSMENT_SCORES_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveCompetenceAssessmentsSummary($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_COMPETENCE_ASSESSMENT_SUMMARY_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_COMPETENCE_ASSESSMENT_SUMMARY_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function saveAppraiseeRemarks($data, $update = false,$identifier = null) {


        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_APPRAISEE_REMARK_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_APPRAISEE_REMARK_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveDirectorComment($data, $update = false,$identifier = null) {


        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_DIRECTOR_COMMENT_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_DIRECTOR_COMMENT_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function allUsers() {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);
            $resp = ApiHandler::makeGetRequest(EndPoints::$USERS_ALL, true, $token,null, EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * We didn't get a response from API
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                /*
                 * Return error
                 * */
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp->statusDescription;
            }

            /*
             * We got a response from the API
             * */
            $apiResp = json_decode($resp->result, true);

            /*
             * Get statusCode, statusDescription
             * */
            $statusCode = $apiResp['statusCode'];
            $statusDescription = $apiResp['statusDescription'];

            /*
             * Check if got a success on fetching the data from the API
             * */
            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            /*
             * We got the data from the API
             * */
            $data = $apiResp['data'];

            /*
             * Format data
             * */
            $users = DataFormatter::formatUsers($data);

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            $baseResp->result = $users;

            return $baseResp;


        }catch (\Exception $exception){
            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception->getMessage());
            return $baseResp;
        }

    }

    public static function appraisalWorkFlowStart($data) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $action = EndPoints::$APPRAISAL_WORKFLOW_START;
            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            //$apiAppraisal = json_decode($apiResult['data'],true);
            //$baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function appraisalWorkFlowMove($data) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $action = EndPoints::$APPRAISAL_WORKFLOW_MOVE;
            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            //$apiAppraisal = json_decode($apiResult['data'],true);
            //$baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function updateUserProfileByOwner($data, $username) {


        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $action = EndPoints::$USERS_UPDATE_PROFILE_BY_USER;
            $resp = ApiHandler::makePutRequest($action, $username, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $baseResp->result = DataFormatter::getApiUser($apiResult['data']);

            //update the user in the session
            session([Security::$SESSION_USER => $baseResp->result]);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function saveUserAcademicBackground($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$USERS_USER_ACADEMIC_BG_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token, EndPoints::$BASE_URL_USER_MANAGEMENT);
            }else{
                $action = EndPoints::$USERS_USER_ACADEMIC_BG_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token, EndPoints::$BASE_URL_USER_MANAGEMENT);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function saveAdminCompetenceCategory($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$COMPETENCE_CATEGORY_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$COMPETENCE_CATEGORY_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }



    public static function updateAppraisalApprovers($data) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $action = EndPoints::$APPRAISAL_UPDATE_APPROVERS;
            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }



    public static function saveAdminCompetence($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$COMPETENCES_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$COMPETENCES_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function deleteAcademicBackground($data, $identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $action = EndPoints::$USERS_USER_ACADEMIC_BG_DELETE;
            $resp = ApiHandler::makeDeleteRequest($action, $identifier, $data,true, $token, EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function deleteUserContract($data, $identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $action = EndPoints::$USER_CONTRACT_DELETE;
            $resp = ApiHandler::makeDeleteRequest($action, $identifier, $data,true, $token,EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }



    public static function saveUserContract($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$USER_CONTRACT_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token,EndPoints::$BASE_URL_USER_MANAGEMENT);
            }else{
                $action = EndPoints::$USER_CONTRACT_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token,EndPoints::$BASE_URL_USER_MANAGEMENT);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            /* $apiAppraisal = json_decode($apiResult['data'],true);
             $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);*/
            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function saveStrategicObjective($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$APPRAISAL_STRATEGIC_OBJECTIVES_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$APPRAISAL_STRATEGIC_OBJECTIVES_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function deleteStrategicObjective($data, $identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $action = EndPoints::$APPRAISAL_STRATEGIC_OBJECTIVES_DELETE;
            $resp = ApiHandler::makeDeleteRequest($action, $identifier, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function getUsersAcademicBackgrounds($username) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            /*
             * We have failed to get an access token
             * */
            if(is_null($token)){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN;
                return $baseResp;
            }

            $action = EndPoints::$USERS_USER_ACADEMIC_BG_ALL;
            $resp = ApiHandler::makeGetRequest($action, true, $token, $username, EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $baseResp->result = DataFormatter::formatUserAcademicBackgrounds($apiResult['data']);

            return $baseResp;

        }catch (\Exception $exception){
            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function getUsersContracts($username) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            /*
             * We have failed to get an access token
             * */
            if(is_null($token)){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN;
                return $baseResp;
            }

            $action = EndPoints::$USER_CONTRACT_ALL;
            $resp = ApiHandler::makeGetRequest($action, true, $token, $username,EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $baseResp->result = DataFormatter::formatUserContracts($apiResult['data']);

            return $baseResp;

        }catch (\Exception $exception){
            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function getAdminCompetenceCategories($token) {

        $baseResp = new ApiResp();

        try{

            $action = EndPoints::$COMPETENCE_CATEGORY_ALL;

            $resp = ApiHandler::makeGetRequest($action, true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            $baseResp->result = DataFormatter::formatAdminCompetenceCategories($apiResult['data']);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function deleteCompetenceCategory($data, $identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $action = EndPoints::$COMPETENCE_CATEGORY_DELETE;
            $resp = ApiHandler::makeDeleteRequest($action, $identifier, $data,true, $token);

            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }



    public static function getOrganizations() {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);
            $resp = ApiHandler::makeGetRequest(EndPoints::$ORGANIZATIONS_ALL, true, $token);

            /*
             * Failed to get a response from the server
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $resp->statusDescription;
            }


            $apiResp = json_decode($resp->result, true);;
            $statusCode = $apiResp['statusCode'];
            $statusDescription = $apiResp['statusDescription'];

            /*
             * Failed to get the organizations from the server
             * */
            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $statusDescription;
            }

            $organizations = DataFormatter::formatOrganizations($apiResp['data']);

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            $baseResp->result = $organizations;

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function getEmployeeCategories() {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);
            $resp = ApiHandler::makeGetRequest(EndPoints::$EMPLOYEE_CATEGORIES_ALL, true, $token);

            /*
             * Failed to get a response from the server
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $resp->statusDescription;
            }


            $apiResp = json_decode($resp->result, true);;
            $statusCode = $apiResp['statusCode'];
            $statusDescription = $apiResp['statusDescription'];

            /*
             * Failed to get the organizations from the server
             * */
            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $statusDescription;
            }

            $employeeCategories = DataFormatter::formatEmployeeCategories($apiResp['data']);

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            $baseResp->result = $employeeCategories;

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function getAdminCompetencesForACompetenceCategory($competenceCategoryId) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            /*
             * We have failed to get an access token
             * */
            if(is_null($token)){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN;
                return $baseResp;
            }

            $action = EndPoints::$COMPETENCES_FOR_CATEGORY_ID;
            $resp = ApiHandler::makeGetRequest($action, true, $token, $competenceCategoryId);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $baseResp->result = DataFormatter::formatAdminCompetences($apiResult['data']);

            return $baseResp;

        }catch (\Exception $exception){
            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function deleteCompetenceByAdmin($data, $identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $action = EndPoints::$COMPETENCES_DELETE;
            $resp = ApiHandler::makeDeleteRequest($action, $identifier, $data,true, $token);

            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function saveErrorLog($data) {

        $baseResp = new ApiResp();

        try{

            $action = EndPoints::$ERROR_LOG_SAVE;
            $resp = ApiHandler::makePostRequest($action, $data);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function appraisalCancel($appraisalRef, $token = null) {

        $baseResp = new ApiResp();

        try{

            if(is_null($token)){
                $token = Cookie::get(Security::$COOKIE_TOKEN);
            }

            /*
             * We have failed to get an access token
             * */
            if(is_null($token)){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = AppConstants::$MSG_SESSION_TIMEOUT_LOGIN_AGAIN;
                return $baseResp;
            }

            $identifier = $appraisalRef;
            $action = EndPoints::$APPRAISAL_WORKFLOW_CANCEL;

            $resp = ApiHandler::makeGetRequest($action, true, $token, $identifier);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = $statusDescription;
            return $baseResp;

        }catch (\Exception $exception){
            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function getAuthenticatedUser($token) {

        $baseResp = new ApiResp();

        try{

            $action = EndPoints::$USERS_GET_AUTHENTICATED_USER;
            $resp = ApiHandler::makeGetRequest($action, true,$token, null, EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->result = DataFormatter::getApiUser($apiResult['data']);
            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function saveEmployeePerformances($token, $data, $update = false) {

        $baseResp = new ApiResp();

        try{

            $action = !$update ?  EndPoints::$EMPLOYEE_PERFORMANCES_SAVE : EndPoints::$EMPLOYEE_PERFORMANCES_UPDATE;

            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function saveEmployeePerformanceScores($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$EMPLOYEE_PERFORMANCE_SCORES_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$EMPLOYEE_PERFORMANCE_SCORES_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }



    public static function saveStrengthsAndWeaknessesAndComments($data) {


        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $action = EndPoints::$STRENGTH_AND_WEAKNESS;
            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function saveAgreedTargets($token, $data, $update = false) {

        $baseResp = new ApiResp();

        try{

            $action = !$update ?  EndPoints::$APPRAISAL_AGREED_TARGETS_SAVE : EndPoints::$APPRAISAL_AGREED_TARGETS_UPDATE;

            $resp = ApiHandler::makePostRequest($action, $data,true, $token);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function saveAssessmentSummary($data, $update = false,$identifier = null) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            if(!$update){
                $action = EndPoints::$ASSESSMENT_SUMMARIES_SAVE;
                $resp = ApiHandler::makePostRequest($action, $data,true, $token);
            }else{
                $action = EndPoints::$ASSESSMENT_SUMMARIES_UPDATE;
                $resp = ApiHandler::makePutRequest($action, $identifier, $data,true, $token);
            }

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;

            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;

            $apiAppraisal = json_decode($apiResult['data'],true);
            $baseResp->result = DataFormatter::getApiAppraisal($apiAppraisal);

            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function getUserByUsername($username) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $resp = ApiHandler::makeGetRequest(EndPoints::$USERS_SHOW, true, $token,$username,EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * We didn't get a response from API
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                /*
                 * Return error
                 * */
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp->statusDescription;
            }

            /*
             * We got a response from the API
             * */
            $apiResp = json_decode($resp->result, true);

            /*
             * Get statusCode, statusDescription
             * */
            $statusCode = $apiResp['statusCode'];
            $statusDescription = $apiResp['statusDescription'];

            /*
             * Check if got a success on fetching the data from the API
             * */
            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            /*
             * We got the data from the API
             * */
            $data = $apiResp['data'];

            /*
             * Format data
             * */
            $user = DataFormatter::getApiUser($data);

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            $baseResp->result = $user;

            return $baseResp;


        }catch (\Exception $exception){
            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception->getMessage());
            return $baseResp;
        }

    }



    public static function workFlowReturnToPreviousStatus($data) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $action = EndPoints::$APPRAISAL_WORKFLOW_RETURN_TO_PREVIOUS_STATUS;
            $resp = ApiHandler::makePostRequest($action, $data,true, $token,EndPoints::$BASE_URL);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;


        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;

        }

    }


    public static function downloadAppraisalPDF($appraisalRef) {

        $baseResp = new ApiResp();

        try{

            $token = Cookie::get(Security::$COOKIE_TOKEN);

            $action = '/appraisals/generate-pdf';
            $resp = ApiHandler::makeGetRequest($action, true, $token, $appraisalRef);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = $statusDescription;
            $baseResp->result = $statusDescription;
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;

        }

    }



}