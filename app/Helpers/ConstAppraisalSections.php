<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/13/19
 * Time: 10:02 AM
 */

namespace App\Helpers;


class ConstAppraisalSections
{

    const SECTION_1 = "SEC_1";
    const SECTION_2 = "SEC_2";
    const SECTION_3 = "SEC_3";
    const SECTION_4 = "SEC_4";
    const SECTION_5 = "SEC_5";
    const SECTION_6A = "SEC_6A";
    const SECTION_6B = "SEC_6B";
    const SECTION_6C = "SEC_6C";
    const SECTION_6D = "SEC_6D";
    const SECTION_6E = "SEC_6E";

}