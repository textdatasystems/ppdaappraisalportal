<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 7/12/2019
 * Time: 00:42
 */


namespace App\Models;


class ApiFormAgreedTarget {

    public $id;
    public $keyResultArea;
    public $keyPerformanceIndicator;
    public $maximumRating;
    public $timeFrame;

}