<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/30/2019
 * Time: 23:28
 */


namespace app\Models;


class ApiFormAppraiseeRemark {


    public $id;
    public $appraiseeName;
    public $agreementDecision;
    public $disagreementReason;
    public $declarationName;
    public $declarationInitials;
    public $declarationDate;

}