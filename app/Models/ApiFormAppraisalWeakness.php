<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 7/11/2019
 * Time: 16:55
 */


namespace App\Models;


class ApiFormAppraisalWeakness {

    public $id;
    public $appraisalReference;
    public $weakness;
    public $mitigationMeasures;
    public $selfImprovementPlan;
    public $supervisorComment;
    public $timeFrame;

}
