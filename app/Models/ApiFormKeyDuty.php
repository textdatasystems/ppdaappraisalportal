<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/30/2019
 * Time: 13:03
 */


namespace app\Models;


class ApiFormKeyDuty {

    public $id;
    public $objectiveId;
    public $objective;
    public $jobAssignment;
    public $expectedOutput;
    public $maximumRating;
    public $timeFrame;

}