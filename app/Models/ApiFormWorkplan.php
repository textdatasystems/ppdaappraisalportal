<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/31/2019
 * Time: 11:29
 */


namespace app\Models;


class ApiFormWorkplan {

    public $id;
    public $jobAssignment;
    public $expectedOutput;
    public $maximumRating;
    public $timeFrame;

}
