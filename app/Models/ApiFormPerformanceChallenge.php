<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/31/2019
 * Time: 10:21
 */


namespace app\Models;


class ApiFormPerformanceChallenge {


    public $id;
    public $challenge;
    public $causes;
    public $recommendation;
    public $when;

}
