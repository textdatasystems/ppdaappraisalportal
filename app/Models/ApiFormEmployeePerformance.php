<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 7/11/2019
 * Time: 10:43
 */


namespace App\Models;


class ApiFormEmployeePerformance {


    public $id;
    public $appraisalReference;
    public $strategicObjectiveId;
    public $keyResultArea;
    public $keyPerformanceIndicator;
    public $performanceResult;
    public $supervisorAssessment;

    public $maximumAssessment;
    public $appraiseeAssessment;
    public $agreedAssessment;
    public $appraiseeComment;
    public $supervisorComment;


}