<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 7/11/2019
 * Time: 16:56
 */


namespace App\Models;


class ApiFormAppraisalWeaknessAndStrengthComment {

    public $id;
    public $appraisalReference;
    public $headOfUnitComment;
    public $hodComment;

}