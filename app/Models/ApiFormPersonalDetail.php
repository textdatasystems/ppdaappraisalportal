<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/29/2019
 * Time: 10:31
 */


namespace app\Models;


class ApiFormPersonalDetail {

    public $id = "";
    public $appraisalPeriodStartDate = "";
    public $appraisalPeriodEndDate = "";
    public $appraisalReference = "";
    public $contractStartDate = "";
    public $contractExpiryDate = "";
    public $createdAt = "";
    public $dateOfBirth= "";
    public $department = "";
    public $designation = "";
    public $firstName = "";
    public $lastName = "";
    public $otherName = "";
    public $staffNumber = "";
    public $employeeCategory = "";
    public $updatedAt = "";

}


