<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 7/11/2019
 * Time: 11:50
 */


namespace App\Models;


class ApiFormEmployeePerformanceScore {

    public $id;
    public $appraisalReference;
    public $totalMaximumAssessment;
    public $totalAppraiseeAssessment;
    public $totalSupervisorAssessment;
    public $totalAgreedAssessment;
    public $supervisorComment;

}
