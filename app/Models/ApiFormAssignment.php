<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/30/2019
 * Time: 16:48
 */


namespace app\Models;


class ApiFormAssignment {

    public $id;
    public $objectiveId;
    public $expectedOutput;
    public $actualPerformance;
    public $maximumRating;
    public $appraiseeRating;
    public $appraiserRating;
    public $agreedRating;

}