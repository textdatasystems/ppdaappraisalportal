<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 7/10/2019
 * Time: 21:58
 */


namespace App\Models;


class ApiBehavioralCompetenceCategory {

    public $id;
    public $category;
    public $categoryCode;
    public $maximumRating;
    public $behavioralCompetences;

}