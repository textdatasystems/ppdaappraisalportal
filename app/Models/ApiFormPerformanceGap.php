<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/31/2019
 * Time: 10:20
 */


namespace app\Models;


class ApiFormPerformanceGap {

    public $id;
    public $performanceGap;
    public $causes;
    public $recommendation;
    public $when;

}
