<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/24/2019
 * Time: 13:47
 */


namespace app\Models;


class ApiStrategicObjective {

    public $id = "";
    public $objective = "";
    public $orgCode = "";
    public $orgName = "";
    public $createdBy = "";

}