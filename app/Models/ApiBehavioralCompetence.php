<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 7/10/2019
 * Time: 21:58
 */


namespace App\Models;


class ApiBehavioralCompetence {

    public $id;
    public $categoryCode;
    public $category;
    public $competence;
    public $maximumScore;

    /*
     * These are used only when the user has already filled in the scores, i.e on updating,
     * we populate them on fetching the form in case of update
     * */
    public $categoryMaximumScore;
    public $scoreAppraiseeRating;
    public $scoreAppraiserRating;
    public $scoreAgreedRating;
    public $scoreRecordId;

}