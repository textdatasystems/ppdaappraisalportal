<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/24/2019
 * Time: 13:49
 */


namespace app\Models;


class ApiCompetence {

    public $id;
    public $competenceCategoryId;
    public $categoryDesc;
    public $competence;
    public $rank;
    public $rating;

}