<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/30/2019
 * Time: 23:25
 */


namespace app\Models;


class ApiFormSupervisorDeclaration {

    public $id;
    public $appraiserName;
    public $appraiseeName;
    public $duration;
    public $startDate;
    public $endDate;


}