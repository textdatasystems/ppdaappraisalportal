<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/24/2019
 * Time: 13:49
 */


namespace app\Models;


class ApiCompetenceCategory {

    public $id;
    public $orgCode;
    public $orgName;
    public $empCategoryCode;
    public $competenceCategory;
    public $maxRating;
    public $createdBy;

}