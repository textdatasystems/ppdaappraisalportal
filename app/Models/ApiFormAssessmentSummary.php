<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 7/12/2019
 * Time: 01:50
 */


namespace App\Models;


class ApiFormAssessmentSummary {

    public $id;
    public $appraisalReference;
    public $keyResultAreaScoreNoWeight;
    public $behavioralScoreNoWeight;
    public $keyResultAreaScoreActual;
    public $behavioralScoreActual;
    public $totalScoreActual;
    public $appraiseeComment;
    public $appraiserRecommendation;
    public $appraiserComment;

}
