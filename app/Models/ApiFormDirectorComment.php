<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/30/2019
 * Time: 23:30
 */


namespace app\Models;


class ApiFormDirectorComment {

    public $id;
    public $name;
    public $initials;
    public $date;
    public $comments;

}