<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use app\Helpers\EndPoints;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

Route::group(['middleware' => ['web']], function (){


    Route::get('/users/auth/{key}',
        [
            'uses' => 'UserController@authenticate',
            'as' => 'users.auth'
        ]);

    /*
     * This loads the home page
     * */
    Route::get('/', function () {
        return Redirect::to(EndPoints::$USER_MANAGEMENT_LOGOUT_LINK);
    })->name('login');


    /*
     * This is for processing the user login
     * */
    Route::post('/signin',
        [
            'uses' => 'UserController@signin',
            'as' => 'signin'
        ]);

    Route::get('/forgot-password',
        [
            'uses' => 'UserController@getResetPasswordForm',
            'as' => 'user.forgot-password'
        ]);


    Route::post('/reset-password',
        [
            'uses' => 'UserController@resetPassword',
            'as' => 'user.reset-password'
        ]);


    Route::get('/password/change-auto',
        [
            'uses' => 'UserController@getChangeDefaultPasswordForm',
            'as' => 'user.password.change-auto.form'
        ]);


    Route::post('/password-change-auto-password',
        [
            'uses' => 'UserController@changeDefaultPassword',
            'as' => 'user.password.change-default'
        ]);

});



/*
 * Routes below are only accessible by authorized users see, AuthorizedUserAccess middleware for logic
 * */

Route::group(['middleware' => ['web', 'users.authorized' /*'auth'*/]], function (){



    Route::get('/users/access-app/{app}',
        [
            'uses' => 'UserController@accessApp',
            'as' => 'users.access-app'
        ]);


    Route::get('/admin/dashboard',
        [
            'uses' => 'UserController@getAdminDashboard',
            'as' => 'admin_dashboard'
        ]);

    Route::get('/users/dashboard',
        [
            'uses' => 'UserController@getUserDashboard',
            'as' => 'user_dashboard'
        ]);


    Route::get('/users/profile',
        [
            'uses' => 'UserController@getUserProfilePage',
            'as' => 'users.profile'
        ]);

    Route::get('/users/academic-bg/{id}',
        [
            'uses' => 'UserController@showAcademicBg',
            'as' => 'users.profile.academic-bg'
        ]);


    Route::post('/users/update-profile',
        [
            'uses' => 'UserController@updateUserProfileByOwner',
            'as' => 'users.update-own-profile'
        ]);

    Route::post('/users/academic-bg-save',
        [
            'uses' => 'UserController@saveUserAcademicBackgroundAjax',
            'as' => 'users.academic-bg-save'
        ]);

    Route::post('/users/academic-bg-update',
        [
            'uses' => 'UserController@updateUserAcademicBackgroundAjax',
            'as' => 'users.academic-bg-update'
        ]);

    Route::get('/users/academic-bg/delete/{id}',
        [
            'uses' => 'UserController@deleteAcademicBackground',
            'as' => 'users.academic-bg.delete'
        ]);

    Route::get('/admin/signout',
        [
            'uses' => 'UserController@signoutAdmin',
            'as' => 'singout_admin'
        ]);

    Route::get('/user/back-to-ppda-apps',
        [
            'uses' => 'UserController@logoutAndRedirectBackToPpdaApps',
            'as' => 'back-to-ppda-apps'
        ]);


    /*
     * Begin routes for users
     * */
    Route::get('/users/new',
        [
            'uses' => 'UserController@getCreationUserForm',
            'as' => 'users.form'
        ]);

    Route::post('/users/store',
        [
            'uses' => 'UserController@saveUserAjax',
            'as' => 'users.store'
        ]);

    Route::get('/users',
        [
            'uses' => 'UserController@allUsers',
            'as' => 'users.all'
        ]);

    Route::post('/users/edit',
        [
            'uses' => 'UserController@updateUser',
            'as' => 'users.update'
        ]);

    Route::get('/users/password',
        [
            'uses' => 'UserController@getPasswordChangeForm',
            'as' => 'users.change-password-form'
        ]);

    Route::post('/users/password/edit',
        [
            'uses' => 'UserController@changePassword',
            'as' => 'change_password'
        ]);


    Route::get('/users/delete/{id}',
        [
            'uses' => 'UserController@deleteProfile',
            'as' => 'delete_user_profile'
        ]);

    /*
     * End routes for users
     * */




    Route::get('/documenttypes',
        [
            'uses' => 'DocumentTypesController@documentTypes',
            'as' => 'document_types'
        ]);

    Route::get('/documenttypes/create/form',
        [
            'uses' => 'DocumentTypesController@getNewDocumentTypeForm',
            'as' => 'create_document_type_form'
        ]);

    Route::get('/documenttypes/create/add',
        [
            'uses' => 'DocumentTypesController@getAddNewDocumentTypeForm',
            'as' => 'add_document_type_form'
        ]);

    Route::post('/documenttypes/add_new_document_type',
        [
            'uses' => 'DocumentTypesController@addNewDocumentType',
            'as' => 'add_new_document_type'
        ]);

    Route::post('/documenttypes/create',
        [
            'uses' => 'DocumentTypesController@createDocumentType',
            'as' => 'create_or_update_doc_type'
        ]);

    Route::get('/documenttypes/delete/{id}',
        [
            'uses' => 'DocumentTypesController@deleteDocumentType',
            'as' => 'delete_document_type'
        ]);

    Route::post('/documenttypes/edit',
        [
            'uses' => 'DocumentTypesController@editDocumentType',
            'as' => 'edit_document_type'
        ]);

    Route::get('/documenttypes/download/html/{id}',
        [
            'uses' => 'DocumentTypesController@downloadHtml',
            'as' => 'download_document_type_html_template'
        ]);
    Route::get('/documenttypes/download/pdf/{id}',
        [
            'uses' => 'DocumentTypesController@downloadPdf',
            'as' => 'download_document_type_pdf_template'
        ]);

    Route::get('/library',
        [
            'uses' => 'DirectoryManagerController@getDirectoryStructure',
            'as' => 'library'
        ]);

    Route::get('/library/open/{directory}',
        [
            'uses' => 'DirectoryManagerController@openDirectory',
            'as' => 'open_directory'
        ]);



    /*
     * Begin routes for organizations
     * */

    Route::get('/settings/organizations',
        [
            'uses' => 'SettingsController@allOrganizations',
            'as' => 'organization.all'
        ]);

    Route::get('/settings/organizations/new',
        [
            'uses' => 'SettingsController@getCreationOrganizationForm',
            'as' => 'organization.form'
        ]);

    Route::post('/settings/organizations/store',
        [
            'uses' => 'SettingsController@saveOrgAjax',
            'as' => 'organization.store'
        ]);

    Route::post('/settings/organizations/edit',
        [
            'uses' => 'SettingsController@editOrganization',
            'as' => 'organizations.update'
        ]);

    Route::get('/settings/organizations/delete/{id}',
        [
            'uses' => 'SettingsController@deleteOrganization',
            'as' => 'organizations.delete'
        ]);

    /*
     * End routes for organizations
     * */



    /*
 * Begin routes for regional offices
 * */

    Route::get('/settings/regional-offices',
        [
            'uses' => 'SettingsController@allRegionalOffices',
            'as' => 'regional-offices.all'
        ]);

    Route::get('/settings/regional-offices/new',
        [
            'uses' => 'SettingsController@getCreationRegionalOfficeForm',
            'as' => 'regional-offices.form'
        ]);

    Route::post('/settings/regional-offices/store',
        [
            'uses' => 'SettingsController@saveRegionalOfficeAjax',
            'as' => 'regional-offices.store'
        ]);

    Route::post('/settings/regional-offices/edit',
        [
            'uses' => 'SettingsController@editRegionalOffice',
            'as' => 'regional-offices.update'
        ]);

    Route::get('/settings/regional-offices/delete/{id}',
        [
            'uses' => 'SettingsController@deleteRegionalOffice',
            'as' => 'regional-offices.delete'
        ]);


    /*
     * End routes for regional offices
     * */



    /*
     * Begin routes for departments
     * */
    Route::get('/settings/departments',
        [
            'uses' => 'SettingsController@allDepartments',
            'as' => 'departments.all'
        ]);

    Route::get('/settings/departments/new',
        [
            'uses' => 'SettingsController@getCreationDepartmentForm',
            'as' => 'departments.form'
        ]);

    Route::post('/settings/departments/store',
        [
            'uses' => 'SettingsController@saveDepartmentAjax',
            'as' => 'departments.store'
        ]);

    Route::post('/settings/departments/edit',
        [
            'uses' => 'SettingsController@editDepartment',
            'as' => 'departments.update'
        ]);

    Route::get('/settings/departments/delete/{id}',
        [
            'uses' => 'SettingsController@deleteDepartment',
            'as' => 'departments.delete'
        ]);

    /*
     * End routes for departments
     * */


    /*
     * Begin Strategic Objectives
     * */
    Route::get('/settings/strategic-objectives',
        [
            'uses' => 'SettingsController@allStrategicObjectives',
            'as' => 'admin.objectives.all'
        ]);
    Route::post('/settings/strategic-objectives-save',
        [
            'uses' => 'SettingsController@saveStrategicObjectiveAjax',
            'as' => 'admin.strategic-objectives.save'
        ]);

    Route::post('/settings/strategic-objectives-update',
        [
            'uses' => 'SettingsController@updateStrategicObjectiveAjax',
            'as' => 'admin.strategic-objectives.update'
        ]);
    Route::get('/settings/strategic-objectives/delete/{id}',
        [
            'uses' => 'SettingsController@deleteStrategicObjective',
            'as' => 'admin.strategic-objectives.delete'
        ]);
    /*
     * End Strategic Objectives
     * */


    /*
     * Begin Admin Compentece Categories
     * */
    Route::get('/settings/competence-categories',
        [
            'uses' => 'SettingsController@allCompetenceCategories',
            'as' => 'admin.competence-categories.all'
        ]);

    Route::post('/settings/competence-categories-save',
        [
            'uses' => 'SettingsController@saveCompetenceCategoryAjax',
            'as' => 'admin.competence-categories.save'
        ]);

    Route::post('/settings/competence-categories-update',
        [
            'uses' => 'SettingsController@updateCompetenceCategoryAjax',
            'as' => 'admin.competence-categories.update'
        ]);

    Route::get('/settings/competence-categories/{id}/competences',
        [
            'uses' => 'SettingsController@getCompetenceCategoryCompetences',
            'as' => 'admin.competence-categories.show-competences'
        ]);

    Route::get('/settings/competence-categories/delete/{id}',
        [
            'uses' => 'SettingsController@deleteCompetenceCategory',
            'as' => 'admin.competence-categories.delete'
        ]);


    /*
     * Begin competence
     * */
    Route::post('/settings/competences-save',
        [
            'uses' => 'SettingsController@saveCompetenceAjax',
            'as' => 'admin.competences.save'
        ]);

    Route::post('/settings/competences-update',
        [
            'uses' => 'SettingsController@updateCompetenceAjax',
            'as' => 'admin.competences.update'
        ]);

    Route::get('/settings/competences/delete/{categoryId}/{id}',
        [
            'uses' => 'SettingsController@deleteCompetence',
            'as' => 'admin.competences.delete'
        ]);

    Route::get('/settings/appraisals/incomplete/{statusFilter?}',
        [
            'uses' => 'SettingsController@getAdminIncompleteAppraisals',
            'as' => 'admin.appraisals.incomplete'
        ]);

    Route::get('/settings/appraisals/complete',
        [
            'uses' => 'SettingsController@getAdminCompletedAppraisals',
            'as' => 'admin.appraisals.complete'
        ]);

    Route::post('/settings/appraisals/update-approvers',
        [
            'uses' => 'SettingsController@updateAppraisalApproversAjax',
            'as' => 'admin.appraisals.update-approvers'
        ]);


    /*
     * Edn Admin Competence Cateogries
     * */


    /*
    * Begin routes for role codes
    * */

    Route::get('/settings/role-codes',
        [
            'uses' => 'SettingsController@allRoleCodes',
            'as' => 'role-codes.all'
        ]);

    Route::get('/settings/role-codes/new',
        [
            'uses' => 'SettingsController@getCreationRoleCodeForm',
            'as' => 'role-codes.form'
        ]);

    Route::post('/settings/role-codes/store',
        [
            'uses' => 'SettingsController@saveRoleCodeAjax',
            'as' => 'role-codes.store'
        ]);

    Route::post('/settings/role-codes/edit',
        [
            'uses' => 'SettingsController@editRoleCode',
            'as' => 'role-codes.update'
        ]);

    Route::get('/settings/role-codes/delete/{id}',
        [
            'uses' => 'SettingsController@deleteRoleCode',
            'as' => 'role-codes.delete'
        ]);

    /*
     * End routes for role codes
     * */



    /*
    * Begin routes for employee categories
    * */

    Route::get('/settings/employee-categories',
        [
            'uses' => 'SettingsController@allEmployeeCategories',
            'as' => 'employee-categories.all'
        ]);

    Route::get('/settings/employee-categories/new',
        [
            'uses' => 'SettingsController@getCreationEmployeeCategoryForm',
            'as' => 'employee-categories.form'
        ]);

    Route::post('/settings/employee-categories/store',
        [
            'uses' => 'SettingsController@saveEmployeeCategoryAjax',
            'as' => 'employee-categories.store'
        ]);

    Route::post('/settings/employee-categories/edit',
        [
            'uses' => 'SettingsController@editEmployeeCategory',
            'as' => 'employee-categories.update'
        ]);

    Route::get('/settings/employee-categories/delete/{id}',
        [
            'uses' => 'SettingsController@deleteEmployeeCategory',
            'as' => 'employee-categories.delete'
        ]);

    /*
     * End routes for  employee categories
     * */
    Route::get('/human-resource',
        [
            'uses' => 'HumanResourceController@index',
            'as' => 'human-resource.index'
        ]);

    Route::get('/human-resource/filtered/{incompleteAppraisalStatusFilter?}',
        [
            'uses' => 'HumanResourceController@indexAppraisalsFilter',
            'as' => 'human-resource.index.filtered'
        ]);

    Route::get('/human-resource/user-contracts/{username}',
        [
            'uses' => 'HumanResourceController@getUserContractsView',
            'as' => 'human-resource.user-contracts'
        ]);

    Route::post('/human-resource/user-contract',
        [
            'uses' => 'HumanResourceController@saveUserContractAjax',
            'as' => 'human-resource.user-contract.store'
        ]);

    Route::post('/human-resource/user-contract-update',
        [
            'uses' => 'HumanResourceController@updateUserContractAjax',
            'as' => 'human-resource.user-contract.update'
        ]);

    Route::get('/human-resource/user-contract-delete/{id}',
        [
            'uses' => 'HumanResourceController@deleteUserContract',
            'as' => 'human-resource.user-contract.delete'
        ]);

    Route::get('/human-resource/user-academic-bg/{username}',
        [
            'uses' => 'HumanResourceController@getUserProfileAndAcademicBgView',
            'as' => 'human-resource.user-academic-bg'
        ]);


    /*
     * Begin Human Resource routes
     * */

    /*
     * End Human Resource routes
     * */




    /*
     * Appraisal Routes
     * */

    Route::get('/appraisal-form/my-appraisals',
        [
            'uses' => 'AppraisalController@getOwnerAppraisals',
            'as' => 'appraisal-forms.owner'
        ]);

    Route::get('/appraisal-form/supervisor-appraisals',
        [
            'uses' => 'AppraisalController@getSupervisorAppraisals',
            'as' => 'appraisal-forms.supervisor'
        ]);

    Route::get('/appraisal-form/head-of-department-appraisals',
        [
            'uses' => 'AppraisalController@getHodAppraisals',
            'as' => 'appraisal-forms.hod'
        ]);

    Route::get('/appraisal-form/executive-director-appraisals',
        [
            'uses' => 'AppraisalController@getDirectorAppraisals',
            'as' => 'appraisal-forms.director'
        ]);



    Route::get('/appraisal/{appraisalRef?}/{activeStep?}/{fromUpdateAction?}',
        [
            'uses' => 'AppraisalController@index',
            'as' => 'open_appraisal'
        ]);

    Route::get('/appraisal-form/view/{id}',
        [
            'uses' => 'AppraisalController@show',
            'as' => 'appraisal-forms.show'
        ]);

    Route::get('/appraisal-form/pdf/{appraisalRef}', "AppraisalController@getLetterDownloadLink")->name('download-appraisal-pdf');

    Route::post('/appraisal-form/save-section-1',
        [
            'uses' => 'PersonalDetailController@saveSection1',
            'as' => 'save_section_1'
        ]);

    Route::post('/appraisal-form/save-section-b',
        [
            'uses' => 'AppraisalController@saveSectionB',
            'as' => 'save_section_b'
        ]);

    Route::post('/appraisal-form/save-section-c',
        [
            'uses' => 'AppraisalController@saveSectionC',
            'as' => 'save_section_c'
        ]);

    Route::post('/appraisal-form/save-section-d',
        [
            'uses' => 'AppraisalController@saveSectionD',
            'as' => 'save_section_d'
        ]);

    Route::post('/appraisal-form/save-section-d-additional',
        [
            'uses' => 'AppraisalController@saveSectionDAdditional',
            'as' => 'save_section_d_additional'
        ]);

    Route::post('/appraisal-form/save-section-e',
        [
            'uses' => 'AppraisalController@saveSectionE',
            'as' => 'save_section_e'
        ]);

    Route::post('/appraisal-form/save-section-f',
        [
            'uses' => 'AppraisalController@saveSectionF',
            'as' => 'save_section_f'
        ]);

    Route::post('/appraisal-form/save-section-g',
        [
            'uses' => 'AppraisalController@saveSectionG',
            'as' => 'save_section_g'
        ]);

    Route::post('/appraisal-form/save-section-h',
        [
            'uses' => 'AppraisalController@saveSectionH',
            'as' => 'save_section_h'
        ]);

    Route::post('/appraisal-form/save-section-j',
        [
            'uses' => 'AppraisalController@saveSectionJ',
            'as' => 'save_section_j'
        ]);




    Route::post('/appraisal-form/save-section-6-b',
        [
            'uses' => 'AppraisalController@saveSection6b',
            'as' => 'save_section_6_b'
        ]);

    Route::post('/appraisal-form/save-section-c-1',
        [
            'uses' => 'AppraisalController@saveSection6c',
            'as' => 'save_section_6_c'
        ]);

    Route::post('/appraisal-form/save-section-6-d',
        [
            'uses' => 'AppraisalController@saveSection6d',
            'as' => 'save_section_6_d'
        ]);

    Route::post('/appraisal-form/save-section-6-e',
        [
            'uses' => 'AppraisalController@saveSection6e',
            'as' => 'save_section_6_e'
        ]);





    Route::post('/appraisal-form/save-section-l',
        [
            'uses' => 'AppraisalController@saveSectionL',
            'as' => 'save_section_l'
        ]);


    Route::get('/appraisal-form/forward/{id}/{workflowStep}',
        [
            'uses' => 'AppraisalController@moveAppraisal',
            'as' => 'move_form'
        ]);

    Route::get('/appraisal-form/cancel/{appraisalRef}',
        [
            'uses' => 'AppraisalController@cancelAppraisal',
            'as' => 'appraisal-form.cancel'
        ]);

    Route::post('/appraisal-form/assign-approvers',
        [
            'uses' => 'AppraisalController@assignApprovers',
            'as' => 'assign_approvers'
        ]);


    Route::get('/appraisal-form/approve/{id}/{workFlowStep}',
        [
            'uses' => 'AppraisalController@appraisalApprove',
            'as' => 'appraisals.approve'
        ]);

    Route::post('/appraisal-form/reject',
        [
            'uses' => 'AppraisalController@appraisalReject',
            'as' => 'appraisals.reject'
        ]);


    Route::post('/appraisal-form/approve-or-reject',
        [
            'uses' => 'AppraisalController@approveOrRejectAppraisal',
            'as' => 'approve_or_reject'
        ]);


    Route::post('/appraisal-form/return-to-previous-status',
        [
            'uses' => 'AppraisalController@returnAppraisalToPreviousStep',
            'as' => 'appraisal.return-to-previous-status'
        ]);



    Route::post('/appraisal-form/save-section-2',
        [
            'uses' => 'EmployeePerformancesController@saveSection2',
            'as' => 'save_section_two'
        ]);

    Route::post('/appraisal-form/save-section-3',
        [
            'uses' => 'AppraisalCompetenceAssessmentController@saveSection3',
            'as' => 'save_section_3'
        ]);

    Route::post('/appraisal-form/save-section-4',
        [
            'uses' => 'EmployeeStrengthAndWeaknessController@saveStrengthAndWeakness',
            'as' => 'save_section_4'
        ]);


    Route::post('/appraisal-form/save-section-5',
        [
            'uses' => 'AgreedTargetsController@saveSection5',
            'as' => 'save_section_5'
        ]);

    Route::post('/appraisal-form/save-section-6a',
        [
            'uses' => 'AssessmentSummaryController@saveSection6A',
            'as' => 'save_section_6a'
        ]);


    Route::get('/appraisal-form/appraisal-comments/{appraisalRef}',
        [
            'uses' => 'AppraisalController@getAppraisalComments',
            'as' => 'appraisal-form.appraisal-comments'
        ]);

});


