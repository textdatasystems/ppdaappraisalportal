
/**
 * Created by user on 2/5/2018.
 */

var hrEmpAcademicBgURL = '';
var appraisalCommentsURL = '';
var ajaxEditURL = '';
var ajaxEditFormId = '';
var profile_record_id = 1;
var role_record_id = 1;
var department_record_id = 1;
var category_record_id = 1;
var reg_office_record_id = 1;
var organization_record_id = 1;
var decimalPlaces = 2;
var debugScript = false;
var stepperInstace;
var defaultActiveStep = 0;

var gbFnAppraiseeRatingSecTwo,gbFnAppraiserRatingSecTwo,gbFnAgreedRatingSecTwo;

$.fn.allchange = function (callback) {
    var me = this;
    var last = "";
    var infunc = function () {
        var text = $(me).val();
        if (text != last) {
            last = text;
            callback();
        }
        setTimeout(infunc, 100);
    };
    setTimeout(infunc, 100);
};

function prefillSectionDBasedOnSectionE() {


    $('.input-keyduty').each(function(i, obj) {
       // alert($(this).val());
        var oldValue = $(this).val();
        $(this).val(oldValue).trigger('change');
    });


}


function setupModalsThatWillPickDataFromTheServer() {

    //for each button attach a click listener such that when click it, 1. Pick the URL attached to it in the data-source
    //attribute. 2. assign a call back to the details modal that will pick the view details from the server using the URL in 1

    $('#hr-table-container').on('click','.hr-emp-details-button', function () {

        //get clear modal container
        $('#modal-data-contracts').html('<div class="col s12 center spacer-bottom spacer-top"><h5 class="red-text center">Loading Data ...</h5></div>');

        //get URL attached to this button
        var hrUserContractURL = $(this).attr('data-source');

        //dynamically attach callback to pick the details from the server
        $('.modal-trigger-with-server-side-data').modal({

                dismissible: true, // Modal can be dismissed by clicking outside of the modal
                ready: function (modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.

                    // use other ajax submission type for post, put ...
                    $.get(hrUserContractURL, function (data) {
                        // use this method you need to handle the response from the view
                        // with rails Server-Generated JavaScript Responses this is portion will be in a .js.erb file
                        $('#modal-data-contracts').html(data);


                        //we also dynamically attach the form handler events
                        var formId = '#form_employee_contract';
                        var map = {};
                        handleForm(formId, '', map, true);

                        handleForm('#form_employee_contract_edit', 'edit', map, true);

                    });

                }
            }
        );
    });


    $('#hr-table-container').on('click','.btn-hr-academic-bg', function () {

        //get clear modal container
        $('#modal-data').html('<div class="col s12 center spacer-bottom spacer-top"><h5 class="red-text center">Loading Data ...</h5></div>');

        //get URL attached to this button
        hrEmpAcademicBgURL = $(this).attr('data-source');

        //dynamically attach callback to pick the details from the server
        $('.modal-trigger-with-server-side-data').modal({

                dismissible: true, // Modal can be dismissed by clicking outside of the modal
                ready: function (modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.

                    // use other ajax submission type for post, put ...
                    $.get(hrEmpAcademicBgURL, function (data) {

                        // use this method you need to handle the response from the view
                        // with rails Server-Generated JavaScript Responses this is portion will be in a .js.erb file
                        $('#modal-data').html(data);

                    });

                }
            }
        );
    });


    $('.btn-view-comments').on('click', function () {

        //get clear modal container
        $('#modal_appraisal_comments').html('<div class="col s12 center spacer-bottom spacer-top"><p class="red-text center">Loading comments ...</p></div>');

        //get URL attached to this button
        appraisalCommentsURL = $(this).attr('data-source');

        // use other ajax submission type for post, put ...
        $.get(appraisalCommentsURL, function (data) {
            $('#modal_appraisal_comments').html(data);
        });

    });

    $('body').on('click','#btnSearchIncompleteAppraisalsByStatus',function(event) {

        event.preventDefault();

        var ddStatusCode = $("#ddAppraisalStatus");
        var url = $(this).attr('href');
        url = url.replace('PARAM_STATUS_CODE',ddStatusCode.val());
        window.location.href = url;

    });

}


function populateFormWithAjaxData(ajaxEditFormId, data) {

    switch(ajaxEditFormId){

        case "#form_modal_user_academic_bg_edit":{

            $("#institution_edit").val(data.institution);
            $("#year_of_study_edit").val(data.yearOfStudy);
            $("#award_edit").val(data.award);
            $("#username_edit").val(data.username);
            $("#record_id_edit").val(data.id);
            Materialize.updateTextFields();

            break;

        }

    }

}

function setupModalsThatWillPickDataFromTheServerDynamic() {

    //for each button attach a click listener such that when click it, 1. Pick the URL attached to it in the data-source
    //attribute. 2. assign a call back to the details modal that will pick the view details from the server using the URL in 1

    $('.ajax-edit-button').click(function () {

        //get URL attached to this button
        ajaxEditURL = $(this).attr('data-source');
        ajaxEditFormId = $(this).attr('data-modal-form-id');

        handleForm(ajaxEditFormId,'edit',{},false);

        //dynamically attach callback to pick the details from the server
        $('.modal-for-ajax-data').modal({

                dismissible: true, // Modal can be dismissed by clicking outside of the modal
                ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.

                    // use other ajax submission type for post, put ...
                    $.get( ajaxEditURL, function(data ) {

                        // use this method you need to handle the response from the controller, in this case I am returning a json response
                        var resp = jQuery.parseJSON(data);

                        if(resp.statusCode == 0){

                            populateFormWithAjaxData(ajaxEditFormId, resp.data);

                        }else{

                            $( ".modal-content" ).html(resp.statusDescription);

                        }

                    });

                },
                complete: function() { // Callback for Modal close

                    //get clear modal container
                    location.reload(false); // $(ajaxEditFormId)[0].reset();

                }
            }
        );

    });

}


function attachListenersToDeleteButtons() {

    /* Handle click event for the user edit button in the user list */
    $('.timo-btn-delete').click(function () {

        //get the necessary details to fill in the modal
        var dtDeleteUrl = $(this).attr('data-delete-url');
        var dtItemGroup = $(this).attr('data-item-gp');
        var dtItemName = $(this).attr('data-item-name');

        //call method to show the delete dialog
        showDynamicDeleteConfirmDialog(dtItemGroup, dtItemName, dtDeleteUrl);

    });

}

function attachListenersToCommentsButtons() {

    /* Handle click event for the user edit button in the user list */
    $('.btn-view-comments').on('click',function () {

        //get the necessary details to fill in the modal
        var dtComments = $(this).attr('data-comments');

        showDynamicCommentModal(dtComments);

    });

}


function attachListenersToMaterializeSteps() {



    $('.step-title').click(function () {

        //what I am attempting to do is close a step if it's open and open it if it's closed
        var currentSteps = stepperInstace.getSteps() ;


        //i need to be sure that I have the current steps object
        var activeStepIndex = currentSteps != null ? currentSteps.active.index : -1;



        //get the step index of the guy clicked
        var clickedStepIndex = $(this).attr('data-step-index');

        //to close the step I just remove the active, the opening will be handled automatically
        if(activeStepIndex == clickedStepIndex){

            ($(this).parent()).removeClass('active');
        }

        // code below was for changing the icon on the stepper indication  to be dynamic
        /*var contentIndicator = $(this).attr('data-before');
        contentIndicator = contentIndicator == 'add' ? 'remove' : 'add';
        $(this).attr('data-before',contentIndicator);*/

    });

}

function handleAutoFillForUsernameField() {

    try{

        $("#username_login_page").allchange(function () {

            $('#username_login_page_label').addClass('active');
            $('#password_login_page_label').addClass('active');

        });

    }catch (exception){

    }

}

function activatePDFViewing() {

    /*
     * Handles PDF viewing using the Jquery-ui library
     * */
    $(document).on('click','.pdf-link', function() {

        //go to EDMS API and get the HREF
        Materialize.toast('Please wait as we download the PDF', 3000);
        var downloadLinksURL = $(this).attr('data-download-url');

        var dialog1 = $("#pdf-dialog").dialog({
            width: 900,
            height: 550,
            top:['center',20]
        });

        $(".ui-button-icon-only").text('X').css('color','white').css('background-color','black');
        $(".ui-icon-closethick").css('background-color','black');
        $("#pdf-dialog").css('background-color','white');

        //show progress dialog
        //show toast

        // use other ajax submission type for post, put ...
        $.get( downloadLinksURL, function(data ) {

            // use this method you need to handle the response from the controller, in this case I am returning a json response
            var resp = jQuery.parseJSON(data);
            if(resp.statusCode == 0){

                $(".ui-dialog").show();
                var pdfURL = resp.result;
                $('#iframe-pdf-viewer').attr('src', pdfURL);

            }else{

                $(".ui-dialog").hide();
                //show alert
                alert(resp.statusDescription);

            }

        });

    });

}

$(document).ready(function(){


    var dateToday = moment().format('YYYY-MM-DD');


    $('.modal').modal({  dismissible: true  });
    $('tabs').tabs();
    $('.dropdown-trigger').dropdown();
    $('.dropdown-button').dropdown();

    $('body').on('mouseover','.dt_wrapper', function () {

        $('.dropdown-trigger').dropdown();
        $('.dropdown-button').dropdown();

    });

    // $('body').on('mouseover','.dropdown-trigger', function () {
    //
    //     $('.dropdown-trigger').dropdown();
    //     $('.dropdown-button').dropdown();
    //
    // });


    activatePDFViewing();

    setupModalsThatWillPickDataFromTheServer();

    setupModalsThatWillPickDataFromTheServerDynamic();

    attachListenersToEditButtons();

    attachListenersToDeleteButtons();

    attachListenersToCommentsButtons();

    handleAutoFillForUsernameField();

    $('#modal_deletion').modal('open');
    $('#modal_message').modal('open');


    $('select').material_select();


    $(".button-collapse").sideNav();



    $('.parallax').parallax();

    // $('.stepper').activateStepper();

    var stepper = document.querySelector('.stepper');
    defaultActiveStep = $('#default_active_step').val();
    stepperInstace = new MStepper(stepper, {
        // options
        firstActive: defaultActiveStep // this is the default
    });


    attachListenersToMaterializeSteps();


    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 100, // Creates a dropdown of 15 years to control year
        format: 'yyyy-mm-dd',
        defaultDate:dateToday,
        setDefaultDate:true
    });


    $('#modal_org_created').modal('open');
    $('#modal_account_created').modal('open');
    $('#modal_change_password').modal('open');
    $('#modal_successful_operation').modal('open');
    $('#modal_form_message').modal('open');


    $('#user_profiles_table').pageMe({
        pagerSelector:'#myPager',
        activeColor: 'green',
        prevText:'Previous',
        nextText:'Next',
        showPrevNext:true,
        hidePageNumbers:false,
        perPage:7,
        totalLabel:"#total_users"
    });

    $('#document_types_table').pageMe({
        pagerSelector:'#myPagerDocumentTypes',
        activeColor: 'green',
        prevText:'Previous',
        nextText:'Next',
        showPrevNext:true,
        hidePageNumbers:false,
        perPage:7,
        totalLabel:"#total_reg"
    });


    prefillSectionDBasedOnSectionE();


});

function attachListenersToEditButtons() {

    /* Handle click event for contract edit by HR */
    $('#modal_hr_contract_history').on('click','.edit-button-contract', function (evt) {

        $('#form_employee_contract').addClass('hidden');
        $('#form_employee_contract_edit').removeClass('hidden');

        var dtRef = $(this).attr('data-ref');
        var dtStartDate = $(this).attr('data-start-date');
        var dtExpiryDate = $(this).attr('data-expiry-date');
        var dtRecordId = $(this).attr('data-record-id');

        $('#contract_reference_edit').val(dtRef);
        $('#contract_start_date_edit').val(dtStartDate);
        $('#contract_expiry_date_edit').val(dtExpiryDate);
        $('#record_id_edit').val(dtRecordId);

        Materialize.updateTextFields();

    });

    $('#modal_hr_contract_history').on('click','.cancel-edit-button', function (evt) {

        $('#form_employee_contract_edit').addClass('hidden');
        $('#form_employee_contract').removeClass('hidden');

    });


    /* Handle click event for the user edit button in the user list */
    $('.edit-btn-competence-cat').click(function () {

        var dtRecordId = $(this).attr('data-record-id');
        var dtOrgCode = $(this).attr('data-org-code');
        var dtEmpCatCode = $(this).attr('data-emp-cat-code');
        var dtCategory = $(this).attr('data-cat');
        var dtMaxRating = $(this).attr('data-max-rating');

        $('#org_code_edit').find('option[value="'+dtOrgCode+'"]').prop('selected', true);
        $('#employee_category_code_edit').find('option[value="'+dtEmpCatCode+'"]').prop('selected', true);

        $('#competence_category_edit').val(dtCategory);
        $('#max_rating_edit').val(dtMaxRating);
        $('#record_id_edit').val(dtRecordId);

        handleForm('#form_modal_competence_cat_edit','edit',{},true);

        Materialize.updateTextFields();

    });


    /* Handle click event for the user edit button in the user list */
    $('.edit-btn-objective').click(function () {

        var dtFormId = $(this).attr('data-form-id');
        var dtRecordId = $(this).attr('data-record-id');
        var dtOrgCode = $(this).attr('data-org-code');
        var dtObjective = $(this).attr('data-obj');

        $('#organization_edit').find('option[value="'+dtOrgCode+'"]').prop('selected', true);

       // $('#record_id').val(dtRecordId);
        $('input:hidden[name="record_id"]').val(dtRecordId);
        $('#objective_edit').val(dtObjective);

        handleForm('#'+dtFormId,'edit',{},true);

        Materialize.updateTextFields();

    });


    /* Handle click event for the user edit button in the user list */
    $('.edit-btn-competence').click(function () {

        var dtFormId = $(this).attr('data-form-id');
        var dtRecordId = $(this).attr('data-record-id');
        var dtCompetence = $(this).attr('data-competence');
        var dtRank = $(this).attr('data-rank');
        var dtRating = $(this).attr('data-rating');
        var dtCatId = $(this).attr('data-cat-id');

        $('input:hidden[name="record_id"]').val(dtRecordId);
        $('#competence_edit').val(dtCompetence);
        $('#rank_edit').val(dtRank);
        $('#rating_edit').val(dtRating);
        $('#competence_category_id_edit').val(dtCatId);

        handleForm('#'+dtFormId,'edit',{},true);

        Materialize.updateTextFields();

    });


    /* Handle click event for the user edit button in the user list */
    $('.edit-btn-appraisal-approvers').click(function () {

        var dtAppraisalRef = $(this).attr('data-appraisal-ref');
        var dtHod = $(this).attr('data-hod');
        var dtSupervisor = $(this).attr('data-supervisor');
        var dtEd = $(this).attr('data-ed');

        $('#head_of_department').find('option[value="'+dtHod+'"]').prop('selected', true);
        $('#supervisor').find('option[value="'+dtSupervisor+'"]').prop('selected', true);
        $('#executive_director').find('option[value="'+dtEd+'"]').prop('selected', true);

       $('input:hidden[name="appraisal_ref"]').val(dtAppraisalRef);

        handleForm('#form_modal_appraisal_approvers','',{},true);

        Materialize.updateTextFields();


    });


    $('.edit-button').click(function () {

        profile_record_id = $(this).attr('data-user_id');
        var formId  = '#edit_profile_form';

        /* Define extra data to pass to form */
        var orgCode = '#org_code_'+profile_record_id;
        var regionalOfficeCode = '#regional_office_code_'+profile_record_id;
        var deptCode = '#department_code_'+profile_record_id;
        var categoryCode = '#category_code_'+profile_record_id;
        var roleCode = '#role_code_'+profile_record_id;

        var map = {
            "org_code" : orgCode,
            "regional_office_code" : regionalOfficeCode,
            "department_code" : deptCode,
            "category_code" : categoryCode,
            "role_code" : roleCode
        };

        handleForm(formId,profile_record_id, map, true);

    });



    /* Handle the click event for the academic background save button */
    $('#btn_save_academic_bg').click(function () {
        var formId  = '#form_modal_user_academic_bg';
        var map = {};
        handleForm(formId,'', map, true);
    });

    $('#btn_save_competence_cat').click(function () {
        var formId  = '#form_modal_competence_cat';
        var map = {};
        handleForm(formId,'', map, true);
    });

    $('#btn_save_competence').click(function () {
        var formId  = '#form_modal_competence';
        var map = {};
        handleForm(formId,'', map, true);
    });

    $('#btn_save_objective').click(function () {
        var formId  = $(this).attr('data-form-id');
        handleForm('#'+formId,'', {}, true);
    });

    $('.timo-btn-add').click(function () {
        var formId  = $(this).attr('data-form-id');
        handleForm('#'+formId,'', {}, true);
    });


    /* Handle the click event for the department edit button in the departments list */
    $('.edit-button-department').click(function () {

        department_record_id = $(this).attr('data-record-id');
        var formId  = '#edit_department_form';

        /* Define extra data to pass to form */
        var orgCode = '#org_code_'+department_record_id;

        var map = {
            "org_code" : orgCode
        };

        handleForm(formId,department_record_id, map, true);

    });


    /* Handle the click event for the employee category edit button in the employee category list */
    $('.edit-button-categories').click(function () {

        category_record_id = $(this).attr('data-record-id');
        var formId  = '#edit_category_form';

        /* Define fields for extra data to pass to form */

        var orgCode = '#org_code_'+category_record_id;

        var map = {
            "org_code" : orgCode
        };

        handleForm(formId,category_record_id, map, true);

    });


    /* Handle the click event for the regional offices edit button in the regional offices list */
    $('.edit-button-regional-office').click(function () {

        reg_office_record_id = $(this).attr('data-record-id');
        var formId  = '#edit_regional_office_form';

        /* Define fields for extra data to pass to form */

        var orgCode = '#org_code_'+reg_office_record_id;

        var map = {
            "org_code" : orgCode
        };

        handleForm(formId,reg_office_record_id, map, true);

    });


    /* Handle the click event for the organization edit button in the organizations list */
    $('.edit-button-organization').click(function () {

        organization_record_id = $(this).attr('data-record-id');
        var formId  = '#edit_organization_form';

        handleForm(formId,organization_record_id, {},true);

    });

    /* Handle the click event for the role code edit button in the role code list */
    $('.edit-button-role-code').click(function () {

        role_record_id = $(this).attr('data-record-id');
        var formId  = '#edit_role_code_form';

        /* Define extra data to pass to form */
        var orgCode = '#org_code_'+role_record_id;

        var map = {
            "org_code" : orgCode
        };

        handleForm(formId,role_record_id, map, true);

    });


}

function setDatePickerValue( dateSent) {

    alert('sasasa');

   /*  var $input = $('.datepicker').pickadate();
    var picker = $input.pickadate('picker');

    picker.set('select', dateSent, { format: 'yyyy/mm/dd' } */

}

function printErrorMsg(errors, recordId) {

    var error_msg_box = "#print-error-msg"+recordId;

    $(error_msg_box).find("ul").html('');
    $(error_msg_box).css('display','block');

    $.each( errors, function( key, value ) {
        $(error_msg_box).find("ul").append('<li>'+value+'</li>');
    });

}


function printErrorMsgDocumentTypes(errors) {

    var error_msg_box = "#print-error-msg-doc-type"+document_type_record_id;

    $(error_msg_box).find("ul").html('');
    $(error_msg_box).css('display','block');

    $.each( errors, function( key, value ) {
        $(error_msg_box).find("ul").append('<li>'+value+'</li>');
    });

}


function clearErrorMsg(recordId) {

    var error_msg_box = "#print-error-msg"+recordId;
    $(error_msg_box).find("ul").html('');
    $(error_msg_box).css('display','none');

}


function printSuccessMessage(formMessages, message) {
    var data = message == null ? '' : message.success;
    $(formMessages).text(data);
}


function handleForm($formId, $recordId, extraDataFieldsObj, refreshPage) {


    /*
    * Not for some cases, I have the edit form and the new form on the same page, so to differentiate the messages fields,
    * I append "edit" to the message field ID (i.e Edit[form-messagesedit] and New[form-messages]) but this should not affect the form ID and thus the logic below
    * */
    var form = $($formId + ($recordId == 'edit' ? '' : $recordId));
    var formMessages = $('#form-messages'+$recordId);


    $(form).submit(function (event) {

        event.preventDefault();

        var formData = $(form).serializeArray();

        /*
        * Attach extra data to the form
        * */
        for (var property in extraDataFieldsObj) {

            if (!extraDataFieldsObj.hasOwnProperty(property)) continue;

            /*
            * Get the field value
            * */
            var fieldValue = $(extraDataFieldsObj[property]).val();

            /*
             * Attach the field to the data
             * */
            formData.push({name: property, value: fieldValue});

        }

        // Submit the form using AJAX.
        $.ajaxSetup({
            headers: {'X-Requested-With': 'XMLHttpRequest'}
        });

        $.ajax({

            type: 'POST',
            url: $(form).attr('action'),
            data: formData,
            success: function (data) {

                if($.isEmptyObject(data.error)){

                    if(refreshPage){
                        window.location.reload(true);
                    }else{
                        printSuccessMessage(formMessages, data);
                        clearErrorMsg($recordId);
                    }

                }else{
                    printSuccessMessage(formMessages,null);
                    printErrorMsg(data.error,$recordId);

                }

            },
            error: function (data) {
                alert(data);
            }

        });

        // Stop the browser from submitting the form.
       // event.preventDefault();
        return false;

    });


}

function handleUserProfileForm() {


    var form = document.getElementById('edit_profile_form'+profile_record_id);
    //alert(elem);

//    var form = $('#edit_profile_form' + profile_record_id);



    var formMessages = document.getElementById('form-messages'+profile_record_id);
   // var formMessages = $('#form-messages'+profile_record_id);


    alert(formMessages);


    $(form).submit(function (event) {

        /*
        * Serialize the form to get the data,
        * Note that for materialize css material_select the select values are not serialized so we added them manually
        * */
        var formData = $(form).serializeArray();


        /*
        * Get the values for the materialize css select fields
        * */
        $orgCode =  $('#org_code_'+profile_record_id).val();
        $regionalOfficeCode = $('#regional_office_code_'+profile_record_id).val();
        $deptCode = $('#department_code_'+profile_record_id).val();
        $categoryCode = $('#category_code_'+profile_record_id).val();
        $roleCode = $('#role_code_'+profile_record_id).val();



        /*
        * Push the select values to the form data manually
        * */
        formData.push({name: 'org_code', value: $orgCode});
        formData.push({name: 'regional_office_code', value: $regionalOfficeCode});
        formData.push({name: 'department_code', value: $deptCode});
        formData.push({name: 'category_code', value: $categoryCode});
        formData.push({name: 'role_code', value: $roleCode});

        /*
        * Now we have all the form data maybe if there is some other weird stuff I don't know of
        * But man if you are wise find away of avoiding the above stuff, it's not scalable
        * */


        // Submit the form using AJAX.
        $.ajaxSetup({
            headers: {'X-Requested-With': 'XMLHttpRequest','Accepts': 'application/json'}
        });

        $.ajax({

            type: 'POST',
            url: $(form).attr('action'),
            data: formData,
            success: function (data) {

                if($.isEmptyObject(data.error)){

                    printSuccessMessage(formMessages, data);
                    clearErrorMsg(profile_record_id);

                }else{

                    printSuccessMessage(formMessages,null);
                    printErrorMsg(data.error,profile_record_id);

                }

            },
            error: function (data) {
                alert(data);
            }

        });

        // Stop the browser from submitting the form.
        event.preventDefault();

    });
}


function showDeleteConfirmationModal(triggerButton, modal){

    $('#' + modal).modal('open');
    $('#' + modal + '_YesBtn').click(function(){
        $('#' + modal).modal('close');
        document.location = triggerButton.href;
    });

}

function showDynamicDeleteConfirmDialog(itemGroup, itemName, deleteURL) {

    //set the item group value in the delete modal
    $('#item_group').text(itemGroup);

    //set the item name value in the delete modal
    $('#item_name').text(itemName);

    //open the delete modal
    $('#modal_delete').modal('open');

    //attach lister to the confirm button of the delete dialog
    $('#modal_delete_YesBtn').click(function(){
        $('#modal_delete').modal('close');
        document.location = deleteURL;
    });

}


function showDynamicCommentModal(comments) {




    //set the item group value in the delete modal
    // $('#comment_textarea').innerHTML(comments);

    var result = comments == null ?  "" : comments.replace(/<br\s?\/?>/g,"\n");

    $("textarea#comment_textarea").html(result);

    //open the delete modal
    $('#modal_comments').modal('open');

}

function closeModalAndRedirect(modalId,href) {

    $(modalId).modal('close');
    document.location = href;
    return false;

}


/**
 * Format to give number of decimal places
 * @param value
 * @param precision
 * @returns {string}
 */
function toFixed(value, precision) {

    var power = Math.pow(10, precision || 0);
    return String(Math.round(value * power) / power);

}

/**
 * Method used to sum up the contents in agiven row and fill the sum into the totals field
 * @param fieldIdPrefix ID Prefix for the fields being looped through
 * @param noOfRows used to loop through the fields
 * @param totalFieldId
 * @param maxValueOfTotal Sum must not go beyond this value
 * @param currentField
 */
function sumColumnRows(fieldIdPrefix, noOfRows, totalFieldId, maxValueOfTotal, currentField) {

    var rowTotal = 0.0;
    for(var count = 1; count <= noOfRows ; count++){

        var fieldId = '#'+fieldIdPrefix+count;
        var rating = parseFloat($(fieldId).val());

        if(!isNaN(rating)){
            rowTotal += rating;
        }

    }

    if(rowTotal > maxValueOfTotal){

        alert("Total is beyond "+maxValueOfTotal+", please check input");

        //clear the field value
        $(currentField).val('');
        //trigger blue event to force recalculation of the totals
        $(currentField).blur();

    }else{
        $('#'+totalFieldId).val(rowTotal);
    }

   // calculateSectionDAndSectionDAdditionalAsAPercentage()
    updateEmployeePerformanceNoWeighedScore();

}

function initiateRecalculationOfEmployeePerformanceTotalScores() {

    try{

        $('#maximum_rating_sec_two_1').blur();
        $('#appraisee_rating_sec_two_1').blur();
        $('#appraiser_rating_sec_two_1').blur();
        $('#agreed_rating_sec_two_1').blur();

    }catch (e) {

    }

}


/**
 * Section D and Section D Additional Should sum upto 120 now we are converting the person's score
 * as a percentage ie ((personScore * 100)/120) where personScore = scoreInSectionD + scoreInSectionDAdditional
 */
function calculateSectionDAndSectionDAdditionalAsAPercentage() {

    var sectionDAgreedRating = parseFloat($('#sec_d_total_agreed_rating').val());
    var sectionDAdditionalAgreedRating = parseFloat($('#sec_d_add_total_agreed_rating').val());

    if(!isNaN(sectionDAgreedRating) && !isNaN(sectionDAdditionalAgreedRating)){
        var percentageScore = ((sectionDAgreedRating + sectionDAdditionalAgreedRating)*100)/120;
        $('#sec_d_final_percentage_score').val(toFixed(percentageScore,decimalPlaces));
    }else{
        $('#sec_d_final_percentage_score').val('');
    }

    apply80PercentWeightOnSectionD();

}



function updateEmployeePerformanceNoWeighedScore() {

    var section2AgreedRating = parseFloat($('#sec_2_total_agreed_rating').val());

    if(!isNaN(section2AgreedRating)){
        $('#key_result_score_no_weight').val(toFixed(section2AgreedRating,decimalPlaces));
    }else{
        $('#key_result_score_no_weight').val('');
    }

    // apply80PercentWeightOnSectionD();
    calculateSection2Actual();

}


function calculateSection2Actual(){

    var weighedValue = 0.0;
    var sec2PercentageScore = window.document.getElementById("sec_2_total_agreed_rating");
    var sec2PercentageElem = window.document.getElementById("key_result_score_no_weight");
    var sec2WeighedBy80 = window.document.getElementById("key_result_score_actual");
    var sec2TotalMaximumRatingElem =  window.document.getElementById("sec_2_total_maximum_rating");

    var totalAgreedScoreSection2 = sec2PercentageScore.value;
    var totalMaximumRatingSection2 = sec2TotalMaximumRatingElem.value;

    sec2PercentageElem.value = isNaN(totalAgreedScoreSection2) ? '' : toFixed(totalAgreedScoreSection2,decimalPlaces);

    if(!isNaN(totalAgreedScoreSection2) && !isNaN(totalMaximumRatingSection2)){

        var percentageScoreSection2 = ((parseFloat(totalAgreedScoreSection2) * 100)/totalMaximumRatingSection2);
        weighedValue = ((parseFloat(percentageScoreSection2)/100)*80);

        //weighedValue = ((parseFloat(totalAgreedScoreSection2)/100)*80);
        sec2WeighedBy80.value = isNaN(weighedValue) ? '' : toFixed(weighedValue,decimalPlaces);

    }else{
        sec2WeighedBy80.value = '';
    }

    sumSections();

}

/**
 * Applies 80% weighting on the score obtained from Section D
 */
function apply80PercentWeightOnSectionD(){

    var weighedValue = 0.0;
    var secDPercentageScore = window.document.getElementById("sec_d_final_percentage_score");
    var secDWeighedBy80 = window.document.getElementById("sec_d_weighed");

    if(!isNaN(secDPercentageScore.value)){
        weighedValue = ((parseFloat(secDPercentageScore.value)/100)*80);
        secDWeighedBy80.value = isNaN(weighedValue) ? '' : toFixed(weighedValue,decimalPlaces);
    }else{
        secDWeighedBy80.value = '';
    }

    window.document.getElementById("FinalScoreSecD").value =  secDWeighedBy80.value;
    sumSections();

}


function finishTable(tableId, columnIndex, totalCellId) {

    var columnToSum = columnIndex;
    var tableElemTableId = tableId;
    var tableElemTotal = totalCellId;
    var totalScore = 0.0;

    //Variables for the footer values
    var totalScoreElem = window.document.getElementById(tableElemTotal);

    try {

        totalScore = computeTableColumnTotal(tableElemTableId, columnToSum);
        totalScoreElem.value = totalScore.toLocaleString();

        if(tableId == "table_sec_e"){
            calculateSecEpercent();
        }
    }
    catch (ex) {
        window.alert("Exception in function finishTable()\n" + ex);
    }
    finally {
        event.target.style.background = "";
        return;
    }

}


function computeTableColumnTotal(tableId, colNumber) {

    // find the table with id attribute tableId
    // return the total of the numerical elements in column colNumber
    // skip the top row (headers) and bottom row (where the total will go)
    var result = 0.0;
    if (debugScript) {
        window.alert("Processing table " + tableId + " Column " + colNumber);
    }
    try {

        var tableElem = window.document.getElementById(tableId);

        var tableBody = tableElem.getElementsByTagName("tbody").item(0);
       // var tableBody = tableElem.getElementsByTagName("tbody");

        var howManyRows = tableBody.rows.length;

        // window.alert("Body has " + howManyRows + " rows.");
        for (var i = 1; i < howManyRows ; i++) // We expect headers and footers in their own sections
        {
            var thisTrElem = tableBody.rows[i];
            var thisTdElem = thisTrElem.cells[colNumber];

            if(thisTdElem.children[0] == null) continue;
            var thisTextNode = thisTdElem.children[0];
            var thisNumber = parseFloat(thisTextNode.value.replace(/,/g, ''));
            if (debugScript) {
                window.alert("Row " + i + " Column " + colNumber + " value is " + thisNumber);
            } // end if
            // try to convert text to numeric
            // if you didn't get back the value NaN (i.e. not a number), add into result
            if (!isNaN(thisNumber))
                result += thisNumber;
            //alert(result);


        }


        // end for
    } // end try
    catch (ex) {
        window.alert("Exception in function computeTableColumnTotal()\n" + ex);
        result = 0.0;
    }
    finally {
        if (debugScript) window.alert("Total is: " + result);


        return result;
    }
}


function updateSection3PercentScore(){

    var agreedTotalElem = window.document.getElementById("secEagreedTotal");
    var agreedTotal = parseFloat(agreedTotalElem.value);

    var secEpercent = window.document.getElementById("behavioral_competence_score_no_weight");

    if (!isNaN(agreedTotal)){
        secEpercent.value = toFixed(agreedTotal,3);
    }


}

function calculateSecEpercent(){

    var maxTotalElem = window.document.getElementById("secEmaxTotal");
    var agreedTotalElem = window.document.getElementById("secEagreedTotal");
    var maxTotal = parseFloat(maxTotalElem.value);
    var agreedTotal = parseFloat(agreedTotalElem.value);
    var secEpercent = window.document.getElementById("behavioral_competence_score_no_weight");
    //additional

    if (!isNaN(maxTotal)){
        if (!isNaN(maxTotal)){

            var percent = (((agreedTotal)/(maxTotal))*100);

           secEpercent.value = toFixed(percent,3);
           // calculateSecEpercentActual();
            calculateSec3percentActual();

        }
    }


}

function updateFormTotalScoresAgain() {

    calculateSection2Actual();
    calculateSec3percentActual();
    sumSections();

}


function calculateSec3percentActual(){

    var percent1 = 0.0;
    var ElemSecETableOutOf100 = window.document.getElementById("secEagreedTotal");

    var section3PercentageScore = ElemSecETableOutOf100.value;

    if(!isNaN(section3PercentageScore)){

        percent1 = ((parseFloat(section3PercentageScore)/100)*20);

        var ElemSection3PercentScore = window.document.getElementById("behavioral_competence_score_no_weight");
        ElemSection3PercentScore.value = toFixed(section3PercentageScore,3);

        var ElemSection3WeighedScore = window.document.getElementById("behavioral_competence_score_actual");
        ElemSection3WeighedScore.value =  toFixed(percent1,3);

    }

    sumSections();

}



function calculateSecEpercentActual(){

    var percent1 = 0.0;
    var ElemSecEOutOf100 = window.document.getElementById("secEagreedTotal");
    var ElemWeighedOutOf20 = window.document.getElementById("behavioral_competence_score_no_weight");

    if(!isNaN(ElemSecEOutOf100.value)){
        percent1 = ((parseFloat(ElemSecEOutOf100.value)/100)*20);

        ElemWeighedOutOf20.value = toFixed(percent1,3);
        window.document.getElementById("behavioral_competence_score_actual").value =  toFixed(percent1,3);

    }

    sumSections();

}

function sumSections(){

    var ElemSecD = window.document.getElementById("key_result_score_actual");
    var ElemSecE = window.document.getElementById("behavioral_competence_score_actual");
    var SumElem = window.document.getElementById("total_score_actual");

    if(!isNaN(ElemSecD.value)){
        if(!isNaN(ElemSecE.value)){
            var FinalMark = parseFloat(ElemSecD.value) + parseFloat(ElemSecE.value);
            SumElem.value = toFixed(FinalMark,3);

            /*Fill up the summary table values after calculating all totals*/
            var ElemsumScored04 = window.document.getElementById("sumScored04");
            var ElemsumScored05 = window.document.getElementById("sumScored05");
            var ElemsumScoredTotal = window.document.getElementById("sumScoredTotal");

        }
    }
}


function generateChildRowHtml(newId, parentId) {

    var childHtml = "";

    if(parentId == 'parent_dynamic_education_bg'){

        childHtml =
            '<div class="col m1 s12 ">' + newId + '</div> ' +
            '<div class="col m4 s12 "><input id="' + 'school_' + newId + '" name="' + 'school_' + newId + '" type="text" class="validate"></div>' +
            '<div class="col m2 s12 "><input id="' + 'year_of_study_' + newId + '" name="' + 'year_of_study_' + newId + '" type="text" class="validate"></div>' +
            '<div class="col m5 s12 "><input id="' + 'award_' + newId + '" name="' + 'award_' + newId + '" type="text" class="validate"></div>';

    }
    else if(parentId == 'parent_dynamic_key_duties'){

        var fnAssignment = "assignment_"+newId;
        var fnExpectedOutput = "expected_output_"+newId;
        var fnExpectedOutputSecD = "expected_output_sec_d_"+newId;
        var fnMaxRating = "max_rating_"+newId;
        var fnTimeFrame = "time_frame_"+newId;
        var fnFormFieldCount = "form_field_count"+newId;

        //get the options for the drop down of the objectives, here I assume the first drop down will have the values
        var firstObjectiveDropDown = document.getElementsByName('objective_0').item(0);
        var objectiveOptions = firstObjectiveDropDown.innerHTML;

        childHtml =
            '<div class="row">'+

                '<div class="col m1 s12 ">'+ newId + '</div>'+
                '<div class="col m11 s12">'+
                    '<select name="objective_'+newId+'" required class="browser-default validate s12" style="width: 100%">'+
                    //'<option value="" disabled selected>Select related strategic objective</option>'+
                    objectiveOptions +
                    '</select>'+
                '</div>'+

            '</div>'+

            '<div class="row ">'+

                '<div class="col m1 s12 ">' + '</div>'+
                '<div class="col m4 s12 ">'+
                    '<textarea  id="'+fnAssignment+'" onchange="copyBetweenTextField(0,1)"'+
                     ' name="'+fnAssignment+'" type="text" class="validate"></textarea>'+
                '</div>'+
                '<div class="col m4 s12 ">'+
                '<textarea id="'+fnExpectedOutput+'" name="'+fnExpectedOutput+'" type="text" class="validate"></textarea>'+
                '</div>'+
                '<div class="col m1 s12 ">'+
                '<input  id="'+fnMaxRating+'" name="'+fnMaxRating+'" type="number"  class="validate browser-default tab-input ">'+
                '</div>'+
                '<div class="col m2 s12 ">'+
                '<input  id="'+fnTimeFrame+'" name="'+fnTimeFrame+'" type="text" class="validate  browser-default tab-input">'+
                '</div>'+

                '<input type="hidden" name="'+fnFormFieldCount+'" value="'+newId+'"/>'+

            '</div>';

        return childHtml;

    }
    else if(parentId == 'parent_dynamic_assignments'){

        var fnObjective = "objective_"+newId;
        var fnExpectedOutputSecD = 'expected_output_sec_d_'+newId;
        var fnActualPerformance = 'actual_performance_'+newId;
        var fnMaxRatingSecD= 'max_rating_sec_d'+newId;
        var fnAppraiseeRatingSecD = 'appraisee_rating_sec_d'+newId;
        var fnAppraiserRatingSecD = 'appraiser_rating_sec_d'+newId;
        var fnAgreedRatingSecD = 'agreed_rating_sec_d'+newId;

        //get the options for the drop down of the objectives, here I assume the first drop down will have the values
        var firstObjectiveDropDown = document.getElementsByName('objective_0').item(0);
        var objectiveOptions = firstObjectiveDropDown.innerHTML;

        childHtml =
            '<div class="row">'+
                '<div class="col m1 s12 ">'+ newId + '</div>'+
                '<div class="col m11 s12" style="padding-left: 1.5%">'+
                    '<select  name="'+fnObjective+'" required class="browser-default validate s12" style="width: 100%">'+
                    objectiveOptions+
                    '</select>'+
                '</div>'+
            '</div>'+

            '<div class="row">'+

                '<div class="col s7">'+

                    '<div class="col s2 ">'+
                    '</div>'+
                    '<div class="col s5 ">'+
                    '<textarea id="'+fnExpectedOutputSecD+'" name="'+fnExpectedOutputSecD+'" type="text" class="validate"></textarea>'+
                    '</div>'+
                    '<div class="col s5 ">'+
                    '<textarea id="'+fnActualPerformance+'" name="'+fnActualPerformance+'" type="text" class="validate"></textarea>'+
                    '</div>'+

                '</div>'+

                '<div class="col s5">'+

                    '<div class="col s3 ">'+
                    '<input id="'+fnMaxRatingSecD+'" min="0" name="'+fnMaxRatingSecD+'" type="number" onblur="sumColumnRows(\'max_rating_sec_d\',14,\'sec_d_total_max_rating\',100)" class="validate browser-default tab-input">'+
                    '</div>'+
                    '<div class="col s3 ">'+
                    '<input id="'+fnAppraiseeRatingSecD+'" min="0" name="'+fnAppraiseeRatingSecD+'" onblur="sumColumnRows(\'appraisee_rating_sec_d\',14,\'sec_d_total_appraisee_rating\',100)" type="number" class="validate browser-default tab-input">'+
                    '</div>'+
                    '<div class="col s3 ">'+
                    '<input id="'+fnAppraiserRatingSecD+'" min="0" name="'+fnAppraiserRatingSecD+'" onblur="sumColumnRows(\'appraiser_rating_sec_d\',14,\'sec_d_total_appraiser_rating\',100)" type="number" class="validate browser-default tab-input">'+
                    '</div>'+
                    '<div class="col s3 ">'+
                    '<input id="'+fnAgreedRatingSecD+'" min="0" name="'+fnAgreedRatingSecD+'" onblur="sumColumnRows(\'agreed_rating_sec_d\',14,\'sec_d_total_agreed_rating\',100)" type="number" class="validate browser-default tab-input">'+
                    '</div>'+
                '</div>'+

            '</div>';

        return childHtml;

    }
    else if(parentId == 'parent_dynamic_employee_performance'){

        var fnObjective1 = "objective_"+newId;
        var fnKeyResultArea = 'key_result_area_'+newId;
        var fnKeyPerformanceIndicator_ = 'key_performance_indicator_'+newId;
        var fnPerformanceResult = 'performance_result_'+newId;
        var fnMaxRatingSecTwo = 'maximum_rating_sec_two_'+newId;

        var fnAppraiseeRatingSecTwo = 'appraisee_rating_sec_two_'+newId;
        gbFnAppraiseeRatingSecTwo = fnAppraiseeRatingSecTwo;

        var fnAppraiserRatingSecTwo = 'appraiser_rating_sec_two_'+newId;
        gbFnAppraiserRatingSecTwo = fnAppraiserRatingSecTwo;


        var fnAgreedRatingSecTwo = 'agreed_rating_sec_two_'+newId;
        gbFnAgreedRatingSecTwo = fnAgreedRatingSecTwo;


        var fnAppraiseeCommentSectionTwo_ = 'appraisee_comment_section_two_'+newId;
        var fnSupervisorCommentSectionTwo = 'supervisor_comment_section_two_'+newId;

        //get the options for the drop down of the objectives, here I assume the first drop down will have the values
        var firstObjectiveDropDown1 = document.getElementsByName('objective_0').item(0);
        var fnObjectiveOptions = firstObjectiveDropDown1.innerHTML;


        childHtml =

            '<div class="row">'+
            '<div class="col s12 ">'+
            '<select  name="'+fnObjective1+'" required class="browser-default validate s12" style="width: 100%">'+
            fnObjectiveOptions+
            '</select>'+
            '</div>'+
            '</div>'+

            '<div class="row">'+

            '<div class="col s6">'+
                '<div class="col s4">'+
                '<textarea id="'+fnKeyResultArea+'" name="'+fnKeyResultArea+'" type="text" class="validate"></textarea>'+
                '</div>'+
                '<div class="col s4">'+
                '<textarea id="'+fnKeyPerformanceIndicator_+'" name="'+fnKeyPerformanceIndicator_+'" type="text" class="validate"></textarea>'+
                '</div>'+
                '<div class="col s4">'+
                '<textarea id="'+fnPerformanceResult+'" name="'+fnPerformanceResult+'" type="text" class="validate"></textarea>'+
                '</div>'+
            '</div>'+

            '<div class="col s3">'+
                '<div class="col s3">'+
                '<input id="'+fnMaxRatingSecTwo+'" min="0" name="'+fnMaxRatingSecTwo+'" onblur="sumColumnRows(\'maximum_rating_sec_two_\',25,\'sec_2_total_maximum_rating\',100,this)" onchange="setMax(this, \''+gbFnAppraiseeRatingSecTwo+'\', \''+gbFnAppraiserRatingSecTwo+'\', \''+gbFnAgreedRatingSecTwo+'\');" type="number" class="validate browser-default tab-input">'+
                '</div>'+
                '<div class="col s3">'+
                '<input id="'+fnAppraiseeRatingSecTwo+'" min="0" name="'+fnAppraiseeRatingSecTwo+'" onblur="checkMaxValue(this);sumColumnRows(\'appraisee_rating_sec_two_\',25,\'sec_2_total_appraisee_rating\',100,this);" type="number" class="validate browser-default tab-input">'+
                '</div>'+
                '<div class="col s3">'+
                '<input id="'+fnAppraiserRatingSecTwo+'" min="0" name="'+fnAppraiserRatingSecTwo+'" onblur="checkMaxValue(this);sumColumnRows(\'appraiser_rating_sec_two_\',25,\'sec_2_total_appraiser_rating\',100,this);" type="number" class="validate browser-default tab-input">'+
                '</div>'+
                '<div class="col s3">'+
                '<input id="'+fnAgreedRatingSecTwo+'" min="0" name="'+fnAgreedRatingSecTwo+'" onblur="checkMaxValue(this);sumColumnRows(\'agreed_rating_sec_two_\',25,\'sec_2_total_agreed_rating\',100,this);" type="number" class="validate browser-default tab-input">'+
                '</div>'+
            '</div>'+

            '<div class="col s3">'+
                '<div class="col s6">'+
                '<textarea id="'+fnAppraiseeCommentSectionTwo_+'" name="'+fnAppraiseeCommentSectionTwo_+'" type="text" class="validate"></textarea>'+
                '</div>'+
                '<div class="col s6">'+
                '<textarea id="'+fnSupervisorCommentSectionTwo+'" name="'+fnSupervisorCommentSectionTwo+'" type="text" class="validate"></textarea>'+
                '</div>'+
            '</div>'+

            '</div>';

        return childHtml;

    }
    else if(parentId == 'parent_dynamic_additional_assignments'){

        var fnObjective = "objective_"+newId;
        var fnExpectedOutputSecD = 'expected_output_sec_d_add'+newId;
        var fnActualPerformance = 'actual_performance_sec_d_add'+newId;
        var fnMaxRatingSecD= 'max_rating_sec_d_add'+newId;
        var fnAppraiseeRatingSecD = 'appraisee_rating_sec_d_add'+newId;
        var fnAppraiserRatingSecD = 'appraiser_rating_sec_d_add'+newId;
        var fnAgreedRatingSecD = 'agreed_rating_sec_d_add'+newId;

        //get the options for the drop down of the objectives, here I assume the first drop down will have the values
        var firstObjectiveDropDown = document.getElementsByName('objective_0').item(0);
        var objectiveOptions = firstObjectiveDropDown.innerHTML;

        childHtml =
            '<div class="row">'+
            '<div class="col m1 s12 ">'+ newId + '</div>'+
            '<div class="col m11 s12" style="padding-left: 1.5%">'+
            '<select  name="'+fnObjective+'" required class="browser-default validate s12" style="width: 100%">'+
            objectiveOptions+
            '</select>'+
            '</div>'+
            '</div>'+

            '<div class="row">'+

            '<div class="col s7">'+

            '<div class="col s2 ">'+
            '</div>'+
            '<div class="col s5 ">'+
            '<textarea id="'+fnExpectedOutputSecD+'" name="'+fnExpectedOutputSecD+'" type="text" class="validate"></textarea>'+
            '</div>'+
            '<div class="col s5 ">'+
            '<textarea id="'+fnActualPerformance+'" name="'+fnActualPerformance+'" type="text" class="validate"></textarea>'+
            '</div>'+

            '</div>'+

            '<div class="col s5">'+

            '<div class="col s3 ">'+
            '<input id="'+fnMaxRatingSecD+'" min="0" name="'+fnMaxRatingSecD+'" type="number" onblur="sumColumnRows(\'max_rating_sec_d_add\',5,\'sec_d_add_total_max_rating\',20)" class="validate browser-default tab-input">'+
            '</div>'+
            '<div class="col s3 ">'+
            '<input id="'+fnAppraiseeRatingSecD+'" min="0" name="'+fnAppraiseeRatingSecD+'" onblur="sumColumnRows(\'appraisee_rating_sec_d_add\',5,\'sec_d_add_total_appraisee_rating\',20)" type="number" class="validate browser-default tab-input">'+
            '</div>'+
            '<div class="col s3 ">'+
            '<input id="'+fnAppraiserRatingSecD+'" min="0" name="'+fnAppraiserRatingSecD+'" onblur="sumColumnRows(\'appraiser_rating_sec_d_add\',5,\'sec_d_add_total_appraiser_rating\',20)" type="number" class="validate browser-default tab-input">'+
            '</div>'+
            '<div class="col s3 ">'+
            '<input id="'+fnAgreedRatingSecD+'" min="0" name="'+fnAgreedRatingSecD+'" onblur="sumColumnRows(\'agreed_rating_sec_d_add\',5,\'sec_d_add_total_agreed_rating\',20)" type="number" class="validate browser-default tab-input">'+
            '</div>'+
            '</div>'+

            '</div>';

        return childHtml;

    }
    else if(parentId == 'parent_dynamic_weaknesses'){

        var fnWeakness= "weakness_"+newId;
        var fnWeaknessMitigation = 'mitigation_measure_'+newId;
        var fnImprovementPlan = 'improvement_plan_'+newId;
        var fnTimeFrame = 'time_frame_'+newId;
        var fnSupervisorComment = 'supervisor_comment_'+newId;

        childHtml =
            '<div class="col s12">'+
                '<div class="col s3 ">'+
                    '<textarea id="'+fnWeakness+'" name="'+fnWeakness+'" type="text" class="validate"></textarea>'+
                '</div>'+
                '<div class="col s3 ">'+
                    '<textarea id="'+fnWeaknessMitigation+'" name="'+fnWeaknessMitigation+'" type="text" class="validate"></textarea>'+
                '</div>'+
                '<div class="col s3 ">'+
                    '<textarea id="'+fnImprovementPlan+'" name="'+fnImprovementPlan+'" type="text" class="validate"></textarea>'+
                '</div>'+
                '<div class="col s1 ">'+
                    '<input id="'+fnTimeFrame+'" name="'+fnTimeFrame+'" type="text" class="validate browser-default tab-input">'+
                '</div>'+
                '<div class="col s2 ">'+
                '<textarea id="'+fnSupervisorComment+'" name="'+fnSupervisorComment+'" type="text" class="validate"></textarea>'+
                '</div>'+
            '</div>';
        return childHtml;

    }
    else if(parentId == 'parent_dynamic_challenges'){

        var fnChallenge = "challenge_"+newId;
        var fnChallengeCause = 'challenge_cause_'+newId;
        var fnChallengeRecommendation = 'challenge_recommendation_'+newId;
        var fnChallengeWhen = 'challenge_when_'+newId;

        childHtml =
            '<div class="col s12">'+
            '<div class="col s1 ">' + newId+
            '</div>'+
            '<div class="col s3 ">'+
            '<textarea id="'+fnChallenge+'" name="'+fnChallenge+'" type="text" class="validate"></textarea>'+
            '</div>'+
            '<div class="col s3 ">'+
            '<textarea id="'+fnChallengeCause+'" name="'+fnChallengeCause+'" type="text" class="validate"></textarea>'+
            '</div>'+
            '<div class="col s3 ">'+
            '<textarea id="'+fnChallengeRecommendation+'" name="'+fnChallengeRecommendation+'" type="text" class="validate"></textarea>'+
            '</div>'+
            '<div class="col s2 ">'+
            '<input id="'+fnChallengeWhen+'" name="'+fnChallengeWhen+'" type="text" class="validate browser-default tab-input">'+
            '</div>'+
            '</div>';
        return childHtml;

    }
    else if(parentId == 'parent_dynamic_performance_gaps'){

        var fnGap = "gap_"+newId;
        var fnGapCause = 'cause_'+newId;
        var fnGapRecommendation = 'recommendation_'+newId;
        var fnGapWhen = 'when_'+newId;

        childHtml =
            '<div class="col s12">'+
                '<div class="col s1 ">' + newId+
                '</div>'+
                '<div class="col s3 ">'+
                '<textarea id="'+fnGap+'" name="'+fnGap+'" type="text" class="validate"></textarea>'+
                '</div>'+
                '<div class="col s3 ">'+
                '<textarea id="'+fnGapCause+'" name="'+fnGapCause+'" type="text" class="validate"></textarea>'+
                '</div>'+
                '<div class="col s3 ">'+
                '<textarea id="'+fnGapRecommendation+'" name="'+fnGapRecommendation+'" type="text" class="validate"></textarea>'+
                '</div>'+
                '<div class="col s2 ">'+
                '<input id="'+fnGapWhen+'" name="'+fnGapWhen+'" type="text" class="validate browser-default tab-input">'+
                '</div>'+
            '</div>';
        return childHtml;

    }
    else if(parentId == 'parent_dynamic_agreed_targets'){

        var fnWpAssignment = "key_result_area_"+newId;
        var fnWpExpectedOutput = 'key_performance_indicator_'+newId;
        var fnWpMaxRating = 'max_rating'+newId;
        var fnWpTimeFrame = 'time_frame_'+newId;
        var fnWpRecordIdCount = 'record_id_count'+newId;

        childHtml =
            '<div class="row ">'+
                '<div class="col m4 s12 ">'+
                '<textarea id="'+fnWpAssignment+'" name="'+fnWpAssignment+'" type="text" class="validate"></textarea>'+
                '</div>'+
                '<div class="col m4 s12 ">'+
                '<textarea id="'+fnWpExpectedOutput+'" name="'+fnWpExpectedOutput+'" type="text" class="validate"></textarea>'+
                '</div>'+
                '<div class="col m2 s12 ">'+
                '<input id="'+fnWpMaxRating+'" name="'+fnWpMaxRating+'"  step="0.25" type="number" class="validate browser-default tab-input">'+
                '</div>'+
                '<div class="col m2 s12 ">'+
                '<input id="'+fnWpTimeFrame+'" name="'+fnWpTimeFrame+'" type="text" class="validate browser-default tab-input">'+
                '</div>'+
                '<input type="hidden" name="'+fnWpRecordIdCount+'" value="'+newId+'">'+
            '</div>';
        return childHtml;

    }
    else if(parentId == 'parent_dynamic_workplan'){

        var fnWpAssignment = "assignment_"+newId;
        var fnWpExpectedOutput = 'expected_output_'+newId;
        var fnWpMaxRating = 'max_rating'+newId;
        var fnWpTimeFrame = 'time_frame_'+newId;
        var fnWpRecordIdCount = 'record_id_count'+newId;

        childHtml =
            '<div class="row ">'+
            '<div class="col m1 s12 ">'+ newId+ '</div>'+
            '<div class="col m4 s12 ">'+
            '<textarea id="'+fnWpAssignment+'" name="'+fnWpAssignment+'" type="text" class="validate"></textarea>'+
            '</div>'+
            '<div class="col m4 s12 ">'+
            '<textarea id="'+fnWpExpectedOutput+'" name="'+fnWpExpectedOutput+'" type="text" class="validate"></textarea>'+
            '</div>'+
            '<div class="col m1 s12 ">'+
            '<input id="'+fnWpMaxRating+'" name="'+fnWpMaxRating+'" type="number" class="validate browser-default tab-input">'+
            '</div>'+
            '<div class="col m2 s12 ">'+
            '<input id="'+fnWpTimeFrame+'" name="'+fnWpTimeFrame+'" type="text" class="validate browser-default tab-input">'+
            '</div>'+
            '<input type="hidden" name="'+fnWpRecordIdCount+'" value="'+newId+'">'+
            '</div>';
        return childHtml;

    }
    else{
        return "Invalid Row Parent ID Supplied";
    }

    return childHtml;

}

function addElement(parentId, elementTag, elementCounterRows) {

    // Adds an element to the document
    var parentElem = document.getElementById(parentId);
    var rowCounterElem = document.getElementById(elementCounterRows);

    //get the number of children in the parent
    //var countChildren = parentElem.childElementCount == null ? 0 : parentElem.childElementCount;
    var countChildren = rowCounterElem == null || rowCounterElem.value == null ? 0 : rowCounterElem.value;

    //increment that number to get the new id
    var newId = (parseInt(countChildren)) + 1;

    //create the holder element
    var newElement = document.createElement(elementTag);

    // newElement.setAttribute('id', elementId);
    newElement.setAttribute('class', 'row');

    if(parentId === 'parent_dynamic_employee_performance'){
        newElement.setAttribute('class', newId%2 !== 0 ? 'row-strip-dark padding-5':'row-strip-light padding-5');
    }

    //this is the content
    var childHtml = generateChildRowHtml(newId, parentId);

    newElement.innerHTML = childHtml;

    parentElem.appendChild(newElement);

    //now we set the counter of the number of rows
    rowCounterElem.value = parentElem.childElementCount;//newId;

}

function deleteLastElement(parentId, elementCounterRows) {

    // Adds an element to the document
    var parentElem = document.getElementById(parentId);

    //the person is trying to remove everything
    if(parentElem.childElementCount <= 1){
        alert('You cannot remove all fields');
        return;
    }

    //get the last element
    var lastElem = parentElem.lastElementChild;

    //remove the last element
    parentElem.removeChild(lastElem);

    //now we set the counter of the number of rows
    var rowCounterElem = document.getElementById(elementCounterRows);
    rowCounterElem.value = parentElem.childElementCount;

    //if we delete from a section that totals, we clear the totals

}

function setMax(elemValueHolder, elemValuePlaceID1, elemValuePlaceID2, elemValuePlaceID3){

    try{

        var data = $(elemValueHolder).val();

        if(!isNaN(data)){

            $('#'+elemValuePlaceID1).attr({  "max" : data });
            $('#'+elemValuePlaceID2).attr({  "max" : data });
            $('#'+elemValuePlaceID3).attr({  "max" : data });


        }

    }catch (e) {

    }

}

function checkMaxValue(currentElem) {

    try{

        //get the max value attribute
        $maxValue = $(currentElem).attr('max');
        //get the input value
        $inputValue = $(currentElem).val();

        //if any of them is not a number chill it
        if(isNaN($maxValue) || isNaN($inputValue) || $maxValue === '' || $inputValue === ''){
            return;
        }


        if (parseFloat($inputValue) <= parseFloat($maxValue)){
        //if($inputValue <= $maxValue){
            $(currentElem).removeClass('invalid-value');
            return;
        }

        //here the input is greater than the max
        alert("Field value can't be more than "+$maxValue+ ". Field value reset, enter valid value");
        $(currentElem).val('');
        $(currentElem).addClass('invalid-value')

    }catch (e) {

    }

}

