
@extends('layouts.master')
@section('title')
    App Selection | {{config('app.name')}}
@endsection

@section('page-level-css')
@endsection

@section('page-level-js')
@endsection

@section('content')
    <header>
        @include('includes.navbar_guest')
    </header>

    <main >

        <div class="parallax-container">
            <div class="container " style="margin-top: 8%" >


               <div class="row">
                   <div class="col l6 offset-l3 m6 offset-m3 s12 center ">
                       <div class=" white-text timo-transparent-bg">
                           <h6>Welcome @if(session()->has('user')){{strtoupper(session('user')->fullName)}}@else{{'USER'}}@endif, Please Select the Application You Want To Access</h6>
                       </div>
                   </div>
               </div>

                <div class="row padding-top-180">
                    <div  style="padding: 20px; background-color: #cecece" class="col card  m8 offset-m2 l8 offset-l2 s12 timo-grey-text ">

                        <div class="col center m6 l6 s12 ">
                            <div class="text-center">
                                <h4 class="tt-headline">
                                    <span class="tt-words-wrapper timo-primary-text"><b>Letter Movement</b></span>
                                </h4>
                                <p class="sub-intro lead ">Create, View and Submit</p>
                                <a class="btn camel-case mt-30 waves-effect waves-light timo-primary" href="{{route('users.access-app',['letter-movement'])}}">Access Application</a>
                            </div>
                        </div>

                        <div class="col center m6 l6 s12 ">
                        <div class="text-center">
                            <h4 class="tt-headline loading-bar">
                                <span class="tt-words-wrapper timo-primary-text"> <b>Staff Appraisal</b> </span>
                            </h4>
                            <p class="sub-intro lead ">Create, Submit and Approve</p>
                            <a class="btn camel-case mt-30 waves-effect waves-light timo-primary" href="{{route('users.access-app',['staff-appraisal'])}}">Access Application</a>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
            <div class="parallax"><img src="{{asset('images/wall_2.jpg')}}">
            </div>
        </div>

    </main>

    {{-- Message modal --}}
    @if(isset($isError) && isset($msg))  @include('includes.modal-message')   @endif

@endsection
