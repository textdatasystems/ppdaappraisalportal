<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="icon" href="favicon.ico?v=2" type="image/x-icon"/>
    <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.custom.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ URL::asset('css/materialize.min.css') }}" type="text/css"/>
    @yield('page-level-css')
    {{--<link rel="stylesheet" href="{{ URL::asset('css/styles-min.css') }}" type="text/css"/>--}}
    <link rel="stylesheet" href="{{ URL::asset('css/styles.css') }}" type="text/css"/>

    <script src="{{ URL::asset('js/jquery.min.js') }}" type="text/javascript"> </script>
    <script src="{{ URL::asset('js/moment.min.js') }}" type="text/javascript"> </script>
    <script src="{{ URL::asset('js/jquery-ui.js') }}" type="text/javascript"> </script>
    <script src="{{ URL::asset('js/jquery-ui.custom.js') }}" type="text/javascript"> </script>
    <script src="{{ URL::asset('materialize/js/materialize.js') }}" type="text/javascript"> </script>
    <script src="{{ URL::asset('js/jquery.validate.min.js') }}" type="text/javascript"> </script>
    @yield('page-level-js')

    <script src="{{ URL::asset('js/scripts.js') }}" type="text/javascript"> </script>
    {{--<script src="{{ URL::asset('js/scripts-min.js') }}" type="text/javascript"> </script>--}}
    <script src="{{ URL::asset('js/pagination.js') }}" type="text/javascript" ></script>
    <script src="{{ URL::asset('js/appraisal.js') }}" type="text/javascript" ></script>



</head>
<body>

<div id="pdf-dialog" style="display:none;">
    <div >
        <iframe id="iframe-pdf-viewer" style="width: 100%;height: 600px" src=""></iframe>
    </div>
</div>


@yield('content')

<!-- BEGIN: Footer-->
<footer class="page-footer footer blue darken-4">
    <div class="footer-copyright">
        <div class="container"><span>&copy; {{date('Y')}} <a class="" href="#" target="_blank">PPDA</a> All rights reserved.</span></div>
    </div>
</footer>
<!-- END: Footer-->


<!-- #start useful modals that we want to show -->

<!-- #start shown on save the appraisal Section A for the first time -->
@include('includes.modal_confirm_workflow_type')
<!-- #end shown on save the appraisal Section A for the first time -->

<!-- #start shows success or failure message -->
@include('includes.modal_custom_message')
<!-- #end shows success or failure message -->

<!-- #end useful modals that we want to show  -->



<!-- inline scripts related to this page -->
<script type="text/javascript">

    function openModal(modalId,show) {

        /**
         * open/hide materialize css modal
         */
        if(show){
            $('#'+modalId).modal('open');
        }else{
            $('#'+modalId).modal('close');
        }

    }


    function showCustomMessageModal(isError,message) {

        /**
         * custom message modal
         */
        var className = isError ? 'red-text' : 'green-text';
        var title = isError ? 'Operation Failed' : 'Operation Successful';
        $('#modal_custom_message .title').removeClass('red-text').removeClass('green-text');
        $('#modal_custom_message .title').addClass(className);
        $('#modal_custom_message .title').html(title);
        $('#modal_custom_message .message').html(message);
        openModal('modal_custom_message', true);

    }


    jQuery(function ($) {

        $('body').on('click', '.dismiss', function () {
            event.preventDefault();
            $(this).closest('.modal').modal('close');
        });

        $('body').on('click', '#btnSaveSection', function () {

            event.preventDefault();

            /**
             * user is creating the appraisal for the first time. show confirmation of workflow type
             * */
            var workflowType = $('#appraisal_workflow_type').val();
            if(workflowType == null || workflowType == ""){
                showCustomMessageModal(true,"Please select appraisal workflow type");
                return;
            }

            $('#modal_confirm_workflow_type_selected_value').html(workflowType);
            openModal('modal_confirm_workflow_type', true);

        });

        $('body').on('click', '#btnConfirmWorkflowType', function () {

            event.preventDefault();

            /**
             * submit section 1 for the first time
             */
            $('#section_1_form').submit();

        });

        $('body').on('change', '#appraisal_workflow_type', function () {

            var workflowType = $(this).val();
            if(workflowType == null || workflowType == ""){
                $('.container_supervisor').css('display','none');
                $('.container_hod').css('display','none');
                $('.container_ed').css('display','none');
                return;
            }

            var workFlowTypeCode = $(this).children("option:selected").attr('data-workflow-type-code');
            if(workFlowTypeCode === 'normal'){
                $('.container_supervisor').css('display','block');
                $('.container_hod').css('display','block');
                $('.container_ed').css('display','block');
            }else if(workFlowTypeCode === 'managers'){
                $('.container_supervisor').css('display','none');
                $('.container_hod').css('display','block');
                $('.container_ed').css('display','block');
            }else if(workFlowTypeCode === 'directors'){
                $('.container_supervisor').css('display','none');
                $('.container_hod').css('display','none');
                $('.container_ed').css('display','block');
            }

        });


    });

</script>


</body>
</html>
 
