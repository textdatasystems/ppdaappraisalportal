
<div id="modal_confirm_workflow_type" class="modal delete-modal">

    <div class="modal-content">
        <h5 class="">Action Required</h5>
        <p>Are you sure to want to save the Appraisal with the Workflow Type:
            <span class="text-primary" style="display: inline-block;" id="modal_confirm_workflow_type_selected_value"></span>
        </p>
    </div>

    <div class="modal-footer">
        <a href="#" class="waves-effect waves-red btn-flat dismiss pink-text">Cancel</a>
        <a href="#" class="waves-effect waves-green btn-flat green-text" id="btnConfirmWorkflowType">Confirm</a>
    </div>

</div>