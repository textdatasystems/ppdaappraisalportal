<div id="modal_custom_message" class="modal modal-success-confirmation">
    <div class="modal-content">
        <h5 class="red-text title"></h5>
        <p class="message"></p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat camel-case">Close</a>
    </div>
</div>