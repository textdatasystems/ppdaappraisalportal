<div style="width: 100%" class="grey lighten-2">
<div class="container ">
    <div class="row " id="logo-section">
        <div class="col l5 m12 s12 left valign-wrapper"><img src="{{URL::asset('images/ppda-logo.png')}}"></div>
        <div class="col l4 m12 s12 left valign-wrapper"><span class="red-text">[Trial Version]</span></div>
        <div class="col l3 m12 s12 right valign-wrapper btn-group-minier" style="padding-left: 3%;padding-right: 0">
            <a  href="{{route('back-to-ppda-apps')}}" class="btn btn-small timo-primary camel-case right"><i class="material-icons small left">home</i>Back to PPDA Apps</a>
            <a href="{{route('singout_admin')}}" class="btn btn-small red camel-case right"><i class="material-icons small left">lock_open</i>Logout</a>
        </div>
    </div>
</div>
</div>

<nav>
    <div class="nav-wrapper blue darken-4 nav-border-bottom">

        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <span>WELCOME @if(session()->has('user')){{session('user')->fullName}}@else{{'USER'}}@endif</span>
        <ul class="right hide-on-med-and-down nav-bar-customize ">
            @if(session('user'))
                <li>
                    <a class="@if($active_module == 'my_appraisals') active-module @endif nav-border-left nav-border-right" href="{{route('appraisal-forms.owner')}}">My Appraisals</a>
                </li>
                <li>
                    <a class="@if($active_module == 'supervisor_appraisals') active-module @endif nav-border-right" href="{{route('appraisal-forms.supervisor')}}">Supervisor Approvals</a>
                </li>
                <li>
                    <a class="@if($active_module == 'hod_appraisals') active-module @endif nav-border-right" href="{{route('appraisal-forms.hod')}}">HOD Approvals</a>
                </li>
                <li>
                    <a class="@if($active_module == 'director_appraisals') active-module @endif nav-border-right" href="{{route('appraisal-forms.director')}}">Executive Director Approvals</a>
                </li>

                @if(session('user')->roleCode == 'HUMAN RESOURCE USER')
                <li>
                    <a class="@if($active_module == 'hr') active-module @endif nav-border-right " href="{{route('human-resource.index')}}" >Human Resource</a>
                </li>
                @endif
                <li>
                    <a class="@if($active_module == 'profiles') active-module @endif nav-border-right " href="{{route('users.profile')}}" >Profile</a>
                </li>

            @endif
        </ul>

        <ul class="side-nav" id="mobile-demo">
            @if(session('user'))
                <li>
                    <a class="@if($active_module == 'my_appraisals') active-module @endif nav-border-left nav-border-right" href="{{route('appraisal-forms.owner')}}">My Appraisals</a>
                </li>
                <li>
                    <a class="@if($active_module == 'supervisor_appraisals') active-module @endif nav-border-right" href="{{route('appraisal-forms.supervisor')}}">Supervisor Approvals</a>
                </li>
                <li>
                    <a class="@if($active_module == 'hod_appraisals') active-module @endif nav-border-right" href="{{route('appraisal-forms.hod')}}">HOD Approvals</a>
                </li>
                <li>
                    <a class="@if($active_module == 'director_appraisals') active-module @endif nav-border-right" href="{{route('appraisal-forms.director')}}">Executive Director Approvals</a>
                </li>
                @if(session('user')->roleCode == 'HUMAN RESOURCE USER')
                <li>
                    <a class="@if($active_module == 'hr') active-module @endif nav-border-right " href="{{route('human-resource.index')}}" >Human Resource</a>
                </li>
                @endif
                <li>
                    <a class="@if($active_module == 'profiles') active-module @endif nav-border-right " href="{{route('users.profile')}}" >Profile</a>
                </li>
            @endif
        </ul>

    </div>
</nav>


