
<div id="modal_delete" class="modal delete-modal">

    <div class="modal-content">
        <h5 class="red-text">Please Confirm</h5>
        <p>Are you sure to want to delete <span id="item_group">Item Group</span>, [<span id="item_name">Item Name</span>]?</p>
    </div>

    <div class="modal-footer">
        <a href="#" class="waves-effect waves-red btn-flat" onclick=" $('#modal_delete').modal('close'); return false;">No</a>
        <a href="#" class="waves-effect waves-green btn-flat" id="modal_delete_YesBtn">Yes</a>
    </div>

</div>