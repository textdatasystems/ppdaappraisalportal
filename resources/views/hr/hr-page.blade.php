@extends('layouts.master')
@section('title')
{{config('app.name')}} | Human Resource
@endsection

@section('page-level-css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/css/select.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('pixinvent/app-assets/css/data-tables.css')}}">
@endsection

@section('content')
<header>
    @include('includes.nav-bar-general')
</header>
<main class="container">

    <div class="row">
        <div class="col s12">
            <ul class="tabs blue darken-1">
                <li class="tab col m3 "><a class="white-text {{!isset($statusFilter) ? 'active':''}} camel-case" href="#tab_contracts">Employee Contracts</a></li>
                <li class="tab col m3"><a  class="white-text {{isset($statusFilter) ? 'active':''}}  camel-case" href="#tab_appraisals_incomplete_all">Incomplete (All)</a></li>
                <li class="tab col m3"><a  class="white-text camel-case" href="#tab_appraisals_incomplete_pending_ed">Incomplete (Pending ED Approval)</a></li>
                <li class="tab col m3"><a  class="white-text camel-case" href="#tab_appraisals_completed">Completed Appraisals</a></li>

            </ul>
        </div>
    </div>

    <div id="tab_contracts" class="row ">

        <div  class="col s12">
            <div class="card">
                <div class="card-content">
                    <div class="row" style="margin-bottom: 5px">
                        <div class="col s6">
                            <h5 class="card-title grey-text text-darken-1">Employee Contracts</h5>
                        </div>
                        <div class="col s6"></div>
                    </div>
                    <div class="hr-dotted spacer-bottom"></div>
                    <div class="row">

                        <div class="col s12 dt_wrapper" id="hr-table-container">
                            <table id="page-length-option"  class="display responsive-table table-tiny-text">
                                <thead>
                                <tr class="timo-table-headers">
                                    <th style="width: 5%;">#</th>
                                    <th style="width: 15%;">Username</th>
                                    <th style="width: 15%;">Employee Name</th>
                                    <th style="width: 10%;">Region</th>
                                    <th style="width: 20%;">Department</th>
                                    <th style="width: 10%;">Designation</th>
                                    <th style="width: 10%;">Contract Start Date</th>
                                    <th style="width: 10%;">Contract End Date</th>
                                    <th style="width: 5%">Contract History</th>
                                    <th style="width: 5%">Academics</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if(isset($users) && count($users) > 0)
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$user->username}}</td>
                                            <td>{{$user->fullName}}</td>
                                            <td>{{$user->regionalOfficeName}}</td>
                                            <td>{{$user->departmentName}}</td>
                                            <td>{{$user->designation}}</td>
                                            <td>{{$user->contractStartDate}}</td>
                                            <td>{{$user->contractExpiryDate}}</td>
                                            <td class="center" ><a data-source="{{route('human-resource.user-contracts',[$user->username])}}" class="center red-text modal-trigger hr-emp-details-button" id="{{$user->username}}" href="#modal_hr_contract_history"><i class="material-icons">history</i></a></td>
                                            <td class="center" ><a data-source="{{route('human-resource.user-academic-bg',[$user->username])}}" class="center red-text modal-trigger btn-hr-academic-bg" id="{{$user->username}}" href="#modal_hr_academic_bg"><i class="material-icons">school</i></a></td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9" class="center">No Users Founds</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="tab_appraisals_incomplete_all" class="row ">

        <div  class="col s12">
            <div class="card">
                <div class="card-content">
                    <div class="row" style="margin-bottom: 5px">
                        <div class="col s6">
                            <h5 class="card-title grey-text text-darken-1">Incomplete Appraisals</h5>
                        </div>
                        <div class="col s6"></div>
                    </div>
                    <div class="hr-dotted spacer-bottom"></div>
                    <div class="row">

                        <div class="col s12 btn-group-minier" style="margin-top: 10px;margin-bottom: 10px">
                            <label>Filter by Status:</label>
                            <select class="browser-default" style="width: 200px !important;" id="ddAppraisalStatus">
                                <option value="">All</option>
                                <option value="{{PENDING_SUPERVISOR_APPROVAL}}" {{ isset($statusFilter) && $statusFilter == PENDING_SUPERVISOR_APPROVAL ? 'selected' : '' }} >Pending Just Supervisor</option>
                                <option value="{{PENDING_DEPARTMENT_HEAD_APPROVAL}}" {{ isset($statusFilter) && $statusFilter == PENDING_DEPARTMENT_HEAD_APPROVAL ? 'selected' : '' }}>Pending Just HOD</option>
                                <option value="{{PENDING_EXECUTIVE_DIRECTOR_APPROVAL}}"  {{ isset($statusFilter) && $statusFilter == PENDING_EXECUTIVE_DIRECTOR_APPROVAL ? 'selected' : '' }}>Pending Just ED</option>
                            </select>
                            <a href="{{route('human-resource.index.filtered',['PARAM_STATUS_CODE'])}}" class="btn btn-small" style="text-transform: capitalize;background: #0D47A1;margin-left: 2em" id="btnSearchIncompleteAppraisalsByStatus">Apply Filter</a>
                        </div>

                        <div class="col l12 s12 dt_wrapper">
                            <table id="page-length-option-b"  class="bordered display table-tiny-text">
                                <thead class="timo-table-headers">
                                <tr>
                                    <th style="width:3%">#</th>
                                    <th style="width:18%">Appraisal</th>
                                    <th style="width:14%">Employee Name</th>
                                    <th style="width:10%">Appraisal Type</th>
                                    <th style="width:14%">Start Date</th>
                                    <th style="width:14%">End Date</th>
                                    <th style="width:8%">Supervisor Action</th>
                                    <th style="width:8%">HOD Action</th>
                                    <th style="width:8%">ED Action</th>
                                    <th data-orderable="false" style="width:5%">Action</th>

                                </tr>
                                </thead>
                                <tbody>

                                @if(isset($appraisalsIncomplete) && count($appraisalsIncomplete) > 0)
                                    @foreach($appraisalsIncomplete as $appraisal)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$appraisal->generatedPdfName}}</td>
                                            <td>{{$appraisal->personalInfo->firstName .' '. $appraisal->personalInfo->lastName}}</td>
                                            <td>{{$appraisal->appraisalType}}</td>
                                            <td>{{$appraisal->personalInfo->appraisalPeriodStartDate}}</td>
                                            <td>{{$appraisal->personalInfo->appraisalPeriodEndDate}}</td>
                                            <td>{{$appraisal->supervisorDecision}}</td>
                                            <td>{{$appraisal->deptHeadDecision}}</td>
                                            <td>{{$appraisal->executiveDirectorDecision}}</td>
                                            <td class="center"><a href="#!" data-constrainwidth="false" data-hover="true" data-beloworigin="true"   data-activates='dd_incomplete_all_{{$appraisal->appraisalRef}}' class="green-text dropdown-trigger"><i class="material-icons">list</i></a></td>
                                            <!-- Dropdown Structure -->
                                            <ul id='dd_incomplete_all_{{$appraisal->appraisalRef}}' class='dropdown-content'>
                                                <li><a href="{{route('appraisal-forms.show',[$appraisal->appraisalRef])}}"><i class="material-icons">visibility</i>View Appraisal</a></li>
                                                <li><a data-source="{{route('appraisal-form.appraisal-comments',[$appraisal->appraisalRef])}}" class="modal-trigger modal-trigger-with-server-side-data btn-view-comments" href="#modal_comments"><i class="material-icons">comment</i>View Comments</a></li>
                                                <li><a class="modal-trigger edit-button pdf-link" data-download-url = "{{route('download-appraisal-pdf',$appraisal->appraisalRef)}}" href="#!"><i class="material-icons center">insert_drive_file</i>View PDF</a></li>
                                            </ul>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="10" class="center">No Incomplete Appraisals Found</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="tab_appraisals_incomplete_pending_ed" class="row ">

        <div  class="col s12">
            <div class="card">
                <div class="card-content">
                    <div class="row" style="margin-bottom: 5px">
                        <div class="col s6">
                            <h5 class="card-title grey-text text-darken-1">Incomplete Appraisals (Pending ED Approval)</h5>
                        </div>
                        <div class="col s6"></div>
                    </div>
                    <div class="hr-dotted spacer-bottom"></div>
                    <div class="row">

                        <div class="col l12 s12 dt_wrapper">
                            <table id="page-length-option-c"  class="bordered display table-tiny-text">
                                <thead class="timo-table-headers">
                                <tr>
                                    <th style="width:3%">#</th>
                                    <th style="width:18%">Appraisal</th>
                                    <th style="width:14%">Employee Name</th>
                                    <th style="width:10%">Appraisal Type</th>
                                    <th style="width:14%">Start Date</th>
                                    <th style="width:14%">End Date</th>
                                    <th style="width:8%">Supervisor Action</th>
                                    <th style="width:8%">HOD Action</th>
                                    <th style="width:8%">ED Action</th>
                                    <th data-orderable="false" style="width:5%">Action</th>

                                </tr>
                                </thead>
                                <tbody>

                                @if(isset($appraisalsIncompletePendingEd) && count($appraisalsIncompletePendingEd) > 0)
                                    @foreach($appraisalsIncompletePendingEd as $appraisal)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$appraisal->generatedPdfName}}</td>
                                            <td>{{$appraisal->personalInfo->firstName .' '. $appraisal->personalInfo->lastName}}</td>
                                            <td>{{$appraisal->appraisalType}}</td>
                                            <td>{{$appraisal->personalInfo->appraisalPeriodStartDate}}</td>
                                            <td>{{$appraisal->personalInfo->appraisalPeriodEndDate}}</td>
                                            <td>{{$appraisal->supervisorDecision}}</td>
                                            <td>{{$appraisal->deptHeadDecision}}</td>
                                            <td>{{$appraisal->executiveDirectorDecision}}</td>
                                            {{--<td><a class="red-text modal-trigger edit-button" href="{{$appraisal->pdfDownloadLink}}"><i class="material-icons center">insert_drive_file</i></a></td>--}}
                                            <td class="center"><a href="#!" data-constrainwidth="false" data-hover="true" data-beloworigin="true"   data-activates='dd_incomplete_pending_ed_{{$appraisal->appraisalRef}}' class="green-text dropdown-trigger"><i class="material-icons">list</i></a></td>
                                            <!-- Dropdown Structure -->
                                            <ul id='dd_incomplete_pending_ed_{{$appraisal->appraisalRef}}' class='dropdown-content'>
                                                <li><a href="{{route('appraisal-forms.show',[$appraisal->appraisalRef])}}"><i class="material-icons">visibility</i>View Appraisal</a></li>
                                                <li><a data-source="{{route('appraisal-form.appraisal-comments',[$appraisal->appraisalRef])}}" class="modal-trigger modal-trigger-with-server-side-data btn-view-comments" href="#modal_comments"><i class="material-icons">comment</i>View Comments</a></li>
                                                <li><a class="modal-trigger edit-button pdf-link" data-download-url = "{{route('download-appraisal-pdf',$appraisal->appraisalRef)}}" href="#!"><i class="material-icons center">insert_drive_file</i>View PDF</a></li>
                                            </ul>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="10" class="center">No Incomplete Appraisals Found</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="tab_appraisals_completed" class="row ">

        <div  class="col s12">
            <div class="card">
                <div class="card-content">
                    <div class="row" style="margin-bottom: 5px">
                        <div class="col s6">
                            <h5 class="card-title grey-text text-darken-1">Completed Appraisals</h5>
                        </div>
                        <div class="col s6"></div>
                    </div>
                    <div class="hr-dotted spacer-bottom"></div>
                    <div class="row">

                        <div class="col l12 s12 dt_wrapper">
                            <table id="page-length-option-a"  class="bordered display table-tiny-text">
                                <thead class="timo-table-headers">
                                <tr>
                                    <th style="width:3%">#</th>
                                    <th style="width:24%">Appraisal</th>
                                    <th style="width:14%">Employee Name</th>
                                    <th style="width:10%">Appraisal Type</th>
                                    <th style="width:10%">Start Date</th>
                                    <th style="width:10%">End Date</th>
                                    <th style="width:8%">Supervisor Action</th>
                                    <th style="width:8%">HOD Action</th>
                                    <th style="width:8%">ED Action</th>
                                    {{--<th data-orderable="false" style="width:5%">Pdf</th>--}}
                                    <th data-orderable="false" style="width:5%">Action</th>

                                </tr>
                                </thead>
                                <tbody>

                                @if(isset($appraisals) && count($appraisals) > 0)
                                    @foreach($appraisals as $appraisal)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$appraisal->generatedPdfName}}</td>
                                            <td>{{$appraisal->personalInfo->firstName .' '. $appraisal->personalInfo->lastName}}</td>
                                            <td>{{$appraisal->appraisalType}}</td>
                                            <td>{{$appraisal->personalInfo->appraisalPeriodStartDate}}</td>
                                            <td>{{$appraisal->personalInfo->appraisalPeriodEndDate}}</td>
                                            <td>{{$appraisal->supervisorDecision}}</td>
                                            <td>{{$appraisal->deptHeadDecision}}</td>
                                            <td>{{$appraisal->executiveDirectorDecision}}</td>
                                            {{--<td><a class="red-text modal-trigger edit-button" href="{{$appraisal->pdfDownloadLink}}"><i class="material-icons center">insert_drive_file</i></a></td>--}}
                                            <td class="center"><a href="#!" data-constrainwidth="false" data-hover="true" data-beloworigin="true"   data-activates='dd_completed_{{$appraisal->appraisalRef}}' class="green-text dropdown-trigger"><i class="material-icons">list</i></a></td>
                                            <!-- Dropdown Structure -->
                                            <ul id='dd_completed_{{$appraisal->appraisalRef}}' class='dropdown-content'>
                                                <li><a href="{{route('appraisal-forms.show',[$appraisal->appraisalRef])}}"><i class="material-icons">visibility</i>View Appraisal</a></li>
                                                <li><a data-source="{{route('appraisal-form.appraisal-comments',[$appraisal->appraisalRef])}}" class="modal-trigger modal-trigger-with-server-side-data btn-view-comments" href="#modal_comments"><i class="material-icons">comment</i>View Comments</a></li>
                                                <li><a class="modal-trigger edit-button pdf-link" data-download-url = "{{route('download-appraisal-pdf',$appraisal->appraisalRef)}}" href="#!"><i class="material-icons center">insert_drive_file</i>View PDF</a></li>
                                            </ul>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="10" class="center">No Completed Appraisals Found</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@include('hr.modal-hr-contract-history')
@include('hr.modal-hr-academic-bg')

{{-- Message modal --}}
@if(isset($msg) && isset($isError))
    @include('includes.modal-message')
@endif

@include('appraisals.modal-view-appraisal-comments')

</main>
@endsection

@section('page-level-js')

    <script src="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/js/dataTables.select.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('pixinvent/app-assets/js/scripts/data-tables.js')}}" type="text/javascript"></script>

@endsection