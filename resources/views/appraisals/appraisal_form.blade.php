
@extends('layouts.master')
@section('title')
Staff Appraisal Form
@endsection
@section('page-level-css')
<link rel="stylesheet" href="{{ URL::asset('css/mstepper.min.css') }}" type="text/css"/>

<meta http-equiv="cache-control" content="no-cache, must-revalidate, post-check=0, pre-check=0" />
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
@endsection

@section('content')
<header> @include('includes.nav-bar-general') </header>

@if(count($errors->all()) > 0) @include('includes.modal-appraisal-form-error') @endif

@include('appraisals.template-objectives-list')

<main class="stepper-container" >

    {{-- This the trigger as to whether the user can edit the form or not, if no we add the class edit-off, if yes we dont --}}
    <div class="hide {{isset($editClass) ? $editClass : ''}}"></div>

    <div class="row">

        <div class="col s12 spacer-top ">
            <div class="col s12">
                <div class="col s12">
                    <div class="row valign-wrapper">
                        <div class="col s9 grey-text text-darken-1">
                            <div class="row">
                                <div class="col s5"><h5 >Staff Appraisal Form</h5></div>
                                <div class="col s7 ">
                                    @if(isset($appraisal) && !is_null($appraisal->comments) && count($appraisal->comments)>0)
                                        <a style="margin-top: 0.82rem;" data-source="{{route('appraisal-form.appraisal-comments',[$appraisal->appraisalRef])}}" class="left modal-trigger modal-trigger-with-server-side-data btn-view-comments timo-primary-text" href="#modal_comments"><i class="material-icons left">comment</i>View Comments({{count($appraisal->comments)}})</a>
                                        @include('appraisals.modal-view-appraisal-comments')
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col s3 right"><a href="{{isset($appraisal) ? $appraisal->redirectTo:URL::previous()}}" class=" timo-primary-text right ">BACK<i class="material-icons left ">keyboard_arrow_left</i></a></div>
                    </div>
                    <div class="col s12 hr-dotted spac  er-bottom"></div>
                    <div class="col s12 spacer-bottom step-by-step-registration">  @include('appraisals.stepped-registration-new') </div>
                    <div style="display: block" class="col s12">

                    @if(isset($appraisal))

                       @if(isset($appraisal) && !$appraisal->isOwner && $appraisal->ownerUsername != $user->username)

                           @if($appraisal->supervisorUsername == $user->username
                           || ($appraisal->appraisal_workflow_type == APPRAISAL_WORKFLOW_TYPE_MANAGERS && $appraisal->deptHeadUsername == $user->username  && $appraisal->status == PENDING_DEPARTMENT_HEAD_APPROVAL )
                           || ($appraisal->appraisal_workflow_type == APPRAISAL_WORKFLOW_TYPE_DIRECTORS && $appraisal->executiveDirectorUsername == $user->username && $appraisal->status == PENDING_EXECUTIVE_DIRECTOR_APPROVAL_AS_SUPERVISOR  )
                           )
                                    @include('appraisals.bottom-actions.appraiser-actions')
                            @elseif($appraisal->deptHeadUsername == $user->username)
                                    @include('appraisals.bottom-actions.hod-actions')
                            @elseif($appraisal->executiveDirectorUsername == $user->username)
                                    @include('appraisals.bottom-actions.executive-director-actions')
                           @endif

                       @elseif(isset($appraisal) && $appraisal->isOwner && !$appraisal->isCancelled && !$appraisal->isCompleted)

                           @include('appraisals.bottom-actions.appraisee-actions')

                       @endif

                    @endif
                    </div>
                </div>
            </div>
        </div>

    </div>

</main>

{{-- Incase of redicting to the previous page with errors, these dialogs where showing up again --}}
@if(count($errors->all()) < 1)

    @if(isset($formMessage) && strlen($formMessage) > 0))
    @include('includes.modal_form_message')
    @elseif((isset($msg) && isset($isError)))
        @include('includes.modal-message')
    @endif

@endif

@endsection
@section('page-level-js')
<script src="{{ URL::asset('js/mstepper.min.js') }}" type="text/javascript"> </script>
@endsection
