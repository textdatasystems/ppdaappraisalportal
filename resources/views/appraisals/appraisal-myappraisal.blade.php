@extends('layouts.master')

@section('title')
    {{config('app.name')}} | My Appraisals
@endsection

@section('page-level-css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/css/select.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('pixinvent/app-assets/css/data-tables.css')}}">
@endsection

@section('content')

    <header>
        @include('includes.nav-bar-general')
    </header>

    <!-- BEGIN: Page Main-->
    <div class="container" id="main">

        <div class="row spacer-top">

            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <div class="row" style="margin-bottom: 5px">
                            <div class="col s6">
                                <h5 class="card-title grey-text text-darken-1 ">APPRAISALS<span class="grey-text text-darken-1 "  style="font-size: small" >&nbsp;initiated by you</span></h5>
                            </div>
                            <div class="col s6">
                                <a class="btn blue darken-4 right camel-case" href="{{route('open_appraisal')}}">Create New Appraisal Form</a>
                            </div>
                        </div>
                        <div class="hr-dotted spacer-bottom"></div>
                        <div class="row">

                            <div class="col s12 dt_wrapper">

                                <table id="page-length-option" class="display responsive-table">

                                    <thead>
                                    <tr class="timo-table-headers">
                                        <th style="width: 4%">#</th>
                                        <th style="width: 36%">Appraisal Name</th>
                                        <th style="width: 15%">Status</th>
                                        <th style="width: 10%">Supervisor Approval</th>
                                        <th style="width: 10%">Employee Acceptance</th>
                                        <th style="width: 10%">HOD Approval</th>
                                        <th style="width: 10%">ED</br>Approval</th>
                                        <th style="width: 10%">Date Created</th>
                                        <th data-orderable="false" style="width: 5%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if(isset($appraisals) && count($appraisals)>0)

                                        @foreach($appraisals as $appraisal)

                                            <tr @if($appraisal->isRejected)class="red-text text-darken-2"@endif>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$appraisal->generatedPdfName}}</td>
                                                <td>{{$appraisal->simpleStatus}}</td>
                                                <td>{{$appraisal->supervisorDecision}}</td>
                                                <td>{{$appraisal->employeeAcceptanceStatus}}</td>
                                                <td>{{$appraisal->deptHeadDecision}}</td>
                                                <td>{{$appraisal->executiveDirectorDecision}}</td>
                                                <td>{{$appraisal->createdAt}}</td>
                                                <td class="center"><a href="#!" data-constrainwidth="false" data-hover="true" data-beloworigin="true"   data-activates='dropdown{{$appraisal->appraisalRef}}' class="green-text dropdown-trigger"><i class="material-icons">list</i></a></td>
                                                <!-- Dropdown Structure -->
                                                <ul id='dropdown{{$appraisal->appraisalRef}}' class='dropdown-content'>
                                                    <li><a href="{{route('appraisal-forms.show',[$appraisal->appraisalRef])}}"><i class="material-icons">visibility</i>View Appraisal</a></li>
                                                    <li><a data-source="{{route('appraisal-form.appraisal-comments',[$appraisal->appraisalRef])}}" class="modal-trigger modal-trigger-with-server-side-data btn-view-comments" href="#modal_comments"><i class="material-icons">comment</i>View Comments</a></li>
                                                </ul>

                                            </tr>



                                        @endforeach

                                    @else
                                        <tr>
                                            <td colspan="8" class="center">No Appraisals Found In The System</td>
                                        </tr>
                                    @endif


                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END: Page Main-->

    {{-- Message modal --}}
    @if(isset($isError) && isset($msg))  @include('includes.modal-message')   @endif
    @include('appraisals.modal-view-appraisal-comments')

@endsection


@section('page-level-js')

    <script src="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/js/dataTables.select.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('pixinvent/app-assets/js/scripts/data-tables.js')}}" type="text/javascript"></script>

@endsection
