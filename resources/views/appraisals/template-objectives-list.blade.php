
<span class="hidden">
    <select  name="objective_0" required class="browser-default validate s12" style="width: 100%">
        <option value="" disabled selected>Select related strategic objective</option>
        @if(isset($strategicObjectives) && count($strategicObjectives) > 0)
            @foreach($strategicObjectives as $objective)
                <option value="{{$objective->id}}">{{$objective->objective}}</option>
            @endforeach
        @endif
    </select>
</span>