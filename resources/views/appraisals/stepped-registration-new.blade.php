
<input id="default_active_step" value="@if(isset($defaultActiveStepIndex)){{$defaultActiveStepIndex}}@else{{0}}@endif" type="hidden" />
<ul style="overflow: visible" class="stepper col s12">

    <li id="SEC_1" class="step @if(isset($activeStep) and $activeStep == 'SEC_1') active @elseif(in_array('SEC_1',$doneSteps)) done @endif">
        <div  data-step-index = "0" data-step-label="To be filled by appraisee" class="step-title waves-effect">SECTION  1 - Employee Profile</div>
        <div class="step-content">
            <div class="row">
                {{-- Check if we updating this section or not --}}
                @if(isset($appraisal) && isset($appraisal->personalInfo)) @include('appraisals.sections-new.section_1_edit')
                @else @include('appraisals.sections-new.section_1') @endif
            </div>
        </div>
    </li>

    <li id="SEC_2" class="step @if(isset($activeStep) and $activeStep == 'SEC_2') active @elseif(in_array('SEC_2',$doneSteps)) done @endif">
        <div data-step-index = "1" data-step-label="To be filled in by both appraiser and appraisee" class="step-title waves-effect">SECTION 2: Employee Performance In Relation To Achievement Of the Authority's Strategic Objective</div>
        <div class="step-content">
            <div class="row">
                {{-- Check if we updating this section or not --}}
                @if(isset($appraisal) && isset($appraisal->employeePerformances) && count($appraisal->employeePerformances) > 0) @include('appraisals.sections-new.section_2_edit')
                @else @include('appraisals.sections-new.section_2') @endif
            </div>
        </div>
    </li>


    <li  id="SEC_3" class="step @if(isset($activeStep) and $activeStep == 'SEC_3') active @elseif(in_array('SEC_3',$doneSteps)) done @endif">
        <div data-step-index = "2" data-step-label="To be filled in by both appraiser and appraisee" class="step-title waves-effect">SECTION 3 : Assessment Of Behavioral Competences</div>
        <div class="step-content">
            <div class="row">
                @include('appraisals.sections-new.section_3')
            </div>
        </div>
    </li>

    <li  id="SEC_4" class="step @if(isset($activeStep) and $activeStep == 'SEC_4') active @elseif(in_array('SEC_4',$doneSteps)) done @endif">
        <div data-step-index = "3" data-step-label="To be filled by appraiser" class="step-title waves-effect">SECTION 4: Employee Strengths And Weaknesses</div>
        <div class="step-content">
            <div class="row">
                {{-- Check if we updating this section or not --}}
                @if(isset($appraisal) && (isset($appraisal->appraisalStrengths) || isset($appraisal->appraisalWeaknesses) || isset($appraisal->appraisalWeaknessAndStrengthComment)))
                    @include('appraisals.sections-new.section_4_edit')
                @else @include('appraisals.sections-new.section_4') @endif
            </div>
        </div>
    </li>

    <li id="SEC_5" class="step @if(isset($activeStep) and $activeStep == 'SEC_5') active @elseif(in_array('SEC_5',$doneSteps)) done @endif">
        <div data-step-index = "4" data-step-label="To be filled by appraisee" class="step-title waves-effect">SECTION 5. Agreed Targets For The Next Period</div>
        <div class="step-content">
            <div class="row">
                {{-- Check if we updating this section or not --}}
                @if(isset($appraisal) && isset($appraisal->agreedTargets) && count($appraisal->agreedTargets) > 0) @include('appraisals.sections-new.section_5_edit')
                @else @include('appraisals.sections-new.section_5') @endif
            </div>
        </div>
    </li>

    <li  id="SEC_6A" class="step @if(isset($activeStep) and $activeStep == 'SEC_6A') active @elseif(in_array('SEC_6A',$doneSteps)) done @endif">
        <div data-step-index = "5" data-step-label="To be filled by appraiser and appraisee" class="step-title waves-effect">SECTION  6(A). Summary of Appraisee Scores</div>
        <div class="step-content">
            <div class="row">
                @include('appraisals.sections-new.section_6_a')
            </div>
        </div>
    </li>

    <li  id="SEC_6B" class="step @if(isset($activeStep) and $activeStep == 'SEC_6B') active @elseif(in_array('SEC_6B',$doneSteps)) done @endif">
        <div data-step-index = "6" data-step-label="To be filled by appraiser" class="step-title waves-effect">SECTION  6(B). Declaration by Supervisor</div>
        <div class="step-content">
            <div class="row">
                @include('appraisals.sections-new.section_6_b')
            </div>
        </div>
    </li>

    <li  id="SEC_6C" class="step @if(isset($activeStep) and $activeStep == 'SEC_6C') active @elseif(in_array('SEC_6C',$doneSteps)) done @endif">
        <div data-step-index = "7" data-step-label="To be filled by appraisee" class="step-title waves-effect">SECTION  6(C). Appraisee Remarks</div>
        <div class="step-content">
            <div class="row">
                @include('appraisals.sections-new.section_6_c')
            </div>
        </div>
    </li>

    <li id="SEC_6D" class="step @if(isset($activeStep) and $activeStep == 'SEC_6D') active @elseif(in_array('SEC_6D',$doneSteps)) done @endif">
        <div data-step-index = "8"  data-step-label="To be filled by Head of Department" class="step-title waves-effect">SECTION  6(D). Head of Department's Comment</div>
        <div class="step-content">
            <div class="row">
                @include('appraisals.sections-new.section_6_d')
            </div>
        </div>
    </li>

        <li id="SEC_6E" class="step  @if(isset($activeStep) and $activeStep == 'SEC_6E') active @elseif(in_array('SEC_6E',$doneSteps)) done @endif">
        <div data-step-index = "9" data-step-label="To be filled by Executive Director" class="step-title waves-effect">SECTION  6(E). Overall Comments by Executive Director</div>
        <div class="step-content">
            <div class="row">
                @include('appraisals.sections-new.section_6_e')
            </div>
        </div>
    </li>

</ul>