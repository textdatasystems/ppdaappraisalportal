<div class="col s12">

    <div class="col s12 spacer-top spacer-bottom">
        <label class="red-text text-darken-4">After filling in and saving all the necessary information in all sections, press the Approve button to approve the Appraisal</label>
    </div>

    <div class="col s6">
        <form class="col s12" method="post" action="{{route('appraisal.return-to-previous-status')}}">

            {{csrf_field()}}

            <input type="hidden" name="appraisal_reference" value="{{$appraisal->appraisalRef}}"/>
            <input type="hidden" name="previous_status" value="003"/>
            <input type="hidden" name="active_module" value="ed"/>

            <div class="row spacer-top">
                <label class="col s12" for="comment">Comment</label>
                <div class="input-field col s12">
                    <textarea rows="3" style="min-height: 25px" id="comment" name="comment" ></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col s12 spacer"></div>
            </div>

            <div class="col s6 "><button class="left btn-move-form btn waves-effect waves-light camel-case red  darken-2" type="submit" name="action">Return To HOD</button> </div>

        </form>


    </div>
    <div class="col s6">
        <div class="row spacer-top">
            <div class="col s12 approval-btn-margins "><a href="{{route('appraisals.approve',[$appraisal->appraisalRef,$appraisal->status])}}" class="right btn-move-form waves-effect waves-light btn camel-case green darken-2">Approve</a></div>
        </div>
    </div>

</div>