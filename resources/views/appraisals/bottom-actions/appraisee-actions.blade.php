@if($appraisal->status == '009')


    <div class="col s12">

        <div class="col s12 spacer-top spacer-bottom">
            <label class="red-text text-darken-4">After filling in and saving all the necessary information in all sections, press the Submit</label>
        </div>

        <div class="col s4">
            <div class="row spacer-top ">
                <div class="col s6 "><a href="{{route('appraisal-form.cancel',[$appraisal->appraisalRef])}}" class="approval-btn-margins left btn-move-form waves-effect waves-light btn camel-case red darken-2">Cancel</a></div>
            </div>
        </div>

        <div class="col s4">
            <form class="col s12" method="post" action="{{route('appraisal.return-to-previous-status')}}">

                {{csrf_field()}}

                <input type="hidden" name="appraisal_reference" value="{{$appraisal->appraisalRef}}"/>
                <input type="hidden" name="previous_status" value="001"/>
                <input type="hidden" name="active_module" value="owner"/>

                <div class="row spacer-top">
                    <label class="col s12" for="comment">Comment</label>
                    <div class="input-field col s12">
                        <textarea rows="3" style="min-height: 25px" id="comment" name="comment" ></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12 spacer"></div>
                </div>

                <div class="col s12 center"><button class="center btn-move-form btn waves-effect waves-light camel-case timo-primary" type="submit" name="action">Return To Appraiser</button> </div>

            </form>


        </div>
        <div class="col s4">
            <div class="row spacer-top ">
                <div class="col s12 "><a href="{{route('move_form',[$appraisal->appraisalRef,$appraisal->status])}}" class="approval-btn-margins right btn-move-form waves-effect waves-light btn camel-case green darken-2">Submit</a></div>
            </div>
        </div>

    </div>


@else

    <div class="col s12 spacer-top spacer-bottom">
        <label class="red-text text-darken-4">After filling in and saving all the necessary information in all sections, press the Submit button to submit form to the next step in workflow</label>
    </div>
    <div class="row">
        <div class="col s6 "><a href="{{route('appraisal-form.cancel',[$appraisal->appraisalRef])}}" class="left btn-move-form waves-effect waves-light btn camel-case red darken-2">Cancel</a></div>
        <div class="col s6 "><a href="{{route('move_form',[$appraisal->appraisalRef,$appraisal->status])}}" class="right btn-move-form waves-effect waves-light btn camel-case green darken-2">Submit</a></div>
    </div>

@endif