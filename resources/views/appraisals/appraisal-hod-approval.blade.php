@extends('layouts.master')

@section('title')
    {{config('app.name')}} | HOD Approval
@endsection

@section('page-level-css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/css/select.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('pixinvent/app-assets/css/data-tables.css')}}">
@endsection

@section('content')

    <header>
        @include('includes.nav-bar-general')
    </header>

    <!-- BEGIN: Page Main-->
    <div class="container" id="main">

        <div class="row spacer-top">

            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title grey-text text-darken-1 ">HOD APPROVALS<span class="grey-text text-darken-1 "  style="font-size: small" >&nbsp;Assigned to you</span></h5>
                        <div class="hr-dotted spacer-bottom"></div>
                        <div class="row">

                            <div class="col s12 dt_wrapper">

                                <table id="page-length-option" class="display responsive-table table-tiny-text">

                                    <thead>
                                    <tr class="timo-table-headers">
                                        <th  style="width: 4%">#</th>
                                        <th  style="width: 35%">Appraisal Name</th>
                                        <th  style="width: 13%">Appraisee Name</th>
                                        <th  style="width: 10%">Supervisor Approval</th>
                                        <th  style="width: 10%">HOD Approval</th>
                                        <th  style="width: 10%">ED</br>Approval</th>
                                        <th  style="width: 14%">Submission Date</th>
                                        <th data-orderable="false"  style="width: 4%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if(isset($appraisals) && count($appraisals)>0)

                                        @foreach($appraisals as $appraisal)

                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$appraisal->generatedPdfName}}</td>
                                                <td>{{$appraisal->personalInfo->firstName.' '.$appraisal->personalInfo->lastName}}</td>
                                                <td>{{$appraisal->supervisorDecision}}</td>
                                                <td>{{$appraisal->deptHeadDecision}}</td>
                                                <td>{{$appraisal->executiveDirectorDecision}}</td>
                                                <td>{{$appraisal->deptHeadSubmissionDate}}</td>
                                                <td class="center"><a href="#!" data-constrainwidth="false" data-hover="true" data-beloworigin="true"   data-activates='dropdown{{$appraisal->appraisalRef}}' class="green-text dropdown-trigger"><i class="material-icons">list</i></a></td>
                                                <!-- Dropdown Structure -->
                                                <ul id='dropdown{{$appraisal->appraisalRef}}' class='dropdown-content'>
                                                    <li><a href="{{route('appraisal-forms.show',[$appraisal->appraisalRef])}}"><i class="material-icons">visibility</i>View Appraisal</a></li>
                                                    <li><a data-source="{{route('appraisal-form.appraisal-comments',[$appraisal->appraisalRef])}}" class="modal-trigger modal-trigger-with-server-side-data btn-view-comments" href="#modal_comments"><i class="material-icons">comment</i>View Comments</a></li>
                                                </ul>

                                            </tr>

                                        @endforeach

                                    @else
                                        <tr>
                                            <td colspan="8" class="center">No Appraisals Found In The System</td>
                                        </tr>
                                    @endif


                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END: Page Main-->

    {{-- Message modal --}}
    @if(isset($isError) && isset($msg))  @include('includes.modal-message')   @endif
    @include('appraisals.modal-view-appraisal-comments')

@endsection


@section('page-level-js')

    <script src="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('pixinvent/app-assets/vendors/data-tables/js/dataTables.select.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('pixinvent/app-assets/js/scripts/data-tables.js')}}" type="text/javascript"></script>

@endsection
