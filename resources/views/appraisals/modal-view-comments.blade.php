<h5 class="timo-primary-text center">Appraisal Comments</h5>
<table class="bordered table table-hover table-tiny-text" id="tabla">

    <thead class="timo-admin-table-head">
    <tr>
        <th>#</th>
        <th>Comment</th>
        <th>Author</th>
        <th>Date</th>
    </tr>
    </thead>

    <tbody >

    @if(isset($appraisal) && isset($appraisal->comments) && count($appraisal->comments)>0)
        @foreach($appraisal->comments as $comment)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$comment->comment}}</td>
                <td>{{$comment->author}}</td>
                <td>{{$comment->date}}</td>
            </tr>
        @endforeach
    @else
        <tr><td class="center" colspan="4">No comments found</td></tr>
    @endif

    </tbody>

</table>