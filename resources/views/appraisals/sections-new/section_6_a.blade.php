
<form id="section-i-form" method="post" action="{{route('save_section_6a')}}" class="col s12 table-format">

    @if(isset($appraisal) && isset($appraisal->assessmentSummary))
        <input type="hidden" name="record_id" value="{{$appraisal->assessmentSummary->id}}"/>
    @endif

        <div style="padding: 20px" class="row spacer-top grey lighten-2 valign-wrapper">

            <div class="col m8 s12  spacer-top">
                <div class="row">

                    <div class="col s12 ">
                        <a class="btn-flat cursor orange camel-case white-text" onclick="updateFormTotalScoresAgain()">Reload Scores</a>
                    </div>

                    <div class="col s12 blue-text bold-text spacer-top ">Percentage Scores (Before applying weight out of 100)</div>

                    <div class="row ">
                        <div class="col s12 valign-wrapper">
                            <div class="col s9">Section 2 :Assessment of Key result areas</div>
                            <div class="col s3 right"><input readonly @if(isset($appraisal) && isset($appraisal->assessmentSummary) ) value="{{$appraisal->assessmentSummary->keyResultAreaScoreNoWeight}}" @endif
                                size="8" name="key_result_score_no_weight" id="key_result_score_no_weight" type="text" class="validate browser-default"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 valign-wrapper">
                            <div class="col s9">Section 3: Assessment of behavioral Competences</div>
                            <div class="col s3 right"><input readonly @if(isset($appraisal) && isset($appraisal->assessmentSummary) ) value="{{$appraisal->assessmentSummary->behavioralScoreNoWeight}}" @endif
                                size="8" name="behavioral_competence_score_no_weight" id="behavioral_competence_score_no_weight" type="text" class="validate browser-default"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12 red-text bold-text ">Overall Assessment</div>
                    <div class="row ">
                        <div class="col s12 valign-wrapper">
                            <div class="col s9 ">Section 2 :Assessment of Key result areas (80%)</div>
                            <div class="col s3 right"><input readonly @if(isset($appraisal) && isset($appraisal->assessmentSummary) ) value="{{$appraisal->assessmentSummary->keyResultAreaScoreActual}}" @endif
                                size="8"  name="key_result_score_actual" id="key_result_score_actual" type="text" class="validate browser-default"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 valign-wrapper">
                            <div class="col s9 ">Section 3: Assessment of behavioral Competences(20%)</div>
                            <div class="col s3 right"><input readonly @if(isset($appraisal) && isset($appraisal->assessmentSummary) ) value="{{$appraisal->assessmentSummary->behavioralScoreActual}}" @endif
                                size="8"  name="behavioral_competence_score_actual" id="behavioral_competence_score_actual" type="text" class="validate browser-default"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 valign-wrapper">
                            <div class="col s9 bold-text">TOTAL</div>
                            <div class="col s3 right"><input readonly @if(isset($appraisal) && isset($appraisal->assessmentSummary) ) value="{{$appraisal->assessmentSummary->totalScoreActual}}" @endif
                                size="8" name="total_score_actual" id="total_score_actual" type="text" class="validate browser-default"></div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col m4 s12  spacer-top">
                <div class="row " style="margin-top: 0">
                    <div class="col  s12">
                        <div class="row row-no-margin-bot"><span class="col s12 left bold-text">Assessment Rating</span></div>
                        <div class="row row-no-margin-bot"> <span class="col s6 left">Unsatisfactory</span>  <span class="col s6 right">below 49%</span></div>
                        <div class="row row-no-margin-bot"> <span class="col s6 left">Moderately Satisfactory</span>  <span class="col s6 right">50% - 59%</span></div>
                        <div class="row row-no-margin-bot"><span class="col s6 left">Satisfactory</span>  <span class="col s6 right">60% - 79%</span></div>
                        <div class="row row-no-margin-bot"><span class="col s6 left">Exceeds Expectation</span>  <span class="col s6 right">80% - 89%</span></div>
                        <div class="row row-no-margin-bot"><span class="col s6 left">Outstanding</span>  <span class="col s6 right">90%-100%</span></div>
                        <div class="row row-no-margin-bot"><span class="col s12 left bold-text">*Pass mark at appraisal is 50%</span></div>
                    </div>
                </div>
            </div>

        </div>


        <div class="row spacer-top">
        <div class="col s12 timo-appraisal-th">Appraisee Comment</div>
        <div class="row">
            <div class="col s12 valign-wrapper">
                <textarea name="appraisee_comment">@if(isset($appraisal) && isset($appraisal->assessmentSummary)){{$appraisal->assessmentSummary->appraiseeComment}}@else{{old('appraisee_comment')}}@endif</textarea>
            </div>
        </div>


            <div class="row spacer-top">
                <div class="col s12 timo-appraisal-th">Appraiser Overall Recommendation</div>
                <div class="col s12">
                    <select name="recommendation_decision" class="browser-default validate " style="width: 100%">
                        <option value="" disabled selected>Select Appraiser Overall Comment/Recommendation</option>
                        <option
                                @if(isset($appraisal) && ($appraisal->assessmentSummary != null) && ($appraisal->assessmentSummary->appraiserRecommendation == 'To confirm in service')) selected @elseif(old('recommendation_decision') == 'To confirm in service') selected @endif
                        >To confirm in service</option>
                        <option
                                @if(isset($appraisal) && ($appraisal->assessmentSummary != null) && ($appraisal->assessmentSummary->appraiserRecommendation == 'Not to confirm in service')) selected @elseif(old('recommendation_decision') == 'Not to confirm in service') selected  @endif
                        >Not to confirm in service</option>
                        <option
                                @if(isset($appraisal) && ($appraisal->assessmentSummary != null) && ($appraisal->assessmentSummary->appraiserRecommendation == 'To renew contract')) selected @elseif(old('recommendation_decision') == 'To renew contract') selected  @endif
                        >To renew contract</option>
                        <option
                                @if(isset($appraisal) && ($appraisal->assessmentSummary != null) && ($appraisal->assessmentSummary->appraiserRecommendation == 'Training')) selected @elseif(old('recommendation_decision') == 'Training') selected  @endif
                        >Training</option>
                        <option
                                @if(isset($appraisal) && ($appraisal->assessmentSummary != null) && ($appraisal->assessmentSummary->appraiserRecommendation == 'Transfer')) selected @elseif(old('recommendation_decision') == 'Transfer') selected  @endif
                        >Transfer</option>
                        <option
                                @if(isset($appraisal) && ($appraisal->assessmentSummary != null) && ($appraisal->assessmentSummary->appraiserRecommendation == 'Regrading')) selected @elseif(old('recommendation_decision') == 'Regrading') selected  @endif
                        >Regrading</option>
                        <option
                                @if(isset($appraisal) && ($appraisal->assessmentSummary != null) && ($appraisal->assessmentSummary->appraiserRecommendation == 'Termination of services')) selected @elseif(old('recommendation_decision') == 'Termination of services') selected  @endif
                        >Termination of services</option>
                        <option
                                @if(isset($appraisal) && ($appraisal->assessmentSummary != null) && ($appraisal->assessmentSummary->appraiserRecommendation == 'Probationary period comments')) selected @elseif(old('recommendation_decision') == 'Probationary period comments') selected  @endif
                        >Probationary period comments</option>
                        <option
                                @if(isset($appraisal) && ($appraisal->assessmentSummary != null) && ($appraisal->assessmentSummary->appraiserRecommendation == 'Others')) selected @elseif(old('recommendation_decision') == 'Others') selected   @endif
                        >Others</option>
                    </select>
                </div>

            </div>

            <div class="row spacer-top">
                <div class="col s12 timo-appraisal-th">Appraiser Comment</div>
                <div class="col s12 ">
                    <textarea placeholder="Enter your comment here" name="appraiser_comment" class="textarea-sec-g">@if(isset($appraisal) && ($appraisal->assessmentSummary != null)){{$appraisal->assessmentSummary->appraiserComment}}@else{{old('appraiser_comment')}}@endif</textarea>
                </div>
            </div>

    </div>

    <div class="row">
        <div class="input-field col s12">

            @if(!isset($appraisal))
                <button type="submit" name="action" class="disabled btn btn-save camel-case blue-stepper">Save Section 6(A)</button>
                <input type="hidden" name="save">
            @elseif(isset($appraisal) && !isset($appraisal->assessmentSummary))
                <button type="submit" name="action" class="btn btn-save camel-case blue-stepper">Save Section 6(A)</button>
                <input type="hidden" name="save">
                <input type="hidden" name="appraisal" value="{{$appraisal->appraisalRef}}">
            @else
                <button type="submit" name="action" class="btn btn-save camel-case blue-stepper">Update Section 6(A)</button>
                <input type="hidden" name="update">
                <input type="hidden" name="appraisal" value="{{$appraisal->appraisalRef}}">
            @endif

            {{csrf_field()}}

        </div>
    </div>

</form>