
<form id="section-h-form" method="post" action="{{route('save_section_4')}}" class="col s12 table-format">

    @if(isset($appraisal) && isset($appraisal->strengthAndWeakness))
        <input type="hidden" name="record_id" value="{{$appraisal->strengthAndWeakness->id}}"/>
    @endif


    <div class="row spacer-top spacer-bottom">
        <div class="col s12 timo-appraisal-th">Strengths of Appraisee</div>
        <div class="row">
            <div class="col s12 valign-wrapper">
                <textarea name="strengths" >@if(isset($appraisal) && isset($appraisal->strengthAndWeakness)){{$appraisal->strengthAndWeakness->strengths}}@else{{old('strengths')}}@endif</textarea>
            </div>
        </div>
    </div>

    <div class="col s12 spacer"></div>
    <div class="col s12 spacer"></div>

    <div class="row spacer-top">

        <div class="row ">
            <div class="col s12">
                <div class="col m3 s12"><span class="timo-appraisal-th">Weakness</span></div>
                <div class="col m3 s12"><span class="timo-appraisal-th-green">Mitigation measures for weakness</span></div>
                <div class="col m3 s12"><span class="timo-appraisal-th">Self-improvement plan by the employee</span></div>
                <div class="col m1 s12"><span class="timo-appraisal-th-green">Time Frame</span></div>
                <div class="col m2 s12"><span class="timo-appraisal-th">Supervisor comments</span></div>
            </div>
        </div>

        {{-- begin of a section to hold rows --}}
        <span id="parent_dynamic_weaknesses">
            @for($i = 1; $i <= 1 ; $i++)
                <div >
                    <div class="col s12">
                        <div class="col s3 ">
                            <textarea id="weakness_{{$i}}" name="weakness_{{$i}}" type="text" class="validate">{{old('weakness_'.$i)}}</textarea>
                        </div>
                        <div class="col s3 ">
                            <textarea id="mitigation_measure_{{$i}}" name="mitigation_measure_{{$i}}" type="text" class="validate">{{old('mitigation_measure_'.$i)}}</textarea>
                        </div>
                        <div class="col s3 ">
                            <textarea id="improvement_plan_{{$i}}" name="improvement_plan_{{$i}}" type="text" class="validate">{{old('improvement_plan_'.$i)}}</textarea>
                        </div>
                        <div class="col s1 ">
                            <input value="{{old('time_frame_'.$i)}}" id="time_frame_{{$i}}" name="time_frame_{{$i}}" type="text" class="validate browser-default tab-input">
                        </div>
                        <div class="col s2 ">
                            <textarea id="supervisor_comment_{{$i}}" name="supervisor_comment_{{$i}}" type="text" class="validate">{{old('supervisor_comment_'.$i)}}</textarea>
                        </div>
                    </div>
                </div>
                    @endfor
            </span>
        {{-- end of section to hold rows --}}

        {{-- Keeps track of how rows we have in the above section by default it's 4, each time we add a row or remove a row we update this value using javascript --}}
        <input type="hidden" value="1" id="counter_max_rows_weaknesses" name="counter_max_rows_weaknesses"/>

        {{-- buttons for dynamically removing or adding rows --}}
        <div class="row">
            <div class="col s12 spacer-top">
                <div onclick="addElement('parent_dynamic_weaknesses','div','counter_max_rows_weaknesses');" class="btn-add-element camel-case grey darken-1">Add Row</div>
                <div onclick="deleteLastElement('parent_dynamic_weaknesses','counter_max_rows_weaknesses');" class="btn-add-element camel-case grey darken-1">Remove Last Row</div>
            </div>
        </div>
    </div>


    <div class="col s12 spacer"></div>
    <div class="col s12 spacer"></div>

    <div class="row spacer-top">
        <div class="col s6">
            <div class="col s12 timo-appraisal-th">Supervisor's comment</div>
            <div class="row">
                <div class="col s12 valign-wrapper">
                    <textarea name="head_of_unit_comment" >@if(isset($appraisal) && isset($appraisal->strengthAndWeakness)){{$appraisal->strengthAndWeakness->strengths}}@else{{old('strengths')}}@endif</textarea>
                </div>
            </div>
        </div>
        <div class="col s6">
            <div class="col s12 timo-appraisal-th">Head of department's comment</div>
            <div class="row">
                <div class="col s12 valign-wrapper">
                    <textarea name="hod_comment" >@if(isset($appraisal) && isset($appraisal->strengthAndWeakness)){{$appraisal->strengthAndWeakness->strengths}}@else{{old('strengths')}}@endif</textarea>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">

            @if(!isset($appraisal))
                <button type="submit" name="action" class="disabled btn btn-save camel-case blue-stepper">Save Section 4</button>
                <input type="hidden" name="save">
            @elseif(isset($appraisal) && !isset($appraisal->strengthAndWeakness))
                <button type="submit" name="action" class="btn btn-save camel-case blue-stepper">Save Section 4</button>
                <input type="hidden" name="save">
                <input type="hidden" name="appraisal" value="{{$appraisal->appraisalRef}}">
            @else
                <button type="submit" name="action" class="btn btn-save camel-case blue-stepper">Update Section 4</button>
                <input type="hidden" name="update">
                <input type="hidden" name="appraisal" value="{{$appraisal->appraisalRef}}">
            @endif

            {{csrf_field()}}

        </div>
    </div>

</form>