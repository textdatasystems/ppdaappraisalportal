
<form id="section-d-form" class="col s12 table-format" action="{{route('save_section_two')}}" method="post">

    <div class="row">
        <div class="col s12 table-tiny-text bold-text spacer-top spacer-bottom">
            <div>Total Maximum Performance Rating is 100</div>
            <div>1.	Unsatisfactory (0-49%) 2. Moderately Satisfactory (50-69%) 3.Satisfactory (70-89%) 4. Highly Satisfactory (90-100%)</div>
        </div>
    </div>
    <div class="row">
        <div class="col s6">
            <div class="col s4 "><span class="timo-appraisal-th-min">Key Result Area (KRA)</span></div>
            <div class="col s4 "><span class="timo-appraisal-th-min-green">Key Performance Indicators (KPI) to achieve Objectives</span></div>
            <div class="col s4 "><span class="timo-appraisal-th-min">Performance Result at end of appraisal period</span></div>
        </div>
        <div class="col s3">
            <div class="col s3 " style="padding-left: 0;padding-right: 0;"><span class="timo-appraisal-th-min-green">Maximum Score</span></div>
            <div class="col s3 " style="padding-left: 0;padding-right: 0;"><span class="timo-appraisal-th-min">Appraisee Score</span></div>
            <div class="col s3 " style="padding-left: 0;padding-right: 0;"><span class="timo-appraisal-th-min-green">Appraiser Score</span></div>
            <div class="col s3 " style="padding-left: 0;padding-right: 0;"><span class="timo-appraisal-th-min">Agreed Score</span></div>
        </div>
        <div class="col s3">
            <div class="col s6 "><span class="timo-appraisal-th-min-green">Appraisee comments</span></div>
            <div class="col s6 "><span class="timo-appraisal-th-min">Appraiser comments</span></div>
        </div>
    </div>

    {{-- begin of a section to hold rows --}}
    <span id="parent_dynamic_employee_performance">
        @for($i = 1; $i <= 4 ; $i++)

            <div style="padding: 5px;" class="@if(($i%2) != 0) row-strip-dark @else row-strip-light @endif">

                <div class="row ">
                    <div class="col s12">
                        <select  name="objective_{{$i}}" required class="browser-default validate s12" style="width: 100%">
                            <option value="" disabled selected>Select related strategic objective</option>
                            @if(isset($strategicObjectives) && count($strategicObjectives) > 0)
                                @foreach($strategicObjectives as $objective)
                                    <option @if(old('objective_'.$i) == $objective->id ) selected @endif value="{{$objective->id}}">{{$objective->objective}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col s6">
                        <div class="col s4 ">
                            <textarea id="key_result_area_{{$i}}" name="key_result_area_{{$i}}" type="text" class="validate">{{old('key_result_area_'.$i)}}</textarea>
                        </div>
                        <div class="col s4 ">
                            <textarea id="key_performance_indicator_{{$i}}" name="key_performance_indicator_{{$i}}" type="text" class="validate">{{old('key_performance_indicator_'.$i)}}</textarea>
                        </div>
                        <div class="col s4 ">
                            <textarea id="performance_result_{{$i}}" name="performance_result_{{$i}}" type="text" class="validate">{{old('performance_result_'.$i)}}</textarea>
                        </div>
                    </div>

                    <div class="col s3">
                        <div class="col s3 ">
                            <input value="{{old('maximum_rating_sec_two_'.$i)}}" id="maximum_rating_sec_two_{{$i}}" min="0" name="maximum_rating_sec_two_{{$i}}"
                                   onblur="sumColumnRows('maximum_rating_sec_two_',25,'sec_2_total_maximum_rating',100,this);"
                                   onchange="setMax(this, 'appraisee_rating_sec_two_{{$i}}','appraiser_rating_sec_two_{{$i}}','agreed_rating_sec_two_{{$i}}');"
                                   type="number" class="validate browser-default tab-input">
                        </div>
                        <div class="col s3 ">
                            <input value="{{old('appraisee_rating_sec_two_'.$i)}}" id="appraisee_rating_sec_two_{{$i}}" min="0" name="appraisee_rating_sec_two_{{$i}}"
                                   onblur="checkMaxValue(this);sumColumnRows('appraisee_rating_sec_two_',25,'sec_2_total_appraisee_rating',100,this);"
                                   type="number" class="validate browser-default tab-input">
                        </div>
                        <div class="col s3 ">
                            <input value="{{old('appraiser_rating_sec_two_'.$i)}}" id="appraiser_rating_sec_two_{{$i}}" min="0" name="appraiser_rating_sec_two_{{$i}}"
                                   onblur="checkMaxValue(this);sumColumnRows('appraiser_rating_sec_two_',25,'sec_2_total_appraiser_rating',100,this);"
                                   type="number" class="validate browser-default tab-input">
                        </div>
                        <div class="col s3 ">
                            <input value="{{old('agreed_rating_sec_two_'.$i)}}" id="agreed_rating_sec_two_{{$i}}" min="0" name="agreed_rating_sec_two_{{$i}}"
                                   onblur="checkMaxValue(this);sumColumnRows('agreed_rating_sec_two_',25,'sec_2_total_agreed_rating',100,this);"
                                   type="number" class="validate browser-default tab-input">
                        </div>
                    </div>

                     <div class="col s3">
                        <div class="col s6 ">
                            <textarea id="appraisee_comment_section_two_{{$i}}" name="appraisee_comment_section_two_{{$i}}" type="text" class="validate">{{old('appraisee_comment_section_two_'.$i)}}</textarea>
                        </div>
                        <div class="col s6 ">
                            <textarea id="supervisor_comment_section_two_{{$i}}" name="supervisor_comment_section_two_{{$i}}" type="text" class="validate">{{old('supervisor_comment_section_two_'.$i)}}</textarea>
                        </div>
                     </div>
                </div>

                {{-- Used to match the fields when generating then for update --}}
                <input type="hidden" name="form_field_count{{$i}}" value="{{$i}}"/>
            </div>

        @endfor
        </span>
    {{-- end of section to hold rows --}}


    <div class="row valign-wrapper">
        <div class="col s6">
            <span class="bold-text right">TOTAL SCORE</span>
        </div>

        <div class="col s3">
            <div class="col s3 ">
                <input readonly value="{{old('sec_2_total_maximum_rating')}}" id="sec_2_total_maximum_rating"  min="0" name="sec_2_total_maximum_rating" type="number" class="validate browser-default tab-input">
            </div>
            <div class="col s3 ">
                <input readonly value="{{old('sec_2_total_appraisee_rating')}}" id="sec_2_total_appraisee_rating"  min="0" name="sec_2_total_appraisee_rating" type="number" class="validate browser-default tab-input">
            </div>
            <div class="col s3 ">
                <input readonly value="{{old('sec_2_total_appraiser_rating')}}" id="sec_2_total_appraiser_rating"  min="0" name="sec_2_total_appraiser_rating" type="number" class="validate browser-default tab-input">
            </div>
            <div class="col s3 ">
                <input readonly value="{{old('sec_2_total_agreed_rating')}}" id="sec_2_total_agreed_rating"  min="0" name="sec_2_total_agreed_rating" type="number" class="validate browser-default tab-input">
            </div>
        </div>
        <div class="col s3"></div>
    </div>


    {{-- Keeps track of how rows we have in the above section by default it's 4, each time we add a row or remove
      a row we update this value using javascript --}}
    <input type="hidden" value="4" id="counter_max_rows_employee_performance" name="counter_max_rows_employee_performance"/>


    {{-- buttons for dynamically removing or adding rows --}}
    <div class="row">
        <div class="col s12 spacer-top">
            <div onclick="addElement('parent_dynamic_employee_performance','div','counter_max_rows_employee_performance');" class="btn-add-element camel-case grey darken-1">Add Row</div>
            <div onclick="deleteLastElement('parent_dynamic_employee_performance','counter_max_rows_employee_performance');initiateRecalculationOfEmployeePerformanceTotalScores();" class="btn-add-element camel-case grey darken-1">Remove Last Row</div>
        </div>
    </div>

    <div class="row spacer-top spacer-bottom">
        <div class="col s12 timo-appraisal-th">Supervisor Comments</div>
        <div class="row">
            <div class="col s12 valign-wrapper">
                <textarea name="supervisor_comment" >{{old('supervisor_comment')}}</textarea>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">

            <button id="btnSubmitSectionD" @if(!isset($appraisal)) disabled @endif type="submit" name="action" class="btn btn-save camel-case blue-stepper">Save Section 2</button>
            <input type="hidden" name="save">
            <input type="hidden" name="appraisal" value="@if(isset($appraisal)){{$appraisal->appraisalRef}}@endif">
            {{csrf_field()}}

        </div>
    </div>

</form>