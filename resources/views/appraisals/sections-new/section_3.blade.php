
<form id="section-e-form" method="post" action="{{route('save_section_3')}}" class="col s12">

    <div class="row">
        <div class="col s12 table-tiny-text bold-text spacer-top">
            <div>Behavioral Competency (Competence Rating: Maximum Score 10)</div>
            <div>1.	Unsatisfactory (0-49%) 2. Moderately Satisfactory (50-69%) 3.Satisfactory (70 -89%) 4. Highly Satisfactory (90-100%)</div>
        </div>
    </div>

    <div class="row ">

        <div class="col s12">

            <table class="bordered" border="1" id="table_sec_e" style="border-collapse: collapse;margin-right: 5px">
                <thead>
                <thead>
                <tr >
                    <th class="timo-appraisal-th" colspan="2">Behavioral Competence</th>
                    <th class="timo-appraisal-th" style="width: 65px;">Maximum score</th>
                    <th class="timo-appraisal-th" style="width: 65px;">Employee self assessment</th>
                    <th class="timo-appraisal-th" style="width: 65px;">Supervisor assessment</th>
                    <th class="timo-appraisal-th" style="width: 65px;">Agreed assessment</th>
                </tr>
                </thead>

                <tbody>

                {{--new competence form--}}
                @if(isset($categorizedCompetences))

                    @foreach($categorizedCompetences as $category)
                        {{--each category is an an array of competence objectives--}}

                        {{--category  header--}}

                        <tr>
                            <th style="width: 10px">{{$loop->iteration}}</th>
                            <th >{{$category->category}}</th>
                            <th style="width: 65px;">{{$category->maximumRating}}</th>
                            <th style="width: 65px;">&nbsp;</th>
                            <th style="width: 65px;">&nbsp;</th>
                            <th style="width: 65px;">&nbsp;</th>
                        </tr>

                        {{-- loop through the competence --}}

                        @foreach($category->behavioralCompetences as $item)
                            <tr>
                                <td></td>
                                <td>{{$item->competence}}</td>
                                <td style="padding-top: 5px;"><input readonly step="any" value="{{$item->maximumScore}}" class="browser-default" tabindex="214"  type="number" size="3" style="width: 65px;" name="secEmax02"  min="0"
                                             onblur="finishTable('table_sec_e',2,'secEmaxTotal')"/></td>

                                <td style="padding-top: 5px;"><input step="any" value="{{isset($item->scoreAppraiseeRating) ? $item->scoreAppraiseeRating : old($category->id.'_appraisee_rating_'.$item->id)}}" class="browser-default" tabindex="215"  type="number" size="3" style="width: 65px;" name="{{$category->id}}_appraisee_rating_{{$item->id}}"  min="0" max="{{$item->maximumScore}}"
                                            onblur="checkMaxValue(this);finishTable('table_sec_e',3,'secEappraiseeTotal')"/></td>

                                <td style="padding-top: 5px;"><input step="any"  value="{{isset($item->scoreAppraiserRating) ? $item->scoreAppraiserRating : old($category->id.'_appraiser_rating_'.$item->id)}}" class="browser-default" tabindex="216"  type="number" size="3" style="width: 65px;" name="{{$category->id}}_appraiser_rating_{{$item->id}}"  min="0" max="{{$item->maximumScore}}"
                                            onblur="checkMaxValue(this);finishTable('table_sec_e',4,'secEappraiserTotal')"/></td>

                                <td style="padding-top: 5px;"><input step="any" value="{{isset($item->scoreAgreedRating) ? $item->scoreAgreedRating : old($category->id.'_agreed_rating_'.$item->id)}}" class="browser-default" tabindex="217"  type="number" size="3" style="width: 65px;" name="{{$category->id}}_agreed_rating_{{$item->id}}"  min="0" max="{{$item->maximumScore}}"
                                            onblur="checkMaxValue(this);finishTable('table_sec_e',5,'secEagreedTotal');updateSection3PercentScore();"/></td>
                            </tr>

                            @if(isset($item->scoreRecordId)) <input value="{{$item->scoreRecordId}}" name="{{$category->id}}_record_id_{{$item->id}}" type="hidden"/> @endif

                        @endforeach


                    @endforeach

                @endif

                </tbody>

                <tfoot>
                <tr><td colspan="2" style="text-align: right"><strong> TOTAL SCORE</strong></td>
                    <td style="padding-top: 5px;"><input @if(isset($appraisal) && isset($appraisal->competenceAssessmentsScores) ) value="{{$appraisal->competenceAssessmentsScores->totalMaximumRating}}" @else value="100" @endif
                                tabindex="406" class="browser-default"  type="text" readonly size="3" style="width: 65px;" name="secEmaxTotal" id="secEmaxTotal" onblur="finishTable('table_sec_e',2,'secEmaxTotal')"/></td>
                    <td style="padding-top: 5px;"><input @if(isset($appraisal) && isset($appraisal->competenceAssessmentsScores) ) value="{{$appraisal->competenceAssessmentsScores->totalAppraiseeRating}}" @else value="{{old('secEappraiseeTotal')}}" @endif
                                tabindex="407" class="browser-default"  type="text" readonly size="3" style="width: 65px;" name="secEappraiseeTotal" id="secEappraiseeTotal" onblur="finishTable('table_sec_e',3,'secEappraiseeTotal')"/></td>
                    <td style="padding-top: 5px;"><input @if(isset($appraisal) && isset($appraisal->competenceAssessmentsScores) ) value="{{$appraisal->competenceAssessmentsScores->totalAppraiserRating}}" @else value="{{old('secEappraiserTotal')}}" @endif
                                tabindex="408" class="browser-default"  type="text" readonly size="3" style="width: 65px;"name="secEappraiserTotal" id="secEappraiserTotal" onblur="finishTable('table_sec_e',4,'secEappraiserTotal')"/></td>
                    <td style="padding-top: 5px;"><input @if(isset($appraisal) && isset($appraisal->competenceAssessmentsScores) ) value="{{$appraisal->competenceAssessmentsScores->totalAgreedRating}}" @else value="{{old('secEagreedTotal')}}" @endif
                                tabindex="409" class="browser-default"  type="text" readonly size="3" style="width: 65px;" name="secEagreedTotal" id="secEagreedTotal" onblur="finishTable('table_sec_e',5,'secEagreedTotal')"/></td>
                </tr>
                </tfoot>

            </table>

            @if(isset($appraisal) && isset($appraisal->competenceAssessmentsScores))
                <input type="hidden" name="scores_sec_e_record_id" value="{{$appraisal->competenceAssessmentsScores->id}}"/>
            @endif

        </div>

    </div>

    <div class="row spacer-top">
        <div class="col s12 timo-appraisal-th">Supervisor Comments</div>
        <div class="row">
            <div class="col s12 valign-wrapper">
                <textarea name="supervisor_comment" >@if(isset($appraisal) && isset($appraisal->competenceAssessmentsScores)){{$appraisal->competenceAssessmentsScores->supervisorComment}}@else{{old('supervisor_comment')}}@endif</textarea>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="input-field col s12">

            @if(
            !isset($appraisal) ||
            (isset($appraisal) && !isset($appraisal->competenceAssessments)) ||
            (isset($appraisal) && count($appraisal->competenceAssessments) <1 ) )

                <button  @if(!isset($appraisal)) disabled @endif  type="submit" name="action" class="btn btn-save camel-case blue-stepper">Save Section 3</button>
                <input type="hidden" name="save">
                <input type="hidden" name="appraisal" value="@if(isset($appraisal)){{$appraisal->appraisalRef}}@endif">

            @else
                <button type="submit" name="action" class="btn btn-save camel-case blue-stepper">Update Section 3</button>
                <input type="hidden" name="update">
                <input type="hidden" name="appraisal" value="{{$appraisal->appraisalRef}}">
            @endif

            {{csrf_field()}}

        </div>
    </div>

</form>