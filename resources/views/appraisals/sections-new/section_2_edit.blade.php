
<form id="section-d-form" class="col s12 table-format" action="{{route('save_section_two')}}" method="post">

    <div class="row">
        <div class="col s12 table-tiny-text bold-text spacer-top spacer-bottom">
            <div>Total Maximum Performance Rating is 100</div>
            <div>1.	Unsatisfactory (0-49%) 2. Moderately Satisfactory (50-69%) 3.Satisfactory (70-89%) 4. Highly Satisfactory (90-100%)</div>
        </div>
    </div>
    <div class="row">
        <div class="col s6">
            <div class="col s4 "><span class="timo-appraisal-th-min">Key Result Area (KRA)</span></div>
            <div class="col s4 "><span class="timo-appraisal-th-min-green">Key Performance Indicators (KPI) to achieve Objectives</span></div>
            <div class="col s4 "><span class="timo-appraisal-th-min">Performance Result at end of appraisal period</span></div>
        </div>
        <div class="col s3">
            <div class="col s3 " style="padding-left: 0;padding-right: 0;"><span class="timo-appraisal-th-min-green">Maximum Score</span></div>
            <div class="col s3 " style="padding-left: 0;padding-right: 0;"><span class="timo-appraisal-th-min">Appraisee Score</span></div>
            <div class="col s3 " style="padding-left: 0;padding-right: 0;"><span class="timo-appraisal-th-min-green">Appraiser Score</span></div>
            <div class="col s3 " style="padding-left: 0;padding-right: 0;"><span class="timo-appraisal-th-min">Agreed Score</span></div>
        </div>
        <div class="col s3">
            <div class="col s6 "><span class="timo-appraisal-th-min-green">Appraisee comments</span></div>
            <div class="col s6 "><span class="timo-appraisal-th-min">Appraiser comments</span></div>
        </div>
    </div>

    @if(isset($appraisal) && count($appraisal->employeePerformances) > 0)

        {{-- begin of a section to hold rows --}}
        <span id="parent_dynamic_employee_performance">
        @foreach($appraisal->employeePerformances as $performance)

           <div class="@if(($loop->iteration%2) != 0) row-strip-dark @else row-strip-light @endif">

                <input name="record_id_{{$loop->iteration}}" value="{{$performance->id}}"  type="hidden"/>

               <div class="row">
                    <div class="col s12">
                        <select  name="objective_{{$loop->iteration}}" required class="browser-default validate s12" style="width: 100%">
                            <option value="" disabled selected>Select related strategic objective</option>
                            @if(isset($strategicObjectives) && count($strategicObjectives) > 0)
                                @foreach($strategicObjectives as $objective)
                                    <option @if($performance->strategicObjectiveId == $objective->id) selected @endif value="{{$objective->id}}">{{$objective->objective}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                
                <div class="row">
                    
                    <div class="col s6">
                        <div class="col s4 ">
                            <textarea id="key_result_area_{{$loop->iteration}}" name="key_result_area_{{$loop->iteration}}" type="text" class="validate">{{$performance->keyResultArea}}</textarea>
                        </div>
                        <div class="col s4 ">
                            <textarea id="key_performance_indicator_{{$loop->iteration}}" name="key_performance_indicator_{{$loop->iteration}}" type="text" class="validate">{{$performance->keyPerformanceIndicator}}</textarea>
                        </div>
                        <div class="col s4 ">
                            <textarea id="performance_result_{{$loop->iteration}}" name="performance_result_{{$loop->iteration}}" type="text" class="validate">{{$performance->performanceResult}}</textarea>
                        </div>
                    </div>

                    <div class="col s3">
                        <div class="col s3 ">
                            <input value="{{$performance->maximumAssessment}}" id="maximum_rating_sec_two_{{$loop->iteration}}" min="0"
                                   name="maximum_rating_sec_two_{{$loop->iteration}}"
                                   onblur="sumColumnRows('maximum_rating_sec_two_',25,'sec_2_total_maximum_rating',100,this)"
                                   onchange="setMax(this, 'appraisee_rating_sec_two_{{$loop->iteration}}','appraiser_rating_sec_two_{{$loop->iteration}}','agreed_rating_sec_two_{{$loop->iteration}}');"
                                   type="number" class="validate browser-default tab-input">
                        </div>
                        <div class="col s3 ">
                            <input value="{{$performance->appraiseeAssessment}}" id="appraisee_rating_sec_two_{{$loop->iteration}}" min="0" max="{{$performance->maximumAssessment}}"
                                   name="appraisee_rating_sec_two_{{$loop->iteration}}"
                                   onblur="checkMaxValue(this);sumColumnRows('appraisee_rating_sec_two_',25,'sec_2_total_appraisee_rating',100,this)"
                                   type="number" class="validate browser-default tab-input">
                        </div>
                        <div class="col s3 ">
                            <input value="{{$performance->supervisorAssessment}}" id="appraiser_rating_sec_two_{{$loop->iteration}}" min="0" max="{{$performance->maximumAssessment}}"
                                   name="appraiser_rating_sec_two_{{$loop->iteration}}"
                                   onblur="checkMaxValue(this);sumColumnRows('appraiser_rating_sec_two_',25,'sec_2_total_appraiser_rating',100,this)"
                                   type="number" class="validate browser-default tab-input">
                        </div>
                        <div class="col s3 ">
                            <input value="{{$performance->agreedAssessment}}" id="agreed_rating_sec_two_{{$loop->iteration}}" min="0" max="{{$performance->maximumAssessment}}"
                                   name="agreed_rating_sec_two_{{$loop->iteration}}"
                                   onblur="checkMaxValue(this);sumColumnRows('agreed_rating_sec_two_',25,'sec_2_total_agreed_rating',100,this)"
                                   type="number" class="validate browser-default tab-input">
                        </div>
                    </div>

                     <div class="col s3">
                        <div class="col s6 ">
                            <textarea id="appraisee_comment_section_two_{{$loop->iteration}}" name="appraisee_comment_section_two_{{$loop->iteration}}" type="text" class="validate">{{$performance->appraiseeComment}}</textarea>
                        </div>
                        <div class="col s6 ">
                            <textarea id="supervisor_comment_section_two_{{$loop->iteration}}" name="supervisor_comment_section_two_{{$loop->iteration}}" type="text" class="validate">{{$performance->supervisorComment}}</textarea>
                        </div>
                     </div>

            </div>

               {{-- Used to match the fields when generating then for update --}}
               <input type="hidden" name="form_field_count{{$loop->iteration}}" value="{{$loop->iteration}}"/>

           </div>

        @endforeach
        </span>
        {{-- end of section to hold rows --}}

    @endif

    <div class="row valign-wrapper">
        <div class="col s6">
            <span class="bold-text right">TOTAL SCORE</span>
        </div>

        <div class="col s3">
            <div class="col s3 ">
                <input readonly @if(isset($appraisal) && isset($appraisal->employeePerformancesScores) ) value="{{$appraisal->employeePerformancesScores->totalMaximumAssessment}}" @endif
                id="sec_2_total_maximum_rating"  min="0" name="sec_2_total_maximum_rating" type="number" class="validate browser-default tab-input">
            </div>
            <div class="col s3 ">
                <input readonly @if(isset($appraisal) && isset($appraisal->employeePerformancesScores) ) value="{{$appraisal->employeePerformancesScores->totalAppraiseeAssessment}}" @endif
                        id="sec_2_total_appraisee_rating"  min="0" name="sec_2_total_appraisee_rating" type="number" class="validate browser-default tab-input">
            </div>
            <div class="col s3 ">
                <input readonly @if(isset($appraisal) && isset($appraisal->employeePerformancesScores) ) value="{{$appraisal->employeePerformancesScores->totalSupervisorAssessment}}" @endif
                        id="sec_2_total_appraiser_rating"  min="0" name="sec_2_total_appraiser_rating" type="number" class="validate browser-default tab-input">
            </div>
            <div class="col s3 ">
                <input readonly @if(isset($appraisal) && isset($appraisal->employeePerformancesScores) ) value="{{$appraisal->employeePerformancesScores->totalAgreedAssessment}}" @endif
                        id="sec_2_total_agreed_rating"  min="0" name="sec_2_total_agreed_rating" type="number" class="validate browser-default tab-input">
            </div>
            @if(isset($appraisal) && isset($appraisal->employeePerformancesScores))
                <input type="hidden" name="scores_sec_2_record_id" value="{{$appraisal->employeePerformancesScores->id}}"/>
            @endif
        </div>

        <div class="col s3"></div>
    </div>


    {{-- Keeps track of how rows we have in the above section by default it's 4, each time we add a row or remove
      a row we update this value using javascript --}}
    <input type="hidden" value="{{count($appraisal->employeePerformances)}}" id="counter_max_rows_employee_performance" name="counter_max_rows_employee_performance"/>


    {{-- buttons for dynamically removing or adding rows --}}
    <div class="row">
        <div class="col s12 spacer-top">
            <div onclick="addElement('parent_dynamic_employee_performance','div','counter_max_rows_employee_performance');" class="btn-add-element camel-case grey darken-1">Add Row</div>
            <div onclick="deleteLastElement('parent_dynamic_employee_performance','counter_max_rows_employee_performance');initiateRecalculationOfEmployeePerformanceTotalScores();" class="btn-add-element camel-case grey darken-1">Remove Last Row</div>
        </div>
    </div>

    <div class="row spacer-top spacer-bottom">
        <div class="col s12 timo-appraisal-th">Supervisor Comments</div>
        <div class="row">
            <div class="col s12 valign-wrapper">
                <textarea name="supervisor_comment" >@if(isset($appraisal) && isset($appraisal->employeePerformancesScores) ){{$appraisal->employeePerformancesScores->supervisorComment}}@endif</textarea>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">

            <button id="btnSubmitSectionD" type="submit" name="action" class="btn btn-save camel-case blue-stepper">Update Section 2</button>
            <input type="hidden" name="update">
            <input type="hidden" name="appraisal" value="{{$appraisal->appraisalRef}}">
            {{csrf_field()}}

        </div>
        </div>


</form>