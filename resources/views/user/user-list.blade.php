
@extends('layouts.master-admin')
@section('title')
    Admin | System Users
@endsection

@include('user.modal-user')
@include('includes.modal-confirm-delete')

@section('content')

    <div class="container">

        <div class="col s12 spacer"></div>
        <div class="row">
            <h5 class="col s6">SYSTEM USERS</h5>
            <div class="col s6 ">
                <a data-form-id = "form_modal_user" href="#modal_user"   class="timo-btn-add btn waves-effect waves-light camel-case blue darken-4 right modal-trigger" type="button" >Add</a>
            </div>
        </div>

        <table class="bordered table table-hover table-tiny-text" id="tabla">

            <thead class="timo-admin-table-head">
            <tr>
                <th>Username</th>
                <th>Name</th>
                {{--<th>Email</th>--}}
                <th>Phone</th>
                <th>Designation</th>
                <th>System Role</th>
                <th>Department</th>
                <th>Edit</th>
                {{--<th>Delete</th>--}}
            </tr>
            </thead>

            <tbody id="user_profiles_table">

            @if(is_null($users) || count($users) == 0)
                <tr><td class="center" colspan="6">No users accounts found in the system</td></tr>
            @else
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->username}}</td>
                        <td>{{$user->fullName}}</td>
                        {{--<td>{{$user->email}}</td>--}}
                        <td>{{$user->phone}}</td>
                        <td>{{$user->designation}}</td>
                        <td>{{$user->roleName}}</td>
                        <td>{{$user->departmentName}}</td>
                        <td><a data-user_id="{{$user->id}}" class="green-text modal-trigger edit-button" href="#modal_user_edit{{$user->id}}"><i class="material-icons center">edit</i></a></td>
                        {{--<td><a data-delete-url="{{route('delete_user_profile',$user->username)}}" data-item-gp="User Profile" data-item-name="{{$user->fullName}}" class="red-text timo-btn-delete" href="#" ><i class="material-icons center">delete_forever</i></a></td>--}}
                    </tr>

                    {{--include profile view modal --}}
                    <div id="modal_user_edit{{$user->id}}" class="modal modal-70">
                        <div class="modal-content">
                            @include('user.user-form-edit')
                        </div>
                    </div>

                @endforeach
            @endif

            </tbody>

        </table>

        <div class="col s12 spacer"></div>
        <div class="col s12 center">
            <div><span id="total_users"></span></div>
            <ul class="pagination pager" id="myPager"></ul>
        </div>

    </div>


    {{-- Helper Modals are below --}}

    @if(isset($deletionMessage))

        <div id="modal_deletion" class="modal">
            <div class="modal-content">
                <h5>User Deletion</h5>
                <p>{{$deletionMessage}}</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
            </div>
        </div>

    @endif

    {{-- End Helper Modals --}}


@endsection
